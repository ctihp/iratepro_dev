//
//  ViewExtension.swift
//  Auction
//
//  Created by mac on 10/12/20.
//

import Foundation
import UIKit

var leftTopRadius: CGFloat = 0.0
var leftBottomRadius: CGFloat = 0.0
var rightTopRadius: CGFloat = 0.0
var rightBottomRadius: CGFloat = 0.0

extension UIView {
    
    @IBInspectable
    var leftTopCorner: CGFloat {
        get {
            return leftTopRadius
        }set {
            leftTopRadius = newValue
            applyRadius()
        }
    }
    
    @IBInspectable
    var leftBottomCorner: CGFloat {
        get {
            return leftBottomRadius
        }set {
            leftBottomRadius = newValue
            applyRadius()
        }
    }
    
    @IBInspectable
    var rightTopCorner: CGFloat {
        get {
            return rightTopRadius
        }set {
            rightTopRadius = newValue
            applyRadius()
        }
    }
    
    @IBInspectable
    var rightBottomCorner: CGFloat {
        get {
            return rightBottomRadius
        }set {
            rightBottomRadius = newValue
            applyRadius()
        }
    }
    
    func applyRadius() {
        let minX = bounds.minX
        let minY = bounds.minY
        let maxX = bounds.maxX
        let maxY = bounds.maxY
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: minX + leftTopRadius, y: minY))
        path.addLine(to: CGPoint(x: maxX - rightTopRadius, y: minY))
        path.addArc(withCenter: CGPoint(x: maxX - rightTopRadius, y: minY + rightTopRadius), radius: rightTopRadius, startAngle:CGFloat(3 * Double.pi / 2), endAngle: 0, clockwise: true)
        path.addLine(to: CGPoint(x: maxX, y: maxY - rightBottomRadius))
        path.addArc(withCenter: CGPoint(x: maxX - rightBottomRadius, y: maxY - rightBottomRadius), radius: rightBottomRadius, startAngle: 0, endAngle: CGFloat(Double.pi / 2), clockwise: true)
        path.addLine(to: CGPoint(x: minX + leftBottomRadius, y: maxY))
        path.addArc(withCenter: CGPoint(x: minX + leftBottomRadius, y: maxY - leftBottomRadius), radius: leftBottomRadius, startAngle: CGFloat(Double.pi / 2), endAngle: CGFloat(Double.pi), clockwise: true)
        path.addLine(to: CGPoint(x: minX, y: minY + leftTopRadius))
        path.addArc(withCenter: CGPoint(x: minX + leftTopRadius, y: minY + leftTopRadius), radius: leftTopRadius, startAngle: CGFloat(Double.pi), endAngle: CGFloat(3 * Double.pi / 2), clockwise: true)
        path.close()
        
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
}


extension UserDefaults {

    //MARK: Check Login
    func setLoggedIn(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isLoggedIn.rawValue)
        //synchronize()
    }
    
    func isLoggedIn()-> Bool {
        return bool(forKey: UserDefaultsKeys.isLoggedIn.rawValue)
    }
    //
    func getUserData() -> Data {
        return data(forKey: UserDefaultsKeys.loginData.rawValue) ?? Data()
    }
    //MARK: Save User Data
    func setUserData(value: Data){
        set(value, forKey: UserDefaultsKeys.loginData.rawValue)
        //synchronize()
    }

}

enum UserDefaultsKeys : String {
    case isLoggedIn
    case loginData
}

// sigle tin class
class AppDataHelper {
    static let shard = AppDataHelper()
    var logins: GetProfileModel!
}



class AppSharedData {
    static let shared = AppSharedData()
    var isCustomer:Bool{
        get{
            var _seeker = false
            if let seeker = UserDefaults.standard.value(forKey: "isCustomer") as? Bool{
                _seeker = seeker
            }else{
                _seeker = false
                
                // FIXME: HIMANSHU PAL 28/8/20
            }
            return _seeker
        }
    }
  
    private init(){
    
    }
}
