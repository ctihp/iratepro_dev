//
//  StoryBoardManager.swift
//  Auction
//
//  Created by mac on 10/12/20.
//

import Foundation
import UIKit
enum StoryBoard: String {
    case Guide = "Guide"
    case Signup = "Signup"
   //
    case Home = "Home"
    case Favourite = "Favourite"
    case Review = "Review"
    case Profile = "Profile"
    case AddFeatures = "AddFeatures"
    case tabBarViewController = "TabBarViewController"
   //
    
    
    
    
    
}




extension UIViewController {
    class func instance(storyBoard: StoryBoard) -> UIViewController {
        let storyboard = UIStoryboard(name: storyBoard.rawValue, bundle: nil)
        let identifier = NSStringFromClass(self).components(separatedBy: ".").last!
        return storyboard.instantiateViewController(withIdentifier: identifier)
    }
    
}
