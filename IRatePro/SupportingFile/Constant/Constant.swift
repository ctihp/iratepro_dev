//
//  Constent.swift
//  Auction
//
//  Created by mac on 10/12/20.
//

import Foundation
import  UIKit
let appDelegate = UIApplication.shared.delegate as! AppDelegate

let kAppName = "IRatePro"
let kisLogin = "isLogin"

//let baseUrl  =  "https://ctinfotech.com/CT01/irate/Api/" // local
//let ImageUrl =  "https://ctinfotech.com/CT01/irate/assets/profile_image/"

let baseUrl  =  "https://iratepro.com/irate/Api/" // live server
let ImageUrl =  "https://iratepro.com/irate/assets/profile_image/"






//struct appColor {
    let gradPurple = UIColor(named: "purple")
    let  lightPurple = UIColor(named: "lightPurple")
    let gradWhite = UIColor(named: "white")
    let gradgray = UIColor(named: "grey")
    let gradCoral = UIColor(named: "Coral")
//}




struct ApiAction {
    static let category_list = "category_list"
    static let signup = "signup"
    static let login = "login"
    static let otp_verification = "otp_verification"
    static let reset_passowrd = "reset_passowrd"
    static let otp_verification_forgot = "otp_verification_forgot"
    static let getProfile = "getProfile"
    static let logout = "logout"
    static let updateProfile = "updateProfile"
    static let productAlllist = "product_all_list"
    static let addProduct = "addProduct"
    static let rateing_list = "rateing_list"
    static let user_list = "user_list"
    static let addCompanyRating = "addCompanyRating"
    static let product_list = "product_list"
    static let addProductRating = "addProductRating"
    static let check_verification = "check_verification"
    static let customer_list = "customer_list"
    static let addcustomerRating = "addcustomerRating"
    static let product_all_list = "product_all_list"
    static let get_notification_count = "get_notification_count"
    static let notification_list = "notification_list"
    static let rateing_detail = "rateing_detail"
    static let delete_notification = "delete_notification"
    static let getMembershipPlan = "getMembershipPlan"
    static let add_subscription = "add_subscription"
    static let getPaymenHistory = "getPaymenHistory"
    static let addTicket = "addTicket"
    static let ticketList = "ticketList"
    static let ticketDetail = "ticketDetail"
    static let getComments = "getComments"
    static let addTicketComment = "addTicketComment"
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}



//MARK:- Validation string
enum Validation: String {
    case kEnterName = "Please Enter Username"
    case kEnterEmail = "Please Enter Your Email"
    case kEnterValidEmail = "Please Enter Valid Email Address"
    case kEnterMobileNumber = "Please Enter Mobile Number"
    case kEnterValidNumber = "Please Enter Valid Mobile Number"
    case kEnterCompanyName = "Please Enter Company Name"
    case kEnterEINNumber = "Please Enter EIN Number"
    case kEnterBusinessType = "Please Select Business Type"
    case kEnterWebUrl = "Please Enter Website Url"
    case kSelectCategory = "Please Select Category"
    case kEnterCompanyDis = "Please Enter Company Description"
    case kEnterAddress = "Please Enter Your Address"
    case kEnterPassword = "Please Enter Your Password"
    case kEnterConfirmPassword = "Please Enter Confirm Password"
    case kEnterValidPassword = "Please Enter Valid Password"
    case kPassowrdNotMatched = "New Password & Confirm Password Not Matched"
    case kEnterNewPassword = "Please Enter New Password"
    
    case kProductName = "Please Enter Product Name"
    case kProductDetail = "Please Enter Product Detail"
    case kProductDiscription = "Please Enter Product Description"
    case kEnterTitle = "Please Enter Title"
    case kEnterDetail = "Please Enter Detail"
    case kDiscription = "Please Enter Description"
    case kUserType = "Please Select UserType"
    case kCompanyImg = "Please Select Company image"
    
  
    

}

//MARK:- Button titile string
enum ButtonTitle: String {
    case kOk = "OK"
    case kCancel = "CANCEL"
    case kYes = "YES"
    case kNo = "NO"
}


struct Message {
    static let msgSorry            = "Sorry something went wrong."
    static let msgTimeOut          = "Request timed out."
    static let msgCheckConnection  = "Please check your connection and try again."
    static let msgConnectionLost   = "Network connection lost."
    static let Key_Alert           = ""
    static let couldNotConnect     = "Could not able to connect with server. Please try again."
    let networkAlertMessage   = "Please Check Internet Connection"
}


struct params {
    static let kusername  = "username"
    static let kemail  = "email"
    static let kphone_number  = "phone_number"
    static let kpassword  = "password"
    static let kein  = "ein"
    static let kbusiness_type  = "business_type"
    static let kaddress  = "address"
    static let kwebsite  = "website"
    static let kcompany_description  = "company_description"
    static let kcategorys  = "categorys"
    static let kcompany_name  = "company_name"
    static let kcountry_code  = "country_code"
    static let kmobile_otp  = "mobile_otp"
    
    //
    //login
    static let kfcm_token  = "fcm_token"
    static let kdevice_type  = "device_type"
    static let kuser_id  = "user_id"
    static let kimage  = "image"
    static let kcompany_image  = "company_image"
    
    static let kstart  = "start"
    static let kcategory_id  = "category_id"
    static let ksearch_keyword  = "search_keyword"
    static let kproduct_name  = "product_name"
    static let kcategory  = "category"
    static let kproduct_detail = "product_detail"
    static let kproduct_description = "product_description"
    
    static let kid = "id"
    static let ktype = "type"
    static let ksort_filter = "sort_filter"
    
    static let kto_user = "to_user"
    static let krate = "rate"
    static let kreview_title = "review_title"
    static let kcompany_id = "company_id"
    static let kreview_description = "review_description"
    static let kproduct_id = "product_id"
    static let kcustomer_id = "customer_id"
    static let kuser_type = "user_type"
    static let kplan_id = "plan_id"
    static let kstripeToken = "stripeToken"
    static let krate_id = "rate_id"
    static let ktitle = "title"
    static let kdetail = "detail"
    static let kdescription = "description"
    static let kticket_id = "ticket_id"
    static let kcomment = "comment"
}



