//
//  FontManager.swift
//  PTEMaster
//
//  Created by mac on 17/08/20.
//  Copyright © 2020 CTIMac. All rights reserved.
//

import Foundation
import UIKit

let FontRegular = "Roboto-Regular"
let FontBold = "Roboto-Bold"




enum Fonts : Int {
    
    case normal = 1
    case bold = 2
    
    public func font(WithSize size : CGFloat) -> UIFont {
        switch self {
        case .normal:
           return UIFont.regularFont(size: size)
        case .bold:
            return UIFont.boldFont(size: size)

        }
    }
}

extension UIFont {
    
    static func regularFont( size:CGFloat ) -> UIFont{
        return  UIFont(name: FontRegular , size: size)!
    }
    
    static func boldFont( size:CGFloat ) -> UIFont{
        return  UIFont(name: FontBold , size: size)!
    }
    
    
}
/*


protocol CustomisableOutlet {
    var custom_font : Int { get set }
    var rds_widthPerc : CGFloat { get set }
    var rds_heightPerc : CGFloat { get set }
}

extension CustomisableOutlet where Self: UIView {
    func setRadiusIfNeeded() {
        if self.rds_widthPerc != 0 {
            self.layer.cornerRadius = self.bounds.width * rds_widthPerc/100
        } else if self.rds_heightPerc != 0 {
            self.layer.cornerRadius = self.bounds.height * rds_heightPerc/100
        }
    }
}


class CustomLabel: UILabel, CustomisableOutlet {
    
    @IBInspectable var rds_widthPerc: CGFloat = 0.0
    
    @IBInspectable var rds_heightPerc: CGFloat = 0.0
    
    @IBInspectable var custom_font : Int = 0 {
        didSet {
            self.font = Fonts.init(rawValue: custom_font)?.font(WithSize: self.font.pointSize)
        }
    }
   
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.setRadiusIfNeeded()
    }
}


class CustomTextfield: UITextField, CustomisableOutlet {
    
    @IBInspectable var rds_widthPerc: CGFloat = 0.0
    
    @IBInspectable var rds_heightPerc: CGFloat = 0.0
    
    @IBInspectable var custom_font : Int = 0 {
        didSet {
            self.font = Fonts.init(rawValue: custom_font)?.font(WithSize: self.font?.pointSize ?? 0)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.setRadiusIfNeeded()
    }
}

class CustomButton: UIButton, CustomisableOutlet {
    
    @IBInspectable var rds_widthPerc: CGFloat = 0.0
    
    @IBInspectable var rds_heightPerc: CGFloat = 0.0
    
    @IBInspectable var custom_font : Int = 0 {
        didSet {
            self.titleLabel?.font = Fonts.init(rawValue: custom_font)?.font(WithSize: self.titleLabel?.font?.pointSize ?? 0)
        }
    }//button.titleLabel!.font
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.setRadiusIfNeeded()
    }
}


*/



extension UILabel {
    @IBInspectable  var CustomFont: Int  {
        get {
            return self.CustomFont
        }
        set {
            self.font = Fonts.init(rawValue: newValue)?.font(WithSize: self.font.pointSize)
        }
    }
    
    @IBInspectable
    var letterSpace: CGFloat {
        set {
            let attributedString: NSMutableAttributedString!
            if let currentAttrString = attributedText {
                attributedString = NSMutableAttributedString(attributedString: currentAttrString)
            } else {
                attributedString = NSMutableAttributedString(string: text ?? "")
                text = nil
            }
            attributedString.addAttribute(NSAttributedString.Key.kern,
                                          value: newValue,
                                          range: NSRange(location: 0, length: attributedString.length))
            attributedText = attributedString
        }
        
        get {
            if let currentLetterSpace = attributedText?.attribute(NSAttributedString.Key.kern, at: 0, effectiveRange: .none) as? CGFloat {
                return currentLetterSpace
            } else {
                return 0
            }
        }
    }
}

extension UITextField {
    @IBInspectable  var CustomFont: Int  {
            get {
                return self.CustomFont
            }
        set {
            self.font = Fonts.init(rawValue: newValue)?.font(WithSize: self.font?.pointSize ?? 0)
        }
    }
}

extension UIButton {
    @IBInspectable  var CustomFont: Int  {
            get {
                return self.CustomFont
            }
        set {
            self.titleLabel?.font = Fonts.init(rawValue:  newValue)?.font(WithSize: self.titleLabel?.font?.pointSize ?? 1)
        }
    }
}
