//
//  HomeVC.swift
//  IRatePro
//
//  Created by mac on 11/01/21.
//

import UIKit

class PlainDetailVC: UIViewController {

    @IBOutlet weak var tblPlans: UITableView!
    
    var  arrPaymentHistory   = [PaymentHistory]()
    var page:Int = 0
    var isDownloading = false
    var pointContentOffset = CGPoint.zero
    
    override func viewDidLoad() {
        super.viewDidLoad()
        page = 0
        call_getPaymenHistory_API()
    }
    
    
    
}


extension PlainDetailVC : UITableViewDelegate , UITableViewDataSource  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return  arrPaymentHistory.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlainDetailCell", for: indexPath) as! PlainDetailCell
        let dict = arrPaymentHistory[indexPath.row]
        cell.lblpriceYear.text =  "$" + dict.price
        cell.lblticket.text = dict.title
        cell.lblTool.text = dict.paymentHistoryDescription
        cell.lblregister.text = dict.createdAt
        cell.viewMember.layer.cornerRadius = 12
        cell.viewMember.layer.maskedCorners = [ .layerMaxXMinYCorner]
        
        if indexPath.row == (arrPaymentHistory.count - 1) && (tblPlans.contentOffset.y > pointContentOffset.y) {
            if !isDownloading {
                isDownloading = true
                if arrPaymentHistory.count % 10 == 0 {
                    page += 10
                    self.call_getPaymenHistory_API()
                }
            }
        }
        
        return cell
    }

    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    
    func call_getPaymenHistory_API(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
        param[params.kstart] =  page
        ServerManager.shared.POST(url: ApiAction.getPaymenHistory , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            
           guard  let obj = try? JSONDecoder().decode(PaymentHistoryModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                
                
                if obj.paymentHistory.count != 0{
                    self.arrPaymentHistory.append(contentsOf: obj.paymentHistory)
                }
                self.tblPlans.reloadData()
                
                if obj.paymentHistory.count == 0{
                    self.isDownloading = true
                }else{
                    self.isDownloading = false
                    }
            } else {
                print("failure")
                self.isDownloading = true
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
}

//---------------------------------------------


class PlainDetailCell: UITableViewCell {
    @IBOutlet weak var lblMembership: UILabel!
    @IBOutlet weak var lblpriceYear: UILabel!
    @IBOutlet weak var lblticket: UILabel!
    @IBOutlet weak var lblTool: UILabel!
    @IBOutlet weak var lblregister: UILabel!
    @IBOutlet weak var btnChoosePlan: UIButton!
    @IBOutlet weak var viewMember: UIView!
    
   
}



// MARK: - PaymentHistoryModel
struct PaymentHistoryModel: Codable {
    let success, message: String
    let paymentHistory: [PaymentHistory]

    enum CodingKeys: String, CodingKey {
        case success, message
        case paymentHistory = "payment_history"
    }
}

// MARK: - PaymentHistory
struct PaymentHistory: Codable {
    let id, userID, planID, title: String
    let paymentHistoryDescription, price, days, status: String
    let type, createdAt: String

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case planID = "plan_id"
        case title
        case paymentHistoryDescription = "description"
        case price, days, status, type
        case createdAt = "created_at"
    }
}
