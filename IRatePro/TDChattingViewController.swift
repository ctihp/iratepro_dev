//
//  TDChattingViewController.swift
//  Traydi
//
//  Created by mac on 09/01/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import UIKit
import UserNotifications
import SDWebImage
import IQKeyboardManagerSwift

class TDChattingViewController: UIViewController ,UIScrollViewDelegate {
    @IBOutlet weak var tVChatting: UITableView!
    @IBOutlet weak var tvMsg: UITextView!
    @IBOutlet weak var tvConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var viewmsgBottom: NSLayoutConstraint!
    var arrDocument = [[String:Any]]()
    var myId = ""
    
    //
    @IBOutlet weak var imgToProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var viewRating: FloatRatingView!
    @IBOutlet weak var lblRatingCount: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    var arrCommentList = [CommentList]()
   var  isMoveButtom = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared.disabledToolbarClasses = [TDChattingViewController.self]
      //  SocketIOManager.sharedInstance.connectSocket()
      //  SocketIOManager.sharedInstance.establishConnection()
        // Do any additional setup after loading the view.
         //self.viewmsgBottom.constant -= (216 + 45)
        tvMsg.textColor = .gray
        tvMsg.delegate = self
     //    getAllChat()
     //   self.setupNotificationkeyboard()
        self.registerNib(tableview: tVChatting)
       // receiveChatData()
        
      //  let custombar = self.tabBarController as! CustomTabBarController
          //    custombar.isHiddenTabBar(hidden: true)
        call_ticketDetail_API()
    }
    
    

    
    
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
      }
    
    override func viewWillDisappear(_ animated: Bool) {
  //   SocketIOManager.sharedInstance.closeConnection()
     }
      
    
    func registerNib(tableview:UITableView) {
        tableview.register(UINib(nibName: "TDChattingSenderTextTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "TDChattingSenderTextTableViewCell")
        tableview.register(UINib(nibName: "TDChattingRecevierTextTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "TDChattingRecevierTextTableViewCell")
    }
    
    func setupNotificationkeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
         print("keyboardWillShow")
        scrollToBottom()
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        UIView.animate(withDuration: 1.0,
        delay: 0.0,
        options: [],
        animations: {
            self.viewmsgBottom.constant -= (keyboardFrame.size.height )
            self.tVChatting.frame.size.height = self.tVChatting.frame.height - keyboardFrame.size.height
        },
        completion: nil)
        
       /* UIView.animate(withDuration: 0.0) {
            self.viewmsgBottom.constant -= (keyboardFrame.size.height )
            print(self.viewmsgBottom.constant)
            self.tVChatting.frame.size.height = self.tVChatting.frame.height - keyboardFrame.size.height
        }*/
        
    }

    @objc func keyboardWillHide(notification: NSNotification){
         print("keyboardWillHide")
        UIView.animate(withDuration: 0.0) {
            self.viewmsgBottom.constant = 0
            self.tVChatting.frame.size.height = 0
        }
    }

  

    
    @IBAction func actionSendMessage(_ sender: Any) {
        if tvMsg.text == "" {
            return
        }
        
        call_addTicketComment_API()
        
       
        
    }
    
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension TDChattingViewController : UITableViewDelegate , UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  arrCommentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let obj = arrCommentList[indexPath.row]
        
        if AppDataHelper.shard.logins.userinfo.userID == obj.user_id {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TDChattingSenderTextTableViewCell", for: indexPath) as! TDChattingSenderTextTableViewCell
               cell.lblMsg.text = obj.comment?.decodeEmoji
                return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TDChattingRecevierTextTableViewCell", for: indexPath) as! TDChattingRecevierTextTableViewCell
           cell.lblMsgReciver.text = obj.comment?.decodeEmoji
            return cell
        }
    }
    
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 44)
        let lableView = UILabel()
        let lblx = (headerView.frame.size.width / 2)
       
        lableView.frame = CGRect(x: lblx - 50 , y: 5, width: 100, height: headerView.frame.height - 10)
        lableView.backgroundColor = .white
      //  lableView.text = "Today"
        lableView.textAlignment = .center
        lableView.font = UIFont.boldSystemFont(ofSize: 12)
        lableView.layer.cornerRadius = 17
        lableView.clipsToBounds = true
        headerView.addSubview(lableView)
      
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
     
}

extension TDChattingViewController:UITextViewDelegate  {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if tvMsg.textColor ==  .gray {
            tvMsg.textColor = .black
            tvMsg.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if tvMsg.text.isEmpty {
            tvMsg.textColor = .gray
            tvMsg.text = "Write your message..."
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let textHeight = textView.contentSize.height
        if textHeight < 120 {
            tvConstraintHeight.constant = textHeight
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func scrollToBottom(){
        DispatchQueue.main.async {
            if self.arrCommentList.count > 0{
            let indexPath = IndexPath(row: self.arrCommentList.count-1, section: 0)
            self.tVChatting.scrollToRow(at: indexPath, at: .bottom, animated: false)
        }
        }
    }

    
    func call_ticketDetail_API( ){
        var param = [String : Any]()
        param[params.kid] = myId
        ServerManager.shared.POST(url: ApiAction.ticketDetail  , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
           guard  let dict = try? JSONDecoder().decode(TicketDetailModel.self, from: data) else {
                return
            }
         
            if dict.success == ResponseApis.KSuccess {
                print("success")
                let obj = dict.ticketDetail
                let obj2 = dict.ratingDetail
                
                self.lblDate.text = obj.createdAt
                self.lblTitle.text = obj.title
                self.lblDescription.text = obj.ticketDetailDescription
                self.lblDetail.text = obj.detail
                self.viewRating.rating = Double(obj.rate) ?? 0
                self.lblRatingCount.text  = obj.rate + "/5"
             if obj2.userType == "1"{
                self.lblName.text = obj2.fromUsername
                self.imgToProfile.sd_setImage(with: URL(string: obj2.fromProfileImage ), placeholderImage: UIImage(named: "user"))
             }else{
                self.lblName.text = obj2.fromCompanyName
                self.imgToProfile.sd_setImage(with: URL(string: obj2.fromCompanyProfileImage ), placeholderImage: UIImage(named: "user"))
             }
                self.call_getComments_API()
                
            
            } else {
                print("failure")
            }
        }
        
    }
    
    func call_getComments_API( ){
        var param = [String : Any]()
        param[params.kticket_id] = myId
        ServerManager.shared.POST(url: ApiAction.getComments  , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
           guard  let obj = try? JSONDecoder().decode(CommentModel.self, from: data) else {
                return
            }
        
            if obj.success == ResponseApis.KSuccess {
                self.arrCommentList.removeAll()
                self.arrCommentList = obj.commentList!
                self.tVChatting.reloadData()
                if  self.isMoveButtom{
                self.scrollToBottom()
                }
                self.isMoveButtom = true
            } else {
                print("failure")
            }
        }
     
        
    }
    func call_addTicketComment_API( ){
        var param = [String : Any]()
        param[params.kticket_id] = myId
        param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
        param[params.kcomment] = tvMsg.text
        
        ServerManager.shared.POST(url: ApiAction.addTicketComment  , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
           guard  let obj = try? JSONDecoder().decode(OtpModel.self, from: data) else {
                return
            }
        
            if obj.success == ResponseApis.KSuccess {
                print("success")
                
                self.tvMsg.text = ""
                self.tvConstraintHeight.constant = 50
                self.call_getComments_API()
            } else {
                print("failure")
            }
        }
        
    }
    
    
    
    
    
    
    
}
/*
extension TDChattingViewController : ApiManagerDelegate {
   
    
    func getAllChat(){
        APIManager.share.delegate = self
         var params = [String:Any]()
        
        params[ApiConstants.key.chatMessage.ksender_id] = AppSignleHelper.shard.login.userinfo?.id
        params[ApiConstants.key.chatMessage.kreceiver_id] = dictSearch?.id
        let apiAction = ApiAction.TradersAndTradees.kmessage_list //
         APIManager.share.performMaltipartPost(params: params , apiAction: apiAction)
    }
    
    func receiveChatData(){
        let id  =  AppSignleHelper.shard.login.userinfo?.id // dictSearch?.id ?? ""
        print("-\(id!)-")
        print("ReceiverById\(id!)")
        // on recive
        SocketIOManager.sharedInstance.defaultSocket.on("ReceiverById\(id!)") { (receiveData, ask) in
            print("receiveData-\(receiveData)-")
            if receiveData.count > 0 {
                if   let dict =  receiveData[0] as? [String:Any]{
                    var cc = chatModal()
                   
                    cc.sender = dict["sender"] as? String ?? ""
                    cc.receiver = dict["receiver"] as? String ?? ""
                    cc.message = dict["message"] as? String ?? ""
                    cc.sender_image = dict["sender_image"] as? String ?? ""
                     cc.receiver_image = dict["receiver_image"] as? String ?? ""
                     cc.receiver_name = dict["receiver_name"] as? String ?? ""
                     cc.sender_name = dict["sender_name"] as? String ?? ""
                     cc.attachment = dict["attachment"] as? String ?? ""
                     cc.image = dict["image"] as? String ?? ""
                    self.arrChatMsg.append(cc)
                    
                    let indexPath = IndexPath.init(row: self.arrChatMsg.count - 1, section: 0)
                    self.tVChatting.insertRows(at: [indexPath], with: .bottom)
                    self.tVChatting.scrollToRow(at: indexPath, at: .bottom, animated: true)
                }
            }
        }
    }
    
   /* func successResponse(response: Any, check: String) {
        Loader.hideLoader()
        guard let data = response as? Data else {
            return
        }
        if check == ApiAction.TradersAndTradees.kmessage_list {
            
            guard let dataFetcher = try? JSONDecoder().decode(chat.self, from:data) else { return }
            if dataFetcher.code == ApiResponseStatus.kAPI_Result_Success {
                if dataFetcher.users!.count > 0 {
                     arrChatMsg = dataFetcher.users!
                    tVChatting.reloadData()
                    scrollToBottom()
                }
            }
        }else  if check == ApiAction.TradersAndTradees.ksend_image {
           do {
                           let resultObject = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                           let code  = resultObject["code"] as? String
                           let message = resultObject["message"] as? String
                           let  image = resultObject["image"] as? String
                           if code == ApiResponseStatus.kAPI_Result_Success {
                            var params = [String:Any]()
                            let userInfo = AppSignleHelper.shard.login.userinfo
                            params["id"] = ""
                            params["sender"] = userInfo?.id
                            params["receiver"] = dictSearch?.id
                            params["message"] = ""
                            params["created_at"] = ""
                            params["receiver_image"] = (dictSearch?.image)!
                            params["receiver_name"] =  (dictSearch?.full_name)!
                            params["sender_image"] =  userInfo!.image!
                            params["sender_name"] = userInfo?.full_name
                            params["type"] = "2"
                            params["image"] = image
                            
                            print("Send message by me\(params)")
                            //emit (send)
                            SocketIOManager.sharedInstance.defaultSocket.emit("SenderById" , with: [params])
                            var cc = chatModal()
                            cc.sender = userInfo?.id // my id
                            cc.image = image
                            cc.type = "2"
                            arrChatMsg.append(cc)
                            let indexPath = IndexPath.init(row: arrChatMsg.count - 1, section: 0)
                            tVChatting.insertRows(at: [indexPath], with: .bottom)
                            tVChatting.scrollToRow(at: indexPath, at: .bottom, animated: false)
                           // tvMsg.text = ""
                           // tvConstraintHeight.constant = 37
                             self.view.layoutIfNeeded()
                           } else {
                               Util.showAlertWithCallback(AppName, message: message, isWithCancel: false)
                           }
                           
                       } catch {
                           print("Unable to parse JSON response")
                       }
        }
        
    }*/
  
       
       func errorResponse(error: Any, check: String) {
           Loader.hideLoader()
           guard let errormsg = error as? Error else {
               return
           }
         //  Util.showAlertWithCallback(AppName, message: errormsg.localizedDescription , isWithCancel: false)
       }
}*/


extension String {
    var decodeEmoji: String{
        let data = self.data(using: String.Encoding.utf8);
        let decodedStr = NSString(data: data!, encoding: String.Encoding.nonLossyASCII.rawValue)
        if let str = decodedStr{
            return str as String
        }
        return self
    }
    var encodeEmoji: String{
        if let encodeStr = NSString(cString: self.cString(using: .nonLossyASCII)!, encoding: String.Encoding.utf8.rawValue){
            return encodeStr as String
        }
        return self
    }
}





struct CommentModel : Codable {
    let success : String?
    let message : String?
    let commentList : [CommentList]?

    enum CodingKeys: String, CodingKey {

        case success = "success"
        case message = "message"
        case commentList = "commentList"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(String.self, forKey: .success)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        commentList = try values.decodeIfPresent([CommentList].self, forKey: .commentList)
    }

}
struct CommentList : Codable {
    let id : String?
    let user_id : String?
    let ticket_id : String?
    let comment : String?
    let created_at : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_id = "user_id"
        case ticket_id = "ticket_id"
        case comment = "comment"
        case created_at = "created_at"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        ticket_id = try values.decodeIfPresent(String.self, forKey: .ticket_id)
        comment = try values.decodeIfPresent(String.self, forKey: .comment)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
    }

}
