//
//  EnterOTPVC.swift
//  IRatePro
//
//  Created by mac on 20/01/21.
//

import UIKit

class EnterOTPVC: UIViewController {

    
    var cCode = ""
    var MobileNumbwer = ""
    var ISComeFromForgot = false
    
    @IBOutlet weak var txtFd1: UITextField!
    @IBOutlet weak var txtFd2: UITextField!
    @IBOutlet weak var txtFd3: UITextField!
    @IBOutlet weak var txtFd4: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtFd1.becomeFirstResponder()
        if ISComeFromForgot{
            
            call_getOtpForgot_API()
        }else{
          //  call_getOtp_API()
        }
    }
    

    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func actionVerify(_ sender: Any) {
        let valid = validation()
        if valid.status {
            
            call_otp_verificationAPI()
        }else{
            presentAlert("", msgStr: valid.message, controller: self)
        }
        
    }
    
    
    func validation() -> (status:Bool, message:String) {
        var msg:String = ""
        var status:Bool = true
        
       if txtFd1.text == "" || txtFd2.text == "" || txtFd3.text == "" || txtFd4.text == "" {
            status = false
            msg = Validation.kEnterPassword.rawValue
        }
        return (status:status, message:msg)
        
        }
    
    
    func call_getOtpForgot_API(){
        var param = [String : Any]()
        param[params.kphone_number] = MobileNumbwer
        param[params.kcountry_code] = cCode
        ServerManager.shared.POST(url: ApiAction.otp_verification_forgot , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
           guard  let obj = try? JSONDecoder().decode(SignupModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
//               let vc = EnterOTPVC.instance(storyBoard: .Signup) as! EnterOTPVC
//               self.navigationController?.pushViewController(vc, animated: true)
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    
    

    
    
    func call_otp_verificationAPI(){
        var param = [String : Any]()
        param[params.kphone_number] = MobileNumbwer
        param[params.kcountry_code] = cCode
        param[params.kmobile_otp] = txtFd1.text!  + txtFd2.text!  + txtFd3.text!  + txtFd4.text!
        
        
        ServerManager.shared.POST(url: ApiAction.check_verification , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
           guard  let obj = try? JSONDecoder().decode(OtpModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                
                DispatchQueue.main.async(execute: {
                    let _ = presentAlertWithOptions("", message: obj.message, controller: self, buttons: ["Ok"]) { (alert, actionTag) in
                        if actionTag == 0 {
                            if  self.ISComeFromForgot{
                                let vc = ForgotPasswordVC.instance(storyBoard: .Signup) as! ForgotPasswordVC
                                vc.cCode  = self.cCode
                                vc.MobileNumbwer = self.MobileNumbwer
                               
                                self.navigationController?.pushViewController(vc, animated: true)
                            }else{
                                let vc = SignUpVC.instance(storyBoard: .Signup) as! SignUpVC
                                vc.cCode  = self.cCode
                                vc.MobileNumbwer = self.MobileNumbwer
                               
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            //Ok Button
                           
                            
                            
                        } else {
                            //Cancel Button
                        }
                    }
                })
                
               
                
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
        
    }

}




extension EnterOTPVC:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        let strr:NSString = textField.text! as NSString
       let str2 = strr.replacingCharacters(in: range, with: string)  as NSString
        let length = (textField.text?.count)! + string.count - range.length
        
        if textField == txtFd1 {
            if str2 == ""{
                txtFd1.text = ""
                return false
            }
            let str = String(String(str2).last!)
            txtFd1.text = ""
            txtFd1.text =  str//String(str)
            txtFd2.becomeFirstResponder()
            return false
        }
       if textField == txtFd2 {
           if str2 == ""{
               txtFd2.text = ""
               txtFd1.becomeFirstResponder()
               return false
           }
           let str = String(String(str2).last!)
           txtFd2.text = ""
           txtFd2.text =  str//String(str)
           txtFd3.becomeFirstResponder()
           return false
       }
        if textField == txtFd3 {
            if str2 == ""{
                txtFd3.text = ""
                txtFd2.becomeFirstResponder()
                return false
            }
            let str = String(String(str2).last!)
            txtFd3.text = ""
            txtFd3.text =  str//String(str)
            txtFd4.becomeFirstResponder()
            return false
        }
        if textField == txtFd4 {
            if str2 == ""{
                txtFd4.text = ""
                txtFd3.becomeFirstResponder()
                return false
            }
            let str = String(String(str2).last!)
            txtFd4.text = ""
            txtFd4.text =  str//String(str)
            textField.becomeFirstResponder()
            return false
        }
        
     
        
        return false
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        if textField == txtFd4{
            textField.resignFirstResponder()
        }
        return true
    }
   
    

}



struct OtpModel: Codable {
    let success,  message: String
}

struct ModelNotifiCount: Codable {
    let success, count ,  message: String
}




