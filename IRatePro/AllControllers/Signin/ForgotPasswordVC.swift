//
//  ForgotPasswordVC.swift
//  IRatePro
//
//  Created by mac on 20/01/21.
//

import UIKit

class ForgotPasswordVC: UIViewController {
    
    
    @IBOutlet weak var tfNewPassword: FloatingLabelInput!
    @IBOutlet weak var tfConfirmPassword: FloatingLabelInput!
    var cCode = ""
    var MobileNumbwer = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    


    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func actionSend(_ sender: Any) {
        
        let valid = validation()
        if valid.status {
            
            call_forgot_password_API()
        }else{
            presentAlert("", msgStr: valid.message, controller: self)
        }
    }
    
    
    
    func validation() -> (status:Bool, message:String) {
        var msg:String = ""
        var status:Bool = true
        
        if let fullname = tfNewPassword.text , fullname.isEmpty {
            status = false
            msg = Validation.kEnterPassword.rawValue
        }
        else if tfNewPassword.text != tfConfirmPassword.text{
            status = false
            msg = Validation.kPassowrdNotMatched.rawValue
        }
        return (status:status, message:msg)
        
        }
    
    func call_forgot_password_API(){
        var param = [String : Any]()
        param[params.kphone_number] = MobileNumbwer
        param[params.kpassword] = tfNewPassword.text
        param[params.kcountry_code] = cCode
        param[params.kfcm_token] = ""
        param[params.kdevice_type] = "Ios"
        ServerManager.shared.POST(url: ApiAction.reset_passowrd , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
           guard  let obj = try? JSONDecoder().decode(SignupModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                
                DispatchQueue.main.async(execute: {
                    let _ = presentAlertWithOptions("", message: obj.message!, controller: self, buttons: ["Ok"]) { (alert, actionTag) in
                        if actionTag == 0 {
                            //Ok Button
                            for controller in self.navigationController!.viewControllers as Array {
                                if controller.isKind(of: LoginVC.self) {
                                    self.navigationController!.popToViewController(controller, animated: true)
                                    break
                                }
                            }
                            
                            
                        } else {
                            //Cancel Button
                        }
                    }
                })
                
                
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
        
    }
    

}
