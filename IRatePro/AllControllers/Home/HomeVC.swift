//
//  HomeVC.swift
//  IRatePro
//
//  Created by mac on 11/01/21.
//

import UIKit

class HomeVC: UIViewController {

    @IBOutlet weak var tblPlans: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    var  arrmonthlyPlanList   = [LyPlanList]()
    var  arryearlyPlanList   = [LyPlanList]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        let font = UIFont.boldFont(size: 15)
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
        NotificationCenter.default.removeObserver(self, name: .membership, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: .membership, object: nil)
        
        NotificationCenter.default.removeObserver(self, name: .planDetail, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification2(notification:)), name: .planDetail, object: nil)
        
        
     //
        
       
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.navigationController?.setNavigationBarHidden(true, animated: true)
        let custombar = self.tabBarController as! CustomTabBarController
        custombar.isHiddenTabBar(hidden: false)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         self.navigationController?.setNavigationBarHidden(true, animated: true)
        let custombar = self.tabBarController as! CustomTabBarController
        custombar.isHiddenTabBar(hidden: false)
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        let custombar = self.tabBarController as? CustomTabBarController
                       custombar?.isHiddenTabBar(hidden: true)
    }
    
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        print("call observer2")
        call_getMembershipPlan_API()
    }
    @objc func methodOfReceivedNotification2(notification: Notification) {
        let vc2 = TicketListingVC.instance(storyBoard: .Review) as! TicketListingVC
        self.navigationController!.pushViewController(vc2, animated: true)
    }

    deinit {
        print("deinit")
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBAction func actionSegment(_ sender: Any) {
        tblPlans.reloadData()
    }
    
    @IBAction func actionMyMembershipPlan(_ sender: Any) {
        let vc = PlainDetailVC.instance(storyBoard: .Favourite) as! PlainDetailVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


extension HomeVC : UITableViewDelegate , UITableViewDataSource , ChoosePlanDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentedControl.selectedSegmentIndex == 0{
           return  arrmonthlyPlanList.count
        }else{
            return  arryearlyPlanList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath) as! HomeCell
        if segmentedControl.selectedSegmentIndex == 0{
            let dict = arrmonthlyPlanList[indexPath.row]
            cell.lblpriceYear.text =  "$" +  dict.price
            
        }else{
            let dict = arryearlyPlanList[indexPath.row]
            cell.lblpriceYear.text = "$" + dict.price
        }
        cell.viewMember.layer.cornerRadius = 12
        cell.viewMember.layer.maskedCorners = [ .layerMaxXMinYCorner]
        cell.delegate = self
        return cell
    }
    
    func actionChoosePlan(cell: HomeCell) {
       guard let indexPath = tblPlans.indexPath(for: cell) else {
             return
         }
       var planId = ""
        if segmentedControl.selectedSegmentIndex == 0{
            let dict = arrmonthlyPlanList[indexPath.row]
            planId = dict.id
        }else{
            let dict = arryearlyPlanList[indexPath.row]
            planId = dict.id
        }
        let vc = PaymentVC.instance(storyBoard: .Favourite) as! PaymentVC
        vc.planId = planId
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    
    
    
}

//---------------------------------------------

protocol ChoosePlanDelegate:NSObject {
    func actionChoosePlan(cell:HomeCell)
}

class HomeCell: UITableViewCell {
    @IBOutlet weak var lblMembership: UILabel!
    @IBOutlet weak var lblpriceYear: UILabel!
    @IBOutlet weak var lblticket: UILabel!
    @IBOutlet weak var lblTool: UILabel!
    @IBOutlet weak var lblregister: UILabel!
    @IBOutlet weak var btnChoosePlan: UIButton!
    @IBOutlet weak var viewMember: UIView!
    weak var delegate: ChoosePlanDelegate?
    
    @IBAction func actionChoosePlan(_ sender: Any) {
        delegate?.actionChoosePlan(cell: self)
    }
    

}

extension HomeVC: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func call_getMembershipPlan_API(){
        let param = [String : Any]()
        ServerManager.shared.POST(url: ApiAction.getMembershipPlan , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            
           guard  let obj = try? JSONDecoder().decode(MembershipListModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                self.arrmonthlyPlanList  = obj.monthlyPlanList
                self.arryearlyPlanList = obj.yearlyPlanList
                self.tblPlans.reloadData()
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
        
    }
    
}


// MARK: - MembershipListModel
struct MembershipListModel: Codable {
    let success, message: String
    let monthlyPlanList, yearlyPlanList: [LyPlanList]
}

// MARK: - LyPlanList
struct LyPlanList: Codable {
    let id, title, lyPlanListDescription, price: String
    let days, planType, status, createdAt: String

    enum CodingKeys: String, CodingKey {
        case id, title
        case lyPlanListDescription = "description"
        case price, days
        case planType = "plan_type"
        case status
        case createdAt = "created_at"
    }
}

