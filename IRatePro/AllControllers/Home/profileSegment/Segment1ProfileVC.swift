//
//  Segment1ProfileVC.swift
//  IRatePro
//
//  Created by mac on 18/01/21.
//

import UIKit
import SDWebImage
import TagListView

class Segment1ProfileVC: UIViewController, TagListViewDelegate {
    @IBOutlet weak var viewBg: UIView!

    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var ratingView: FloatRatingView!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblMobileNumber: UILabel!
    
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblEinNumber: UILabel!
    @IBOutlet weak var lblBusniessType: UILabel!
    @IBOutlet weak var lblWebsiteUrl: UILabel!
    @IBOutlet weak var lblCompanyDiscriprition: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    var userinfo : UserinfoProfile?
    @IBOutlet weak var tagListView: TagListView!
    var strSkill = [String]()
    
    var arrCategory = [CategoryList]()
    var comeFromCustomerProfile = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBg.layer.cornerRadius = 42
        viewBg.layer.maskedCorners = [ .layerMinXMaxYCorner ]
       // NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: .providerProfile, object: nil)
        
        NotificationCenter.default.removeObserver(self, name: .providerProfile, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: .providerProfile, object: nil)

      /*  if comeFromCustomerProfile{
            let vc = EditProfilePro.instance(storyBoard: .Profile) as! EditProfilePro
            vc.userinfo = userinfo
            self.navigationController?.pushViewController(vc, animated: false)
        }*/
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
       
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        print("call observer")
        call_getProfile_Api()
    }
    
    /*override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default
         .removeObserver(self, name: .providerProfile, object: nil)
        
    }*/
    
    deinit {
        print("deinit")
        NotificationCenter.default.removeObserver(self)
    }
    
    
    
    
    
    @IBAction func actioneditProfile(_ sender: Any) {
        let vc = EditProfilePro.instance(storyBoard: .Profile) as! EditProfilePro
        vc.userinfo = userinfo
    self.navigationController?.pushViewController(vc, animated: true)
        
       }
    
    @IBAction func actionSetting(_ sender: Any) {
        let vc = SettingVC.instance(storyBoard: .Profile) as! SettingVC
                    self.navigationController?.pushViewController(vc, animated: true)
       }
    
    
    @IBAction func actionLogout(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let _ = presentAlertWithOptions("", message: "Sure to logout", controller: self, buttons: ["Ok" , "Cancel"]) { (alert, actionTag) in
                if actionTag == 0 {
                    self.call_Logout_Api()
                } else {
                    //Cancel Button
                }
            }
        })
        
       }
}

extension Segment1ProfileVC{
    func call_getProfile_Api(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
        ServerManager.shared.POST(url: ApiAction.getProfile , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
           guard  let obj = try? JSONDecoder().decode(GetProfileModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                self.setProfileData(obj.userinfo)
                self.userinfo = obj.userinfo
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    func call_Logout_Api(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
        ServerManager.shared.POST(url: ApiAction.logout , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
         
            let vc = LoginVC.instance(storyBoard: .Signup) as! LoginVC
               UIApplication.shared.windows.first?.rootViewController = vc
               UIApplication.shared.windows.first?.makeKeyAndVisible()
           // self.navigationController!.setViewControllers([LoginVC.instance(storyBoard: .Signup)], animated: true)
        }
    }
    
    func call_category_listAPI( ){
        let parameter = [String : Any]()
       
        ServerManager.shared.POST(url: ApiAction.category_list  , param: parameter, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
           guard  let obj = try? JSONDecoder().decode(CategoryModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                print("success")
                self.arrCategory.removeAll()
                self.arrCategory = obj.categoryList
                self.setProfileData(self.userinfo!)
            } else {
                print("failure")
            }
        }
        
    }
    
    
    func setProfileData(_ obj : UserinfoProfile ){
        lblUserName.text = obj.username
        lblName.text = obj.companyName
        lblEmail.text = obj.email
        lblMobileNumber.text = obj.phoneNumber
        ratingView.rating = Double(obj.avgRating) ?? 0.0
        
        let doubleValue : Double = Double(obj.avgRating) ?? 0.0
        lblRating.text = String(format:"%.1f", doubleValue)  + "/5"
        imgProfile.sd_setImage(with: URL(string: obj.company_profile_image ), placeholderImage: UIImage(named: "user"))
        
        lblCompanyName.text = obj.companyName
        lblEinNumber.text = obj.ein
        lblBusniessType.text = obj.businessType
        lblWebsiteUrl.text = obj.website
        lblCompanyDiscriprition.text = obj.companyDescription
        lblAddress.text = obj.address
        tagListView.removeAllTags()
        strSkill.removeAll()
        for dict2 in obj.category{
            strSkill.append(dict2.itemText)
            
        }
        tagListView.addTags(strSkill)
    }
   
    // MARK: TagListViewDelegate
func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
    print("Tag pressed: \(title), \(sender)")
    tagView.isSelected = !tagView.isSelected
}
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {

      
        sender.removeTagView(tagView)
    }
    
}


// MARK: - GetProfileModel
struct GetProfileModel: Codable {
    let success, message: String
    let userinfo: UserinfoProfile
}

// MARK: - Userinfo
struct UserinfoProfile: Codable {
    let userID, username, email, token: String
    let countryCode, phoneNumber, businessType, ein: String
    let actToken, address, fcmToken, gender: String
    let profileImage, company_profile_image: String
    let avgRating, ratingCount, companyName, companyDescription: String
    let website, status: String
    let category: [Category]
    let businessStatus, deviceType: String

    enum CodingKeys: String, CodingKey {
        case userID = "user_id"
        case username, email, token
        case countryCode = "country_code"
        case phoneNumber = "phone_number"
        case businessType = "business_type"
        case ein
        case actToken = "act_token"
        case address
        case fcmToken = "fcm_token"
        case gender
        case profileImage = "profile_image"
        case avgRating = "avg_rating"
        case ratingCount = "rating_count"
        case companyName = "company_name"
        case companyDescription = "company_description"
        case website, status, category
        case businessStatus = "business_status"
        case deviceType = "device_type"
        case company_profile_image = "company_profile_image"
    }
}

// MARK: - Category
struct Category: Codable {
    let itemID, itemText: String

    enum CodingKeys: String, CodingKey {
        case itemID = "item_id"
        case itemText = "item_text"
    }
}
