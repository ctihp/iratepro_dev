//
//  AllCompaniesVC.swift
//  IRatePro
//
//  Created by mac on 15/01/21.
//

import UIKit
import SDWebImage
import  SafariServices


class AllCompaniesVC: UIViewController ,btnUrlDelegate {
  
    
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var cvCategory: UICollectionView!
    @IBOutlet weak var tblResult: UITableView!
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var viewCategoryTopConst: NSLayoutConstraint!
    var isSelectRadius = 0
    var  arrUserList = [UserList]()
    var isSearch = false
    var searchKey = ""
    //
    var isComeFrom: Page = .none
    var page:Int = 0
    var isDownloading = false
    var pointContentOffset = CGPoint.zero
    var isPopToRoot = false

    var currentValue: CGFloat = 0

    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBG.layer.cornerRadius = 42
        viewBG.layer.maskedCorners = [ .layerMinXMaxYCorner ]
        
        tfSearch.addTarget(self, action: #selector(SearchDidChange(_:)), for: .editingChanged)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        arrUserList.removeAll()
        page = 0
        tfSearch.text = searchKey
        isDownloading = false
        call_user_list_API(searchKey)
        self.cvCategory.reloadData()
        
       
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let index = IndexPath(item: isSelectRadius, section: 0)
     //   self.cvCategory.reloadData()
        self.cvCategory.scrollToItem(at: IndexPath(row: index.item, section: 0), at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
    }
    
    @IBAction func actionFiltet(_ sender: Any) {
        let alertController = storyboard?.instantiateViewController(withIdentifier: "TDPopProjectVC") as! TDPopProjectVC
        alertController.modalPresentationStyle = .popover
       //  alertController.preferredContentSize = CGSize(width: 200, height: 200)
        let popover = alertController.popoverPresentationController
      //  popover?.permittedArrowDirections =  UIPopoverArrowDirection.init(rawValue: 0)
        popover?.delegate = self
        alertController.delegate = self
        popover?.sourceView = btnFilter
        popover?.permittedArrowDirections =  .up
        alertController.preferredContentSize = CGSize(width: 80, height: 80)
        self.present(alertController, animated: true, completion: nil)
     
    }

    @IBAction func actionBack(_ sender: Any) {
        isPopToRoot  = true
         self.navigationController?.popViewController(animated: true)
     }
    
  
    
    override func viewWillDisappear(_ animated: Bool) {
        if  !isPopToRoot{
            self.navigationController?.popToRootViewController(animated: false)
        }
    }
    

}
extension AllCompaniesVC: SFSafariViewControllerDelegate {
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        print("safari did finish")
    }
    
    func safariViewController(_ controller: SFSafariViewController, initialLoadDidRedirectTo URL: URL) {
        print("url\(URL.absoluteString)")
        let urlString = "https://www.google.co.in/"
        
        if URL.absoluteString == urlString {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                // do stuff 42 seconds later
                self.dismiss(animated: true) {
                    NotificationCenter.default.post(name: Notification.Name("popAndPush"), object: nil)
                    self.navigationController?.popViewController(animated: true)
                    
                }
            }
        }
    }
}

extension AllCompaniesVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  arrCategory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bestReviewCategoryCell", for: indexPath) as! bestReviewCategoryCell
        let dict = arrCategory[indexPath.row]
        cell.imgCategory.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imgCategory.sd_setImage(with: URL(string: dict.categoryImage ), placeholderImage: UIImage(named: "logo"))
        cell.lblCategoryName.text = dict.categoryName
        
        if  isSelectRadius == indexPath.row{
            cell.lblCategoryName.textColor =  .black
            cell.viewBg.backgroundColor =  UIColor(named: "SemiWhite")!
        }else{
            cell.viewBg.backgroundColor =  .clear
            cell.lblCategoryName.textColor =  UIColor(named: "SemiWhite")!
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        isSelectRadius = indexPath.row
        page = 0
        isDownloading = false
        tfSearch.text = ""
         //  cvCategory.reloadWithoutAnimation()
      
      self.cvCategory.reloadData()
      /*  self.cvCategory.scrollToItem(at: IndexPath(row: index.item, section: 0), at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)*/
        
        arrUserList.removeAll()
        call_user_list_API("")
    }
    func reloadRowsWithoutAnimation(at indexPaths: [IndexPath]) {
            let contentOffset = cvCategory.contentOffset
            UIView.setAnimationsEnabled(false)
        cvCategory.performBatchUpdates {
            cvCategory.reloadItems(at: indexPaths)
            }
            UIView.setAnimationsEnabled(true)
        cvCategory.setContentOffset(contentOffset, animated: false)
        }
    
}

extension UICollectionView {
    func reloadWithoutAnimation(){
        CATransaction.begin()
        CATransaction.setValue(kCFBooleanTrue, forKey: kCATransactionDisableActions)
        self.reloadData()
        CATransaction.commit()
    }
    
  
}


extension AllCompaniesVC : UITableViewDelegate , UITableViewDataSource{
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       arrUserList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ResultCell", for: indexPath) as! ResultCell
        cell.delegate = self
        let obj =  arrUserList[indexPath.row]
        cell.imgCategory.sd_imageIndicator = SDWebImageActivityIndicator.gray
        
        
        cell.imgCategory.sd_setImage(with: URL(string: obj.profile_image! ), placeholderImage: UIImage(named: "logo"))
        cell.lblCategoryName.text = obj.company_name
        cell.lblTotalReviews.text = "0" + " Review"
        cell.ratingCompany.rating = Double(obj.rating_count!) ?? 0
        cell.lbltotalRate.text =  "Rate Pro " + obj.rating_count!
        cell.lblDiscription.text = obj.company_description
        cell.btnUrl.setTitle(obj.website, for: .normal)
        cell.btnUrl.underline()
        
        if indexPath.row == (arrUserList.count - 1) && (tblResult.contentOffset.y > pointContentOffset.y) {
            if !isDownloading {
                isDownloading = true
                if arrUserList.count % 10 == 0 {
                    page += 10
                    self.call_user_list_API(tfSearch.text ?? "")
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = CompanyNameVC.instance(storyBoard: .Home) as! CompanyNameVC
        
        vc.dictUserList = arrUserList[indexPath.row]
     //   print(arrUserList[indexPath.row].user_id)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func actionUrl(cell: ResultCell) {
        guard let indexPath = tblResult.indexPath(for: cell) else {
              return
          }
        if  let obj = arrUserList[indexPath.row].website{
            
            if verifyUrl(urlString: obj) {
                let vc = SFSafariViewController(url: URL(string: obj)!)
                vc.delegate = self
                self.present(vc, animated: true, completion: nil)
            }
            
        }
    }
    
    
    func verifyUrl (urlString: String?) -> Bool {
       if let urlString = urlString {
           if let url = NSURL(string: urlString) {
               return UIApplication.shared.canOpenURL(url as URL)
           }
       }
       return false
   }
    
}




extension AllCompaniesVC : UIPopoverPresentationControllerDelegate ,TDPopProjectVCDelegate{
    func deleteIndex(buttontag: Int) {
        print(buttontag)
    }
    
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
}

extension AllCompaniesVC{
    func call_user_list_API(_ str : String){
        let categoryID = arrCategory[isSelectRadius].categoryID
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
        param[params.kstart] =  page
        param[params.kcategory_id] = categoryID
        param[params.ksearch_keyword] = str
        
        ServerManager.shared.POST(url: ApiAction.user_list  , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            
        guard  let obj = try? JSONDecoder().decode(CompanyListingModel.self, from: data) else {
                return
            }
            
            if obj.success == ResponseApis.KSuccess {
                print("success")
                if self.isSearch {
                    self.arrUserList.removeAll()
                    self.arrUserList = obj.userList!
                    self.isSearch = false
                    let index = IndexPath(item: self.isSelectRadius, section: 0)
                  self.cvCategory.scrollToItem(at: IndexPath(row: index.item, section: 0), at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
                }else{
                    if obj.userList?.count != 0{
                        self.arrUserList.append(contentsOf: obj.userList!)
                    }
                    
                }
               
                
             
                print("self.arrUserList --\(self.arrUserList.count)-")
              //  self.cvCategory.reloadData()
                self.tblResult.reloadData()
                
                if obj.userList?.count == 0{
                    self.isDownloading = true
                }else{
                    self.isDownloading = false
                    }
                if self.arrUserList.count == 0{
                      self.tblResult.displayBackgroundImageWithText(text: "Data not Found" , fontStyle: "", fontSize: 15, imgName: "cloud")
                  }else{
                    self.tblResult.displayBackgroundImageWithText(text: "" , fontStyle: "", fontSize: 15, imgName: "")
                  }
                
                
            } else {
                self.isDownloading = true
                print("failure")
            }
        }
        
    }
}

extension AllCompaniesVC:  UITextFieldDelegate , UIScrollViewDelegate {
    
    @objc func SearchDidChange(_ textField: UITextField) {
          let textToSearch = textField.text ?? ""
        print("textToSearch-=-=-=-=-=-=-=-=-=-=--\(textToSearch)")
        isSearch = true
        searchKey = ""
        page = 0
        isDownloading = false
        if textToSearch.count > 2{
            call_user_list_API(textToSearch)
        }else  if textToSearch.count == 0 {
            call_user_list_API("")
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
   
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let minValue: CGFloat = -240
        let maxValue: CGFloat = 0
        currentValue -= scrollView.contentOffset.y
        var stopScroll = true
        if currentValue <= minValue {
            currentValue = minValue
            stopScroll = false
        } else if currentValue >= maxValue {
            currentValue = maxValue
            stopScroll = false
        }
        viewCategoryTopConst.constant = currentValue
        if stopScroll {
            tblResult.contentOffset.y = 0
        }
    }
}


protocol btnUrlDelegate:NSObject {
    func actionUrl(cell:ResultCell)
}

class ResultCell : UITableViewCell{
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var ratingCompany: FloatRatingView!
    @IBOutlet weak var lblTotalReviews: UILabel!
    @IBOutlet weak var lbltotalRate: UILabel!
    @IBOutlet weak var btnUrl: UIButton!
    @IBOutlet weak var lblDiscription: UILabel!
    
    weak var delegate: btnUrlDelegate?
    
    @IBAction func actionUrl(_ sender: Any) {
        delegate?.actionUrl(cell: self)
    }
    
}







extension UIButton {
    func underline() {
        guard let text = self.titleLabel?.text else { return }
        let attributedString = NSMutableAttributedString(string: text)
        //NSAttributedStringKey.foregroundColor : UIColor.blue
        attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: self.titleColor(for: .normal)!, range: NSRange(location: 0, length: text.count))
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: self.titleColor(for: .normal)!, range: NSRange(location: 0, length: text.count))
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: text.count))
        self.setAttributedTitle(attributedString, for: .normal)
    }
}


