//
//  AllReviewsVC.swift
//  IRatePro
//
//  Created by mac on 10/02/21.
//

import UIKit

class AllReviewsVC: UIViewController {
    @IBOutlet weak var tblReview: UITableView!
    
    var arrRatingList = [RatingList]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblReview.reloadData()
        // Do any additional setup after loading the view.
    }
    

    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}



extension AllReviewsVC: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return arrRatingList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllRateListingCell", for: indexPath) as! AllRateListingCell
     
     let obj =    arrRatingList[indexPath.row]
        cell.lblToName.text = obj.toUsername
        cell.lbltitle.text = obj.reviewTitle  //to_profile_image
        cell.lbldiscription.text = obj.reviewDescription
        cell.imgTo.sd_setImage(with: URL(string: obj.toProfileImage ), placeholderImage: UIImage(named: "user"))
        cell.rating.rating = Double(obj.allRatingCount) ?? 0.0
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}








class AllRateListingCell : UITableViewCell{
    @IBOutlet weak var imgTo: UIImageView!
    @IBOutlet weak var lblToName: UILabel!
    @IBOutlet weak var rating: FloatRatingView!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lbldiscription: UILabel!
    
}

