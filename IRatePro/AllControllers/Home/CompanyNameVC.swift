//
//  CompanyNameVC.swift
//  IRatePro
//
//  Created by mac on 16/01/21.
//

import UIKit

class CompanyNameVC: UIViewController {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var viewBg: UIImageView!
    @IBOutlet weak var container: UIView!
    var  dictUserList : UserList?
    var isPopToRoot = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentedControl.selectedSegmentIndex = 0
        
        viewBg.layer.cornerRadius = 22
        viewBg.layer.maskedCorners = [ .layerMinXMaxYCorner ]
        print(dictUserList?.user_id)
        setupView()
    }
    
    private func setupView() {
        updateView()
    }
    
    private lazy var Segment0Comp: Segment0Company = {
        let vc = Segment0Company.instance(storyBoard: .Home) as! Segment0Company
        self.add(asChildViewController: vc)
        return vc
    }()

    private lazy var Segment1Prod: CompanyProductVC = {
        let viewController = CompanyProductVC.instance(storyBoard: .Profile) as! CompanyProductVC
        self.add(asChildViewController: viewController)
        return viewController
    }()
    
    private func add(asChildViewController viewController: UIViewController) {
        addChild(viewController)
        container.addSubview(viewController.view)
        viewController.view.frame = container.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParent: self)
    }
    
    
    private func remove(asChildViewController viewController: UIViewController) {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
    
    private func updateView() {
       if segmentedControl.selectedSegmentIndex == 0 {
            remove(asChildViewController: Segment1Prod)
            add(asChildViewController: Segment0Comp)
        //NotificationCenter.default.post(Notification(name: NSNotification.Name(rawValue: "honey")))
          NotificationCenter.default.post(Notification(name: .company, object: dictUserList, userInfo: nil))
       
        } else {
            remove(asChildViewController: Segment0Comp)
            add(asChildViewController: Segment1Prod)
            NotificationCenter.default.post(Notification(name: .productList, object: dictUserList, userInfo: nil))
        }
    }
    
  
    
    
    @IBAction func actionSegment(_ sender: Any) {
        updateView()
    }
    
    @IBAction func actionBack(_ sender: Any) {
        isPopToRoot  = true
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
         if  !isPopToRoot{
             self.navigationController?.popToRootViewController(animated: false)
         }
     }
    
    
}
