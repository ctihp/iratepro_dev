//
//  BestReviewVC.swift
//  IRatePro
//
//  Created by mac on 15/01/21.
//

import UIKit
import SDWebImage
import  UserNotifications
var arrCategory = [CategoryList]()
class BestReviewVC: UIViewController {
    
    
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var cvCategory: UICollectionView!
    // @IBOutlet weak var tblResult: UITableView!
    @IBOutlet weak var cvRecentReview: UICollectionView!
    @IBOutlet weak var cvPopular: UICollectionView!
    
    @IBOutlet weak  var viewBg : UIView!
    @IBOutlet weak  var lblCount : UILabel!
    @IBOutlet weak  var viewCount : UIView!
    
    var isSelectRadius = 0
    var menuSelected: MenuList = .company
    let notificationCenter = UNUserNotificationCenter.current()
    
    //***********************************************
    //MARK:- Life Cycle
    //***********************************************
    override func viewDidLoad() {
        super.viewDidLoad()
        notificationCenter.delegate = self
        let options: UNAuthorizationOptions = [.alert, .sound, .badge]
        
        notificationCenter.requestAuthorization(options: options) {
            (didAllow, error) in
            if !didAllow {
                print("User has declined notifications")
            }
        }
       // lblCount.layer.cornerRadius = 5
        viewBg.layer.cornerRadius = 40
        viewBg.layer.maskedCorners = [ .layerMinXMaxYCorner]
        
       NotificationCenter.default.removeObserver(self, name: .notificationDetail, object: nil)
       NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: .notificationDetail, object: nil)
        
      //  NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: NSNotification.Name(rawValue: "receive_notification_identifier"), object: nil)
        
        
        call_category_listAPI()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.navigationController?.setNavigationBarHidden(true, animated: true)
        let custombar = self.tabBarController as! CustomTabBarController
        custombar.isHiddenTabBar(hidden: false)
        tfSearch.text = ""
      //  NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: NSNotification.Name(rawValue: "receive_notification_identifier"), object: nil)
      //  NotificationCenter.default.removeObserver(self, name: .notificationDetail, object: nil)
      //  NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: .notificationDetail, object: nil)
        call_get_notification_count_Api()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         self.navigationController?.setNavigationBarHidden(true, animated: true)
        let custombar = self.tabBarController as! CustomTabBarController
        custombar.isHiddenTabBar(hidden: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        let custombar = self.tabBarController as? CustomTabBarController
                       custombar?.isHiddenTabBar(hidden: true)
    }
 
    //***********************************************
    //MARK:- Button Action
    //***********************************************
    
    
    @IBAction func actionNotification(_ sender: UIButton) {
        let vc = NotificationListVC.instance(storyBoard: .Home) as! NotificationListVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func btnCompany_Action(_ sender: UIButton) {
        let alertController = storyboard?.instantiateViewController(withIdentifier: "MenuPopupVC") as! MenuPopupVC
        alertController.modalPresentationStyle = .popover
        let popover = alertController.popoverPresentationController
        popover?.delegate = self
        alertController.delegate = self
        popover?.sourceView = sender
        popover?.permittedArrowDirections =  .up
        alertController.preferredContentSize = CGSize(width: 130, height: 120)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func btnSearch_Action(_ sender: UIButton) {
        if tfSearch.text != "" {
            if menuSelected == .customer {
                let vc = UserListVC.instance(storyBoard: .Review) as! UserListVC
                vc.searchKey = tfSearch.text ?? ""
                vc.isComeFrom = .BestReviewVC
                self.navigationController?.pushViewController(vc, animated: true)
                
            } else if menuSelected == .company {
                let vc = AllCompaniesVC.instance(storyBoard: .Home) as! AllCompaniesVC
                vc.isSelectRadius = self.isSelectRadius
                vc.searchKey = tfSearch.text ?? ""
                vc.isComeFrom = .BestReviewVC
                self.navigationController?.pushViewController(vc, animated: true)
                
            } else if menuSelected == .product {
                let vc = AllProductVC.instance(storyBoard: .Profile) as! AllProductVC
                vc.searchKey = tfSearch.text ?? ""
                vc.isComeFrom = .BestReviewVC
                vc.isSearch = true
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
        }
    }
    @objc func methodOfReceivedNotification2() {
        presentAlert("", msgStr: "didReceive response333", controller: self)
    }
    @objc func methodOfReceivedNotification(notification: Notification) {
        let userInfo = notification.object as? [String:Any]
            
          print("notificationType=-\(userInfo!)-")
        if let str = userInfo?["gcm.notification.data"] as? String{
            print(str)
            if let data = str.data(using: String.Encoding.utf8) {
                         do {
                         let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                            let type = json["type"] as? String
                            let rating_id = json["rating_id"] as? String
                            self.tabBarController?.selectedIndex = 0
                            let tabBarController = self.tabBarController as! CustomTabBarController
                            tabBarController.selectTabIndex(index: 0)
                            let vc = NotificationDetailVC.instance(storyBoard: .Home) as! NotificationDetailVC
                            vc.type = type!
                            vc.rating_id = rating_id ?? "0"
                            self.navigationController?.pushViewController(vc, animated: true)
                         } catch {
                         print("Something went wrong")
                         }
                         }
        }
        
    }

    deinit {
        print("deinit")
        NotificationCenter.default.removeObserver(self)
    }
    
}

extension BestReviewVC: MenuDelegate {
    func onButtonClick(button str: MenuList) {
        if str == .customer {
            self.btnMenu.setTitle("Customer", for: .normal)
            menuSelected = .customer
            
        } else if str == .company {
            self.btnMenu.setTitle("Company", for: .normal)
            menuSelected = .company
            
        } else if str == .product {
            self.btnMenu.setTitle("Product", for: .normal)
            menuSelected = .product
            
        }
    }
}

extension BestReviewVC: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
}
extension BestReviewVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  arrCategory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bestReviewCategoryCell", for: indexPath) as! bestReviewCategoryCell
        if collectionView == cvCategory{
            let dict = arrCategory[indexPath.row]
            cell.imgCategory.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imgCategory.sd_setImage(with: URL(string: dict.categoryImage ), placeholderImage: UIImage(named: "logo"))
            cell.lblCategoryName.text = dict.categoryName
            
            if  isSelectRadius == indexPath.row{
                cell.lblCategoryName.textColor =  .black
                cell.viewBg.backgroundColor =  UIColor(named: "SemiWhite")!
            }else{
                cell.viewBg.backgroundColor =  .clear
                cell.lblCategoryName.textColor =  UIColor(named: "SemiWhite")!
            }
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == cvCategory{
            isSelectRadius = indexPath.row
            cvCategory.reloadData()
            let seconds = 0.5
            DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
                // Put your code which should be executed with a delay here
                let vc = AllCompaniesVC.instance(storyBoard: .Home) as! AllCompaniesVC
                vc.isSelectRadius = self.isSelectRadius
                //  vc.previceIndex = indexPath
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    /* func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
     return UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
     }
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
     return CGSize(width:65, height: 100)
     }*/
    
}
/*
 extension BestReviewVC : UITableViewDelegate , UITableViewDataSource{
 /*  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
 20
 }
 
 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
 
 let cell = tableView.dequeueReusableCell(withIdentifier: "bestReviewCategoryCell", for: indexPath) as! bestReviewCategoryCell
 
 
 
 return cell
 }*/
 
 
 }*/




extension BestReviewVC{
    func call_category_listAPI( ){
        let parameter = [String : Any]()
        
        ServerManager.shared.POST(url: ApiAction.category_list  , param: parameter, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(CategoryModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                print("success")
                arrCategory.removeAll()
                arrCategory = obj.categoryList
                self.cvCategory.reloadData()
                self.cvRecentReview.reloadData()
                self.cvPopular.reloadData()
                
            } else {
                print("failure")
            }
        }
        
    }
    
    func call_get_notification_count_Api(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
        ServerManager.shared.POST(url: ApiAction.get_notification_count , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
           guard  let obj = try? JSONDecoder().decode(ModelNotifiCount.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                
                self.lblCount.text = obj.count
                if obj.count == "0"{
                    self.viewCount.isHidden = true
                }else{
                    self.viewCount.isHidden = false
                }
            }
            else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
}

extension BestReviewVC: UNUserNotificationCenterDelegate {
    //when app is in foreground recieve notificatios
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let userInfo = notification.request.content.userInfo as? [String : Any]
        print(userInfo)
        completionHandler([.alert, .sound])
    }
    //when app is in background recieve notificatios
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
       
        let userInfo = response.notification.request.content.userInfo as? [String : Any]
        print(userInfo!)
        if let str = userInfo?["gcm.notification.data"] as? String{
            print(str)
            if let data = str.data(using: String.Encoding.utf8) {
                         do {
                         let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                         //    let to_user_id = json["to_user_id"] as? String
                            let type = json["type"] as? String
                            let rating_id = json["rating_id"] as? String
                            
                            self.tabBarController?.selectedIndex = 0
                            let tabBarController = self.tabBarController as! CustomTabBarController
                            tabBarController.selectTabIndex(index: 0)
                            
                            
                            
                            let vc = NotificationDetailVC.instance(storyBoard: .Home) as! NotificationDetailVC
                            vc.type = type!
                            vc.rating_id = rating_id ?? "0"
                            
                            self.navigationController?.pushViewController(vc, animated: true)
                            
                         } catch {
                         print("Something went wrong")
                         }
                         }
        }
        
       /* if userInfo["type"] as? String == "challenge" {
            
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let contestJustVC = storyboard.instantiateViewController(withIdentifier: "ContestJustMeVC") as! ContestJustMeVC
            contestJustVC.contestPostViewController = .pushNotification
            contestJustVC.contestID = contestID.validate
            
            self.navigationController?.pushViewController(contestJustVC, animated: true)
            NotificationCenter.default.post(name: .contextInvite, object: nil, userInfo: userInfo)
        }*/
        
        completionHandler()
    }
    
    

    
}




enum MenuList {
    case customer
    case product
    case company
}








