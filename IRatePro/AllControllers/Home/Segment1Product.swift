//
//  Segment1Product.swift
//  IRatePro
//
//  Created by mac on 16/01/21.
//

import UIKit
import SDWebImage

class Segment1Product: UIViewController {
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var cvResult: UICollectionView!
    
    
    var isSelectRadius = 0
  //  var arrCategory = [CategoryList]()
     var arrResult = [ProductList]()
     var categoryId = "0"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        viewBg.layer.cornerRadius = 42
        viewBg.layer.maskedCorners = [ .layerMinXMaxYCorner]
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default
            .removeObserver(self, name: .providerProduct, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: .providerProduct, object: nil)
        
    }
    
    
    @objc func methodOfReceivedNotification(notification: Notification) {
           print("call observer2")
        call_productAlllist_listAPI()
       }
    override func viewWillDisappear(_ animated: Bool) {
       //   NotificationCenter.default
        //   .removeObserver(self, name: .providerProduct, object: nil)
       // NotificationCenter.default.removeObserver(self)
      }
    
    
    
    @IBAction func actionAddProduct(_ sender: Any) {
    let vc = AddProductVC.instance(storyBoard: .Profile) as! AddProductVC
        vc.arrCategory = arrCategory
    self.navigationController?.pushViewController(vc, animated: true)
    }
    
 

}
extension Segment1Product : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return  arrResult.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == cvResult{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ResultCategoryCell", for: indexPath) as! ResultCategoryCell
            let dict = arrResult[indexPath.row]
            cell.imgCategory.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imgCategory.sd_setImage(with: URL(string: dict.categoryImage ), placeholderImage: UIImage(named: "logo"))
            cell.lblCategoryName.text = dict.productName
        
        cell.constWeightCell.constant  = (self.view.frame.width / 2) - 20
        
            cell.rating.rating = 1
        
        return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bestReviewCategoryCell", for: indexPath) as! bestReviewCategoryCell
                        
            let dict = arrCategory[indexPath.row]
            cell.imgCategory.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imgCategory.sd_setImage(with: URL(string: dict.categoryImage ), placeholderImage: UIImage(named: "logo"))
            cell.lblCategoryName.text = dict.categoryName
            
            if  isSelectRadius == indexPath.row{
                cell.lblCategoryName.textColor =  .black
                cell.viewBg.backgroundColor =  UIColor(named: "SemiWhite")!
            }else{
                cell.viewBg.backgroundColor =  .clear
                cell.lblCategoryName.textColor =  UIColor(named: "SemiWhite")!
            }
            return cell
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        isSelectRadius = indexPath.row
        
        if collectionView == cvResult{
        }
    }
    

    
    func call_productAlllist_listAPI(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
        param[params.kcategory_id] = self.categoryId
        param[params.ksearch_keyword] = ""
        param[params.kstart] = ""
        
   ServerManager.shared.POST(url: ApiAction.product_list  , param: param, true,header: nil) { (data, error) in
            
            guard let data = data else {
                print("data not available")
                return
            }
           guard  let obj = try? JSONDecoder().decode(ProductModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                print("success")
                self.arrResult.removeAll()
                self.arrResult = obj.productList
                self.cvResult.reloadData()
            } else {
                print("failure")
            }
        }
        
    }
    
    
    
    
}



class ResultCategoryCell : UICollectionViewCell{
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var rating: FloatRatingView!
    @IBOutlet weak var constWeightCell: NSLayoutConstraint!
    @IBOutlet weak var constHeightCell: NSLayoutConstraint!
}




// MARK: - ProductModel
    struct ProductModel: Codable {
        let success, message: String
        let productList: [ProductList]
    }

    // MARK: - ProductList
    struct ProductList: Codable {
        let productID, userID, category, productName: String
        let productImage, productImageThumb: String
        let productDetail, productDescription, status, categoryName: String
        let categoryImage, categoryImageThumb, avgRating, ratingCount: String

        enum CodingKeys: String, CodingKey {
            case productID = "product_id"
            case userID = "user_id"
            case category
            case productName = "product_name"
            case productImage = "product_image"
            case productImageThumb = "product_image_thumb"
            case productDetail = "product_detail"
            case productDescription = "product_description"
            case status
            case categoryName = "category_name"
            case categoryImage = "category_image"
            case categoryImageThumb = "category_image_thumb"
            case avgRating = "avg_rating"
            case ratingCount = "rating_count"
        }
    }




class bestReviewCategoryCell : UICollectionViewCell{
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var viewBg: UIView!
}
