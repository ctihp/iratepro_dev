//
//  Segment0Company.swift
//  IRatePro
//
//  Created by mac on 16/01/21.
//

import UIKit
import SDWebImage

class Segment0Company: UIViewController , ApprovalPopupDelegate{
    @IBOutlet weak var tblReview: UITableView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var ratingOwn: FloatRatingView!
    @IBOutlet weak var lblrattingCOunt: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var imgCompany: UIImageView!
    @IBOutlet weak var tfTitleReview: UITextField!
    @IBOutlet weak var tvReviewDiscription: UITextView!
    
    
    var arrRatingList = [RatingList]()
    var dictCompany : UserList?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewTop.layer.cornerRadius = 8
        viewTop.layer.maskedCorners = [ .layerMinXMinYCorner , .layerMaxXMinYCorner]
        
        viewBottom.layer.cornerRadius = 12
        viewBottom.layer.maskedCorners = [ .layerMinXMaxYCorner , .layerMaxXMaxYCorner]

        NotificationCenter.default.removeObserver(self, name: .company, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: .company, object: nil)
        
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        print("get data from observer")
       let dict = notification.object as? UserList
        dictCompany = dict
        setUserDetail(dict!)
        self.call_getReviewList_Api()
    }
    
    deinit {
        print("deinit")
        NotificationCenter.default.removeObserver(self)
    }

    
    func setUserDetail(_ obj : UserList )  {
        imgCompany.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imgCompany.sd_setImage(with: URL(string: obj.profile_image! ), placeholderImage: UIImage(named: "user"))
        lblCompanyName.text = obj.company_name
        ratingOwn.rating = Double(obj.rating_count!)!
        lblrattingCOunt.text = obj.rating_count! + "/5"
    }
    
    
    func call_addCompanyRating_Api(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
        param[params.kcompany_id] = dictCompany!.user_id!
        param[params.kto_user] = dictCompany!.user_id!
        param[params.krate] = ratingView.rating
        param[params.kreview_title] = tfTitleReview.text
        param[params.kreview_description] = tvReviewDiscription.text
        
        ServerManager.shared.POST(url: ApiAction.addCompanyRating , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
           guard  let obj = try? JSONDecoder().decode(OtpModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                let vc = ApprovalPopupVC.instance(storyBoard: .Signup) as!  ApprovalPopupVC
                     vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                     vc.isModalInPresentation = true
                     vc.delegate = self
                vc.strHeading = "Thanks you for sharing!"
                vc.strDiscription = "Your feedback helps others make better decisions about which apps to use"
                     self.present(vc, animated: false, completion: nil)
                
                
              /*  DispatchQueue.main.async(execute: {
                    let _ = presentAlertWithOptions("", message: obj.message, controller: self, buttons: ["Ok"]) { (alert, actionTag) in
                        if actionTag == 0 {
                            //Ok Button
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                })*/
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    func actionRemove() {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func actionAddReview(_ sender: Any) {
        let valid = validation()
        if valid.status {
            call_addCompanyRating_Api()
        }else{
            presentAlert("", msgStr: valid.message, controller: self)
        }
    }
    
    @IBAction func actionAllReviewList(_ sender: Any) {
        let vc = AllReviewsVC.instance(storyBoard: .Home) as! AllReviewsVC
        vc.arrRatingList = arrRatingList
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func validation() -> (status:Bool, message:String) {
        var msg:String = ""
        var status:Bool = true
        if let fullname = tfTitleReview.text , fullname.isEmpty {
            status = false
            msg = Validation.kEnterTitle.rawValue
        }
        else if let fullname = tvReviewDiscription.text , fullname.isEmpty {
            status = false
            msg = Validation.kDiscription.rawValue
        }
        return (status:status, message:msg)
        }
}


extension Segment0Company{
    func call_getReviewList_Api(){
        var param = [String : Any]()
        param[params.ksearch_keyword] = ""
        param[params.kid] = dictCompany?.user_id!
        param[params.kstart] = "0"
        param[params.ktype] = "1"
        param[params.ksort_filter] = ""
        param[params.kcompany_id] = ""
        print(param)
        
        ServerManager.shared.POST(url: ApiAction.rateing_list , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
           guard  let obj = try? JSONDecoder().decode(ReviewtListingModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                self.arrRatingList = obj.ratingList
                print(self.arrRatingList.count)
                self.tblReview.reloadData()
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
}



extension Segment0Company: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var ccount = 0
        if arrRatingList.count > 1{
            ccount = 2
        }else if arrRatingList.count == 1{
            ccount = 1
        }
        return ccount
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RateListingCell", for: indexPath) as! RateListingCell
     
     let obj =    arrRatingList[indexPath.row]
        cell.lblToName.text = obj.toUsername
        cell.lblReviews.text = obj.reviewDescription  //to_profile_image
        cell.imgTo.sd_setImage(with: URL(string: obj.toProfileImage ), placeholderImage: UIImage(named: "user"))
        cell.rating.rating = Double(obj.allRatingCount) ?? 0.0
        return cell
    }
}





class RateListingCell : UITableViewCell{
    @IBOutlet weak var imgTo: UIImageView!
    @IBOutlet weak var lblToName: UILabel!
    @IBOutlet weak var rating: FloatRatingView!
    @IBOutlet weak var lblReviews: UILabel!
}









