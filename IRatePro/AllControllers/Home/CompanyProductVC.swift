//
//  Segment1Product.swift
//  IRatePro
//
//  Created by mac on 16/01/21.
//

import UIKit
import SDWebImage

class CompanyProductVC: UIViewController {
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var cvResult: UICollectionView!
    @IBOutlet weak var btnSearch: UIButton!
    
    var dictCompany : UserList?
    var isSelectRadius = 0
    var arrCategory = [ProductList]()
    var categoryId = "0"
    var isSearch = false
    var isPopToRoot = false
    var page:Int = 0
    var isDownloading = false
    var pointContentOffset = CGPoint.zero
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.removeObserver(self, name: .productList, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: .productList, object: nil)
        viewBG.layer.cornerRadius = 42
        viewBG.layer.maskedCorners = [ .layerMinXMaxYCorner ]
        tfSearch.addTarget(self, action: #selector(SearchDidChange(_:)), for: .editingChanged)
        arrCategory.removeAll()
        page = 0
        isDownloading = false
    }

    override func viewWillAppear(_ animated: Bool) {
    }
    
    @objc func SearchDidChange(_ textField: UITextField) {
        let textToSearch = textField.text ?? ""
        
        isSearch = true
        page = 0
        isDownloading = false
        if textToSearch.count > 2{
            call_product_list_API()
        }else  if textToSearch.count == 0 {
            call_product_list_API()
        }
        
        
        
    }
    @objc func methodOfReceivedNotification(notification: Notification) {
        print("call observer2")
        dictCompany = notification.object as? UserList
        call_product_list_API()
    }

    deinit {
        print("deinit")
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //   NotificationCenter.default
        //   .removeObserver(self, name: .providerProduct, object: nil)
        // NotificationCenter.default.removeObserver(self)
    }

    @IBAction func actionAddProduct(_ sender: Any) {

    }
    
    @IBAction func btnSearch_Action(_ sender: Any) {
    }
}
extension CompanyProductVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  arrCategory.count
        
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ResultCategoryCell", for: indexPath) as! ResultCategoryCell
            let dict = arrCategory[indexPath.row]
            cell.imgCategory.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imgCategory.sd_setImage(with: URL(string: dict.categoryImage ), placeholderImage: UIImage(named: "logo"))
        cell.lblCategoryName.text = dict.productName
        cell.constWeightCell.constant  = (self.view.frame.width / 2) - 20
        cell.rating.rating = Double(dict.ratingCount) ?? 0.0
    
        if indexPath.row == (arrCategory.count - 1) && (cvResult.contentOffset.y > pointContentOffset.y) {
            if !isDownloading {
                isDownloading = true
                if arrCategory.count % 10 == 0 {
                    page += 10
                    self.call_product_list_API()
                }
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict = arrCategory[indexPath.row]
        page = 0
        isDownloading = false
        tfSearch.text = ""
       
        call_product_list_API()
        let vc = AddReviewOnProductVC.instance(storyBoard: .Home) as! AddReviewOnProductVC
        vc.dictCompany = dictCompany
        vc.dictProduct = dict
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func call_product_list_API(){
        var param = [String : Any]()
        param[params.kuser_id] = dictCompany!.user_id! /// AppDataHelper.shard.logins.userinfo.userID
        //param[params.kcompany_id] = dictCompany!.user_id!
        //param[params.kstart] = "0"
        param[params.kstart] =  page
        param[params.ksearch_keyword] = tfSearch.text
        
        ServerManager.shared.POST(url: ApiAction.product_list  , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(ProductModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                print("success")
                //self.arrCategory.removeAll()
                if self.isSearch {
                    self.arrCategory.removeAll()
                    self.arrCategory = obj.productList
                    self.isSearch = false
                }else{
                    if obj.productList.count != 0{
                        self.arrCategory.append(contentsOf: obj.productList)
                    }
                }
               
                if obj.productList.count == 0{
                    self.isDownloading = true
                }else{
                    self.isDownloading = false
                }

                self.cvResult.reloadData()
            } else {
                self.isDownloading = true
                print("failure")
            }
        }
    }
    
   /*func call_productAlllist_listAPI( _ search : String){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
        param[params.kcategory_id] = self.categoryId
        param[params.ksearch_keyword] = search
        param[params.kstart] = ""
        
        ServerManager.shared.POST(url: ApiAction.productAlllist  , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
           guard  let obj = try? JSONDecoder().decode(ProductModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                print("success")
                self.arrResult.removeAll()
                self.arrResult = obj.productList
                self.cvResult.reloadData()
            } else {
                print("failure")
            }
        }
    }*/
}
