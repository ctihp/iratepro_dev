//
//  NotificationListVC.swift
//  IRatePro
//
//  Created by mac on 23/02/21.
//

import UIKit

class NotificationListVC: UIViewController, NotifiDetailDelegate {
    @IBOutlet weak var tblNotification: UITableView!
    var arrNotifiList = [NotificationInfo]()
    var page:Int = 0
    var isDownloading = false
    var pointContentOffset = CGPoint.zero
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        page = 0
        isDownloading = false
        arrNotifiList.removeAll()
        call_notification_list_Api()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func actionChoosePlan(cell: NotificationListingCell) {
        guard let indexPath = tblNotification.indexPath(for: cell) else {
              return
          }
        
        let obj =    arrNotifiList[indexPath.row]
        let vc = NotificationDetailVC.instance(storyBoard: .Home) as! NotificationDetailVC
        vc.type = obj.type
        vc.rating_id = obj.rating_id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func actionDelete(cell: NotificationListingCell) {
        
        guard let indexPath = tblNotification.indexPath(for: cell) else {
              return
          }
        let obj =    arrNotifiList[indexPath.row]
        call_deleteNotification_Api(notificationID: obj.rating_id, indexPath)
       
      
    }
    

}


extension NotificationListVC: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return arrNotifiList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationListingCell", for: indexPath) as! NotificationListingCell
     
     let obj =    arrNotifiList[indexPath.row]
        cell.delegate  = self
        print(obj.userType)
        if obj.userType == "1"{
            cell.imgTo.sd_setImage(with: URL(string: obj.profileImageFrom ), placeholderImage: UIImage(named: "user"))
            cell.lbltitle.text = obj.usernameFrom
        }else  if obj.userType == "2"{
            cell.imgTo.sd_setImage(with: URL(string: obj.companyProfileImageFrom ), placeholderImage: UIImage(named: "user"))
            cell.lbltitle.text = obj.companyNameFrom
        }
        
       //to_profile_image
        cell.lbldiscription.text = obj.message
        
        let aa =  stringToDateWithFormat(strDate: obj.createdAt ,dateFormat: "dd-MM-yyyy HH:mm:ss")
        let utcDate = aa!.toGlobalTime()
        cell.lblDate.text = utcDate.timeAgoSinceDate()
        
        if indexPath.row == (arrNotifiList.count - 1) && (tblNotification.contentOffset.y > pointContentOffset.y) {
            if !isDownloading {
                isDownloading = true
                if arrNotifiList.count % 10 == 0 {
                    page += 10
                    self.call_notification_list_Api()
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func stringToDateWithFormat( strDate:String,  dateFormat:String) -> Date?{
        
        let formatter = DateFormatter()// "2021-02-23 17:55:33"
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"//dateFormat
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        if let date1 = formatter.date(from: strDate) {
      //  print("strDate---->\(strDate), date------->\(date1)")
            return date1//"\(formatter.string(from: Date()))"
        }else {
            return nil
        }
    }

    
    
    
    func call_notification_list_Api(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
        param[params.kstart] =  page
        ServerManager.shared.POST(url: ApiAction.notification_list , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
           guard  let obj = try? JSONDecoder().decode(NotificationModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
              //  self.arrNotifiList.removeAll()
              
                self.arrNotifiList.append(contentsOf: obj.notificationInfo)
               // self.arrNotifiList = obj.notificationInfo
                self.tblNotification.reloadData()
                
                if obj.notificationInfo.count == 0{
                    self.isDownloading = true
                }else{
                    self.isDownloading = false
                    }
                
                if self.arrNotifiList.count == 0{
                      self.tblNotification.displayBackgroundImageWithText(text: "Data not Found" , fontStyle: "", fontSize: 15, imgName: "cloud")
                  }else{
                    self.tblNotification.displayBackgroundImageWithText(text: "" , fontStyle: "", fontSize: 15, imgName: "")
                  }
                
            }
            else {
                print("failure")
                self.isDownloading = true
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    
    
    
    func call_deleteNotification_Api(notificationID : String, _ indexpath : IndexPath ){
        var param = [String : Any]()
        param[params.kuser_id] = notificationID
        ServerManager.shared.POST(url: ApiAction.delete_notification , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
           guard  let obj = try? JSONDecoder().decode(OtpModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                self.arrNotifiList.remove(at:indexpath.row)
                self.tblNotification.deleteRows(at: [indexpath], with: .top)
                if self.arrNotifiList.count == 0{
                      self.tblNotification.displayBackgroundImageWithText(text: "Notification not Found" , fontStyle: "", fontSize: 15, imgName: "cloud")
                  }else{
                    self.tblNotification.displayBackgroundImageWithText(text: "" , fontStyle: "", fontSize: 15, imgName: "")
                  }
            }
            else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    
    
    
}







protocol NotifiDetailDelegate:NSObject {
    func actionChoosePlan(cell:NotificationListingCell)
    func actionDelete(cell:NotificationListingCell)
}

class NotificationListingCell : UITableViewCell{
    @IBOutlet weak var imgTo: UIImageView!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lbldiscription: UILabel!
    weak var delegate: NotifiDetailDelegate?
    
    @IBAction func actionChoosePlan(_ sender: Any) {
        delegate?.actionChoosePlan(cell: self)
    }
    
    @IBAction func actionDelete(_ sender: Any) {
        delegate?.actionDelete(cell: self)
    }
    
}






struct NotificationModel: Codable {
    let success, message: String
    let notificationInfo: [NotificationInfo]

    enum CodingKeys: String, CodingKey {
        case success, message
        case notificationInfo = "notification_info"
    }
}

// MARK: - NotificationInfo
struct NotificationInfo: Codable {
    let id, message, userSendTo, userSendFrom: String
    let createdAt, type, productID, companyID: String
    let userType, usernameTo, emailTo: String
    let profileImageTo: String
    let usernameFrom, companyNameFrom, emailFrom: String
    let profileImageFrom, rating_id: String
    let companyProfileImageFrom: String

    enum CodingKeys: String, CodingKey {
        case id, message
        case userSendTo = "user_send_to"
        case userSendFrom = "user_send_from"
        case createdAt = "created_at"
        case type
        case productID = "product_id"
        case companyID = "company_id"
        case userType = "user_type"
        case usernameTo = "username_to"
        case emailTo = "email_to"
        case profileImageTo = "profile_image_to"
        case usernameFrom = "username_from"
        case companyNameFrom = "company_name_from"
        case emailFrom = "email_from"
        case profileImageFrom = "profile_image_from"
        case companyProfileImageFrom = "company_profile_image_from"
        case rating_id = "rating_id"
        
    }
}



