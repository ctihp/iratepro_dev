//
//  NotificationDetailVC.swift
//  IRatePro
//
//  Created by mac on 24/02/21.
//

import UIKit
import  SDWebImage

class NotificationDetailVC: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblToName: UILabel!
    @IBOutlet weak var imgToProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblFromNAme: UILabel!
    @IBOutlet weak var lblFromDescription: UILabel!
    @IBOutlet weak var viewRating: FloatRatingView!
    @IBOutlet weak var lblRatingCount: UILabel!
    @IBOutlet weak var imgFormPerson: UIImageView!
    @IBOutlet weak  var viewBg : UIView!
    var rating_id =  ""
    var type =  ""
     var   ratingDetail: RatingDetail?
    var strUserID = ""
    
    ///---
    
    
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var tfUserType: UITextField!
    @IBOutlet weak var tfTitle: UITextField!
    @IBOutlet weak var tvComment: KMPlaceholderTextView!
    @IBOutlet weak var ratingView: FloatRatingView!
    var arrUserType = [[String : String]]()
    var pickerView = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(rating_id)
        print(type)
        tfUserType.inputView = pickerView
        arrUserType = [["type":"User", "id": "1"], ["type":"Company", "id": "2"]]
      //  type  1= company,2 = product, customer = 3
        if type == "1"{
            lblTitle.text = "Company"
        }else if type == "2"{
            lblTitle.text = "Product"
        }else if type == "3"{
            lblTitle.text = "Customer"
        }
        
        viewBg.layer.cornerRadius = 40
        viewBg.layer.maskedCorners = [ .layerMinXMaxYCorner]
        call_notification_list_Api()
        //---
        
        
        ratingView.type = .halfRatings
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        print(AppSharedData.shared.isCustomer)
        
        if AppSharedData.shared.isCustomer{
            tfUserType.text = "User"
            strUserID = "1"
            tfUserType.isUserInteractionEnabled = false
        }
        tfTitle.setLeftPaddingPoints(5)
        tfUserType.setLeftPaddingPoints(5)
        
        
        

    }
    

    
    @IBAction func actionBack(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
       }
    
    
    func call_notification_list_Api(){
        var param = [String : Any]()
        param[params.kid] = rating_id
        ServerManager.shared.POST(url: ApiAction.rateing_detail , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
           guard  let obj = try? JSONDecoder().decode(NotificationDetailModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                self.ratingDetail = obj.ratingDetail
                self.setdata()
                
            }
            else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    func setdata(){
        imgFormPerson.sd_imageIndicator = SDWebImageActivityIndicator.gray
         imgToProfile.sd_setImage(with: URL(string: self.ratingDetail!.fromProfileImage ), placeholderImage: UIImage(named: "logo"))
        
        
        //  type  1= company,2 = product, customer = 3
        
        if type == "1"{
            
            lblToName.text = self.ratingDetail!.companyName
            imgToProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
             imgToProfile.sd_setImage(with: URL(string: self.ratingDetail!.companyProfileImage ), placeholderImage: UIImage(named: "logo"))
        }else if type == "2"{
            lblToName.text = self.ratingDetail!.productName
            imgToProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
             imgToProfile.sd_setImage(with: URL(string: self.ratingDetail!.productImageThumb ), placeholderImage: UIImage(named: "logo"))
        }else{
            lblToName.text = self.ratingDetail!.toUsername
            imgToProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
             imgToProfile.sd_setImage(with: URL(string: self.ratingDetail!.toProfileImage ), placeholderImage: UIImage(named: "logo"))
        }
      
        print(self.ratingDetail!.userType)
        if self.ratingDetail!.userType == "1"{
            lblName.text = self.ratingDetail!.fromUsername
            lblMsg.text = "Give Reviews & Rate to " + self.ratingDetail!.fromUsername
           imgFormPerson.sd_setImage(with: URL(string: self.ratingDetail!.fromProfileImage ), placeholderImage: UIImage(named: "user"))
        }else  if self.ratingDetail!.userType == "2"{
            lblMsg.text = "Give Reviews & Rate to " + self.ratingDetail!.fromCompanyName
            lblName.text = self.ratingDetail!.fromCompanyName
            imgFormPerson.sd_setImage(with: URL(string: self.ratingDetail!.fromCompanyProfileImage ), placeholderImage: UIImage(named: "user"))
        }
        
        
        lblFromNAme.text = self.ratingDetail!.reviewTitle
        lblFromDescription.text = self.ratingDetail!.reviewDescription
        
        lblRatingCount.text = self.ratingDetail!.rate  + "/5"
        viewRating.rating = Double(self.ratingDetail!.rate) ?? 0.0
        
         
        
        
    }
    
    func validation() -> (status:Bool, message:String) {
        var msg:String = ""
        var status:Bool = true
        
        if let fullname = tfTitle.text , fullname.isEmpty {
            status = false
            msg = Validation.kEnterTitle.rawValue
        }
        else if  tvComment.text! == ""{
            status = false
            msg = Validation.kDiscription.rawValue
        }
        else if  strUserID == ""{
            status = false
            msg = Validation.kUserType.rawValue
        }
        return (status:status, message:msg)
        
        }
    
    @IBAction func actionAddReview(_ sender: Any) {
        let valid = validation()
        if valid.status {
            if self.ratingDetail!.userType == "1"{
                // user
                call_addcustomerRating_API()
            }else{
                // company
                call_addCompanyRating_Api()
                
            }
           
           
        }else{
            presentAlert("", msgStr: valid.message, controller: self)
        }
    }
}

extension NotificationDetailVC : UITextFieldDelegate, ApprovalPopupDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
  
    

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrUserType.count
       
    }

    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrUserType[row]["type"]
        
    }

    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        tfUserType.text = arrUserType[row]["type"]
        strUserID = arrUserType[row]["id"]!
    }
    
    
    
     func call_addcustomerRating_API(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
        param[params.kcustomer_id] = (self.ratingDetail!.userID)
        param[params.kcompany_id] = (self.ratingDetail!.userID)
        param[params.kto_user] = (self.ratingDetail!.userID)
        param[params.krate] = ratingView.rating
        param[params.kreview_title] = tfTitle.text
        param[params.kreview_description] = tvComment.text
        param[params.kuser_type] = strUserID
  
        
        ServerManager.shared.POST(url: ApiAction.addcustomerRating , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
           guard  let obj = try? JSONDecoder().decode(OtpModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                
                let vc = ApprovalPopupVC.instance(storyBoard: .Signup) as!  ApprovalPopupVC
                     vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                     vc.isModalInPresentation = true
                     vc.delegate = self
                vc.strHeading = "Thanks you for sharing!"
                vc.strDiscription = "Your feedback helps others make better decisions about which apps to use"
                     self.present(vc, animated: false, completion: nil)
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
        
    }
    
    
    func call_addCompanyRating_Api(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
        param[params.kcompany_id] = (self.ratingDetail!.userID)
        param[params.kto_user] = (self.ratingDetail!.userID)
        param[params.krate] = ratingView.rating
        param[params.kreview_title] = tfTitle.text
        param[params.kreview_description] = tvComment.text
        param[params.kuser_type] = strUserID
        
        ServerManager.shared.POST(url: ApiAction.addCompanyRating , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
           guard  let obj = try? JSONDecoder().decode(OtpModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                let vc = ApprovalPopupVC.instance(storyBoard: .Signup) as!  ApprovalPopupVC
                     vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                     vc.isModalInPresentation = true
                     vc.delegate = self
                vc.strHeading = "Thanks you for sharing!"
                vc.strDiscription = "Your feedback helps others make better decisions about which apps to use"
                     self.present(vc, animated: false, completion: nil)
                
       
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    
    
    
    
    func actionRemove() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}








///-*-------------------


// MARK: - NotificationDetailModel
struct NotificationDetailModel: Codable {
    let success, message: String
    let ratingDetail: RatingDetail
}

// MARK: - RatingDetail
struct RatingDetail: Codable {
    let id, userID, toUser, companyID: String
    let customerID, productID, rate, reviewTitle: String
    let reviewDescription, rateImage, rateImageThumb, status: String
    let type, userType, createdAt, fromUsername: String
    let fromCompanyName, fromEmail, fromPhoneNumber, fromFcmToken: String
    let fromProfileImage: String
    let fromCompanyProfileImage, toUsername, toEmail, toPhoneNumber: String
    let toFcmToken: String
    let toProfileImage: String
    let companyName, companyDescription, companyCategory, companyEin: String
    let companyWebsite, companyAddress, companyProfileImage, productName: String
    let productCategory, productImage, productImageThumb, productDetail: String
    let productDescription, userAvgRating, productAvgRating, allAvgRating: String
    let userRatingCount, productRatingCount, allRatingCount, companyNameSearch: String
    let productNameSearch, usernameSearch: String

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case toUser = "to_user"
        case companyID = "company_id"
        case customerID = "customer_id"
        case productID = "product_id"
        case rate
        case reviewTitle = "review_title"
        case reviewDescription = "review_description"
        case rateImage = "rate_image"
        case rateImageThumb = "rate_image_thumb"
        case status, type
        case userType = "user_type"
        case createdAt = "created_at"
        case fromUsername = "from_username"
        case fromCompanyName = "from_company_name"
        case fromEmail = "from_email"
        case fromPhoneNumber = "from_phone_number"
        case fromFcmToken = "from_fcm_token"
        case fromProfileImage = "from_profile_image"
        case fromCompanyProfileImage = "from_company_profile_image"
        case toUsername = "to_username"
        case toEmail = "to_email"
        case toPhoneNumber = "to_phone_number"
        case toFcmToken = "to_fcm_token"
        case toProfileImage = "to_profile_image"
        case companyName = "company_name"
        case companyDescription = "company_description"
        case companyCategory = "company_category"
        case companyEin = "company_ein"
        case companyWebsite = "company_website"
        case companyAddress = "company_address"
        case companyProfileImage = "company_profile_image"
        case productName = "product_name"
        case productCategory = "product_category"
        case productImage = "product_image"
        case productImageThumb = "product_image_thumb"
        case productDetail = "product_detail"
        case productDescription = "product_description"
        case userAvgRating = "user_avg_rating"
        case productAvgRating = "product_avg_rating"
        case allAvgRating = "all_avg_rating"
        case userRatingCount = "user_rating_count"
        case productRatingCount = "product_rating_count"
        case allRatingCount = "all_rating_count"
        case companyNameSearch = "company_name_search"
        case productNameSearch = "product_name_search"
        case usernameSearch = "username_search"
    }
}


