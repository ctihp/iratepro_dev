//
//  PaymentVC.swift
//  IRatePro
//
//  Created by mac on 15/01/21.
//

import UIKit
import Stripe

class PaymentVC: UIViewController {
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var viewPayment: UIView!
    @IBOutlet weak var imgCardImage: UIImageView!
    @IBOutlet weak var txtCardNumber: UITextField!
    @IBOutlet weak var txtExpDate: UITextField!
    @IBOutlet weak var txtCVC: UITextField!
    
    var isPopToRoot = false
    var planId = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBg.layer.cornerRadius = 40
        viewBg.layer.maskedCorners = [ .layerMinXMaxYCorner]
        
        viewPayment.layer.cornerRadius = 12
        viewPayment.layer.maskedCorners = [ .layerMaxXMinYCorner]
        let cardBrand = STPCardValidator.brand(forNumber: "133")
        self.imgCardImage.image = STPImageLibrary.cardBrandImage(for: cardBrand)
        
     //   self.navigationController?.popToRootViewController(animated: true)
        
      //  let custombar = self.tabBarController as! CustomTabBarController
           //    custombar.isHiddenTabBar(hidden: true)
        
        }
    
 
    
    
    @IBAction func actionChoosePlan(_ sender: Any) {
       
    }

    
    @IBAction func actionBack(_ sender: Any) {
        isPopToRoot  = true
           self.navigationController?.popViewController(animated: true)
       }
    
    override func viewWillDisappear(_ animated: Bool) {
        if  !isPopToRoot{
            self.navigationController?.popToRootViewController(animated: false)
        }
    }
    
    @IBAction func actionOnPay(_ sender : UIButton)
    {
        self.view.endEditing(true)
        if isValidData()
        {
            getStripeToken()
        }
    }
    
    
}

extension PaymentVC:UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let rawString = string
        let range1 = rawString.rangeOfCharacter(from: .whitespaces)
        if ((textField.text?.count)! == 0 && range1  != nil)
            || ((textField.text?.count)! > 0  && range1 != nil)  {
            return false
        }
        
        if  var text = textField.text,
            let textRange = Range(range, in: text) {
            
            let updatedText = text.replacingCharacters(in: textRange,  with: string)
            let replacementText = updatedText.replacingOccurrences(of: " ", with: "")
            
            //Verify entered text is a numeric value
            if !CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: string)) {
                return false
            }
            
            // CVC Number
            if textField == txtCVC && string != ""
            {
                let cardBrand = STPCardValidator.brand(forNumber: txtCardNumber.text ?? "")
                let cvcLength = STPCardValidator.maxCVCLength(for:cardBrand)
                return range.location < cvcLength
            }
                //Exp Date
            else if textField == txtExpDate
            {
                if range.location > 4 {
                    return false
                }
                //Put / after 2 digit
                if range.location == 2 && string != "" {
                    text.append("/")
                    textField.text = text
                }
            }
                // CArd number
            else if  textField == txtCardNumber
            {
                if replacementText.count > 16 {
                    return false
                }
                let cardBrand = STPCardValidator.brand(forNumber: replacementText)
               // self.imgCardImage.image = STPImageLibrary.brandImage(for: cardBrand)
                self.imgCardImage.image = STPImageLibrary.cardBrandImage(for: cardBrand)
                //Put space  after 4 digit
                if (range.location % 5) == 0 && string != ""{
                    text.append(" ")
                    textField.text = text
                }
            }
        }
        return true
    }
    
    
    func isValidData() -> Bool
    {
        let year = Int(Date().getYear(format: "yy"))
        
        if (txtCardNumber.text?.isEmpty)!
        {
            _ = presentAlertWithOptions("", message: "Please Enter Card Number", controller: self, buttons: ["OK"], tapBlock: { (alert, index) in
            })
            return false
        }
        else if (txtExpDate.text?.isEmpty)!
        {
            _ = presentAlertWithOptions("", message: "Please Enter Card Expiry Date" , controller: self, buttons: ["OK"], tapBlock: { (alert, index) in
            })
            return false
        }
        else if  let expArr = txtExpDate.text?.components(separatedBy: "/"),
            expArr.count != 2 || Int(expArr[0])! > 12 || Int(expArr[1])! < year!
        {
            _ = presentAlertWithOptions("", message: "Please Enter Card Expiry Date" , controller: self, buttons: ["OK"], tapBlock: { (alert, index) in
            })
            return false
        }
        else if (txtCVC.text?.isEmpty)!
        {
            _ = presentAlertWithOptions("", message: "Please Enter Card CVC" , controller: self, buttons: ["OK"], tapBlock: { (alert, index) in
            })
            return false
        }
        return true
    }
    
    
    func getStripeToken()  {
        let expArr = txtExpDate.text?.components(separatedBy: "/")
        let cardParams = STPCardParams()
        cardParams.number = txtCardNumber.text
        cardParams.expMonth = UInt(expArr![0])!
        cardParams.expYear = UInt(expArr![1])!
        cardParams.cvc = txtCVC.text
        
        STPAPIClient.shared.createToken(withCard: cardParams) { (token: STPToken?, error: Error?) in
            if  let token = token, error == nil
            {
                print(token)
                print(token.tokenId)
                self.call_getMembershipPlan_API(strpieToken: String(token.tokenId))
            }
            else
            {
                print(error?.localizedDescription)
            }
        }
    }
    
    func call_getMembershipPlan_API(strpieToken: String){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
        param[params.kplan_id] = planId
        param[params.kstripeToken] = strpieToken
        
        ServerManager.shared.POST(url: ApiAction.add_subscription , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            
           guard  let obj = try? JSONDecoder().decode(OtpModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                
              DispatchQueue.main.async(execute: {
                      let _ = presentAlertWithOptions("", message: obj.message, controller: self, buttons: ["Ok"]) { (alert, actionTag) in
                          if actionTag == 0 {
                              self.navigationController?.popViewController(animated: true)
                          }
                      }
                  })
                
                
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
        
    }
    
}

extension Date {
    
    func  getYear(format:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: Date())
    }
    
 
}
