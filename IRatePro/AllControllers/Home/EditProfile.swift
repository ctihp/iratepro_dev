//
//  EditProfile.swift
//  IRatePro
//
//  Created by mac on 19/01/21.
//

import UIKit

class EditProfile: UIViewController , TDImagePickerDelegate{
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var tfUserName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfMobileNumber: UITextField!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    var userinfo : UserinfoProfile?
    
    
    var imagepickerDocument:TDImagePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagepickerDocument = TDImagePicker(presentationController: self, delegate: self)
        viewBg.layer.cornerRadius = 42
        viewBg.layer.maskedCorners = [ .layerMinXMaxYCorner ]
        setProfileData(userinfo!)
        

        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionBack(_ sender: Any) {
          
        self.navigationController?.popViewController(animated: true)
       }
    @IBAction func actionUpdate(_ sender: Any) {
        if tfUserName.text == ""{
            presentAlert("", msgStr: Validation.kEnterName.rawValue, controller: self)
        }else{
            call_SignupAPI()
        }
       
       }
    
    
    func setProfileData(_ obj : UserinfoProfile ){
        tfUserName.text = obj.username
        lblUserName.text = tfUserName.text
        tfEmail.text = obj.email
        tfMobileNumber.text = obj.phoneNumber
        ratingView.rating = Double(obj.ratingCount) ?? 0.0
        lblRating.text = obj.ratingCount  + "/5"
        imgProfile.sd_setImage(with: URL(string: obj.profileImage ), placeholderImage: UIImage(named: "user"))
        
    }
    
    @IBAction func uploadVerificationIDBtnAction(_ sender: UIButton) {
        print("upload verfication id button clicked")
        imagepickerDocument.present(from: sender)
        
    }
    
    func didSelect(image: UIImage?) {
        imgProfile.image = image
        
    }
    
    
    
   
}


extension EditProfile{
    func call_SignupAPI2(){
        var param = [String : Any]()
        param[params.kusername] = tfUserName.text
        param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
        
        ServerManager.shared.POST(url: ApiAction.updateProfile , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
           guard  let obj = try? JSONDecoder().decode(SignupModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                let _ = presentAlertWithOptions(kAppName, message: obj.message!, controller: self, buttons: ["Ok"]) { (alert, actionTag) in
                    if actionTag == 0 {
                        //Ok Button
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        //Cancel Button
                    }
                }
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
        
    }
    
    
    func call_SignupAPI(){
         var param = [String : Any]()
         param[params.kusername] = tfUserName.text
         param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
         
         var imagesParams = [String:Data]()
         imagesParams[params.kimage] = imgProfile.image?.jpegData(compressionQuality: 0.5)
         
         ServerManager.shared.POSTWithImage(url: ApiAction.updateProfile, param: param, imageView: imgProfile) { (data, error) in
           
                 guard let data = data else {
                     print("data not available")
                     return
                 }
                 
                 guard  let obj = try? JSONDecoder().decode(SignupModel.self, from: data) else {
                      return
                  }
                  if obj.success == ResponseApis.KSuccess {
                      let _ = presentAlertWithOptions(kAppName, message: obj.message!, controller: self, buttons: ["Ok"]) { (alert, actionTag) in
                          if actionTag == 0 {
                              //Ok Button
                              self.navigationController?.popViewController(animated: true)
                          } else {
                              //Cancel Button
                          }
                      }
                  } else {
                      print("failure")
                      presentAlert("", msgStr: obj.message, controller: self)
                  }
             }
             
         }
}
