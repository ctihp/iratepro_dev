//
//  SettingVC.swift
//  IRatePro
//
//  Created by mac on 18/01/21.
//

import UIKit
import  SafariServices
import MessageUI
import AudioToolbox
class SettingVC: UIViewController , MFMailComposeViewControllerDelegate  {
    
    
    @IBOutlet weak var btnNotificationOnOff: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionBack(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
       }
    
    @IBAction func actionNotificationOnOff(_ sender: Any) {
        btnNotificationOnOff.isSelected.toggle()
        print(btnNotificationOnOff.isSelected)
    }
    @IBAction func actionTermsCondition(_ sender: Any) {
        let vc = SFSafariViewController(url: URL(string: "https://www.google.co.in/")!)
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
      
    }
    @IBAction func actionPrivacypolicy(_ sender: Any) {
        let vc = SFSafariViewController(url: URL(string: "https://www.google.co.in/")!)
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func actionContactUs(_ sender: Any) {
        let emailTitle = "Feedback"
         let messageBody = ""
         let toRecipents = ["admin@.com"]
        if MFMailComposeViewController.canSendMail() {
          let mc: MFMailComposeViewController = MFMailComposeViewController()
         mc.mailComposeDelegate = self
         mc.setSubject(emailTitle)
         mc.setMessageBody(messageBody, isHTML: false)
         mc.setToRecipients(toRecipents)
         self.present(mc, animated: true, completion: nil)
        }
          /*      //TODO:  You should chack if we can send email or not
                if MFMailComposeViewController.canSendMail() {
                    let mail = MFMailComposeViewController()
                    mail.mailComposeDelegate = self
                    mail.setToRecipients(["you@yoursite.com"])
                    mail.setSubject("Email Subject Here")
                    mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
                    present(mail, animated: true)
                } else {
                    print("Application is not able to send an email")
                }
         */
         
     }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }

    

}

extension SettingVC: SFSafariViewControllerDelegate {
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        print("safari did finish")
    }
    
    func safariViewController(_ controller: SFSafariViewController, initialLoadDidRedirectTo URL: URL) {
        print("url\(URL.absoluteString)")
        let urlString = "https://www.google.co.in/"
        
        if URL.absoluteString == urlString {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                // do stuff 42 seconds later
                self.dismiss(animated: true) {
                    NotificationCenter.default.post(name: Notification.Name("popAndPush"), object: nil)
                    self.navigationController?.popViewController(animated: true)
                    
                }
            }
        }
    }
}
