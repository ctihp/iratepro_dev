//
//  Segment0Company.swift
//  IRatePro
//
//  Created by mac on 16/01/21.
//

import UIKit
import SDWebImage

class AddReviewOnProductVC : UIViewController , ApprovalPopupDelegate{
    func actionRemove() {
        self.navigationController?.popViewController(animated: true)
    }
    

    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var tblReview: UITableView!
    @IBOutlet weak var lblProductName: UILabel!
    // @IBOutlet weak var viewTop: UIView!
   // @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var ratingOwn: FloatRatingView!
    @IBOutlet weak var lblrattingCOunt: UILabel!
    @IBOutlet weak var tfTitleReview: UITextField!
    @IBOutlet weak var tvReviewDiscription: UITextView!
    
    var dictCompany : UserList?
    var dictProduct : ProductList?
    var arrRatingList = [RatingList]()
    override func viewDidLoad() {
        super.viewDidLoad()
       /* viewTop.layer.cornerRadius = 8
        viewTop.layer.maskedCorners = [ .layerMinXMinYCorner , .layerMaxXMinYCorner]
        
        viewBottom.layer.cornerRadius = 12
        viewBottom.layer.maskedCorners = [ .layerMinXMaxYCorner , .layerMaxXMaxYCorner]*/
        tfTitleReview.setLeftPaddingPoints(5)
        
        setUserDetail(dictProduct!)
        call_getReviewList_Api()
    }
    


    
    func setUserDetail(_ obj : ProductList )  {
       
        imgProduct.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imgProduct.sd_setImage(with: URL(string: obj.productImage ), placeholderImage: UIImage(named: "logo"))
        lblProductName.text = obj.productName
        ratingOwn.rating = Double(obj.ratingCount)!
        lblrattingCOunt.text = obj.ratingCount + "/5"
    }
    
    
    func call_addCompanyRating_Api(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
        param[params.kproduct_id] = dictProduct?.productID
        param[params.kto_user] = dictProduct?.userID
        param[params.krate] = ratingView.rating
        param[params.kreview_title] = tfTitleReview.text
        param[params.kreview_description] = tvReviewDiscription.text
        
        ServerManager.shared.POST(url: ApiAction.addProductRating , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
           guard  let obj = try? JSONDecoder().decode(OtpModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                let vc = ApprovalPopupVC.instance(storyBoard: .Signup) as!  ApprovalPopupVC
                     vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                     vc.isModalInPresentation = true
                     vc.delegate = self
                vc.strHeading = "Thanks you for sharing!"
                vc.strDiscription = "Your feedback helps others make better decisions about which apps to use"
                     self.present(vc, animated: false, completion: nil)
                
                
              /*  DispatchQueue.main.async(execute: {
                    let _ = presentAlertWithOptions("", message: obj.message, controller: self, buttons: ["Ok"]) { (alert, actionTag) in
                        if actionTag == 0 {
                            //Ok Button
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                })*/
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    var isPopToRoot = false
    @IBAction func actionBack(_ sender: Any) {
        isPopToRoot  = true
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
            if  !isPopToRoot{
                self.navigationController?.popToRootViewController(animated: false)
            }
        }
    
    @IBAction func actionAllReviewList(_ sender: Any) {
        let vc = AllReviewsVC.instance(storyBoard: .Home) as! AllReviewsVC
        vc.arrRatingList = arrRatingList
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func actionAddReview(_ sender: Any) {
        let valid = validation()
        if valid.status {
            call_addCompanyRating_Api()
        }else{
            presentAlert("", msgStr: valid.message, controller: self)
        }
    }
    
    
    func validation() -> (status:Bool, message:String) {
        var msg:String = ""
        var status:Bool = true
        
        if let fullname = tfTitleReview.text , fullname.isEmpty {
            status = false
            msg = Validation.kEnterTitle.rawValue
        }
        else if let fullname = tvReviewDiscription.text , fullname.isEmpty {
            status = false
            msg = Validation.kDiscription.rawValue
        }
        return (status:status, message:msg)
        
        }
        
    

   

    
   

}



extension AddReviewOnProductVC{
    func call_getReviewList_Api(){
        var param = [String : Any]()
        param[params.ksearch_keyword] = ""
        param[params.kid] = dictProduct?.productID
        param[params.kstart] = "0"
        param[params.ktype] = "2"
        param[params.ksort_filter] = ""
        param[params.kcompany_id] = ""
        print(param)
        
        ServerManager.shared.POST(url: ApiAction.rateing_list , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
           guard  let obj = try? JSONDecoder().decode(ReviewtListingModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                self.arrRatingList = obj.ratingList
                print(self.arrRatingList.count)
                self.tblReview.reloadData()
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
}



extension AddReviewOnProductVC: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var ccount = 0
        if arrRatingList.count > 1{
            ccount = 2
        }else if arrRatingList.count == 1{
            ccount = 1
        }
        return ccount
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RateListingCell", for: indexPath) as! RateListingCell
     
     let obj =    arrRatingList[indexPath.row]
        cell.lblToName.text = obj.toUsername
        cell.lblReviews.text = obj.reviewDescription  //to_profile_image
        cell.imgTo.sd_setImage(with: URL(string: obj.toProfileImage ), placeholderImage: UIImage(named: "logo"))
        cell.rating.rating = Double(obj.allRatingCount) ?? 0.0
        return cell
    }
}


