//
//  ApprovalPopupVC.swift
//  IRatePro
//
//  Created by mac on 18/01/21.
//

import UIKit

protocol ApprovalPopupDelegate:NSObject {
    func actionRemove()
}

class ApprovalPopupVC: UIViewController {

    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    weak var delegate: ApprovalPopupDelegate?
   var strHeading = ""
    var strDiscription = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        if strHeading != ""{
            lblHeading.text = strHeading
        }
        
        if strDiscription != ""{
            lblDescription.text = strDiscription
        }
        // Do any additional setup after loading the view.
    }
    
    //***********************************************
    //MARK:- Button action
    //***********************************************
    @IBAction func btnBack_Action(_ sender: Any) {
        dismiss(animated: true) {
            self.delegate?.actionRemove()
        }
    }
}

