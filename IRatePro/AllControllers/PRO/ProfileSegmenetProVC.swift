//
//  CompanyNameVC.swift
//  IRatePro
//
//  Created by mac on 16/01/21.
//

import UIKit

class ProfileSegmenetProVC: UIViewController {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var viewBg: UIImageView!
    @IBOutlet weak var container: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentedControl.selectedSegmentIndex = 0
        viewBg.layer.cornerRadius = 42
        viewBg.layer.maskedCorners = [ .layerMinXMaxYCorner ]
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
       let custombar = self.tabBarController as! CustomTabBarController
       custombar.isHiddenTabBar(hidden: false)
        segmentedControl.isHidden = true
        if AppSharedData.shared.isCustomer{
            updateViewCustomer()
        }else{
            segmentedControl.isHidden = false
            setupView()
        }
    }
    

     override func viewDidAppear(_ animated: Bool) {
         super.viewDidAppear(animated)
          self.navigationController?.setNavigationBarHidden(true, animated: true)
         let custombar = self.tabBarController as! CustomTabBarController
         custombar.isHiddenTabBar(hidden: false)
     }
     
     
     
     override func viewWillDisappear(_ animated: Bool) {
         let custombar = self.tabBarController as? CustomTabBarController
                        custombar?.isHiddenTabBar(hidden: true)
     }
    
    @IBAction func actionBack(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
       }

    
    private func setupView() {
        updateView()
    }
    
    //-----------------------provider------------------------------
    private lazy var Segment1Profile: Segment1ProfileVC = {
        let vc = Segment1ProfileVC.instance(storyBoard: .Profile) as! Segment1ProfileVC
    self.add(asChildViewController: vc)
        return vc
    }()

    private lazy var Seg1Product: Segment1Product = {
        let viewController = Segment1Product.instance(storyBoard: .Profile) as! Segment1Product
        self.add(asChildViewController: viewController)

        return viewController
    }()

    // customer profile
    private lazy var segmentCustomerProfile: ProfileVC = {
        let viewController = ProfileVC.instance(storyBoard: .Profile) as! ProfileVC
        self.add(asChildViewController: viewController)
        return viewController
    }()
    
    
    private func add(asChildViewController viewController: UIViewController) {
        addChild(viewController)
        container.addSubview(viewController.view)
        viewController.view.frame = container.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParent: self)
    }
    
    
    private func remove(asChildViewController viewController: UIViewController) {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
    
    private func updateView() {
       if segmentedControl.selectedSegmentIndex == 0 {
            remove(asChildViewController: Seg1Product)
            add(asChildViewController: Segment1Profile)
        NotificationCenter.default.post(Notification(name: .providerProfile, object: nil, userInfo: nil))
        }else  if segmentedControl.selectedSegmentIndex == 1 {
            remove(asChildViewController: Segment1Profile)
            add(asChildViewController: Seg1Product)
            NotificationCenter.default.post(Notification(name: .providerProduct, object: nil, userInfo: nil))
        }
    
    }
    
    private func updateViewCustomer() {
        add(asChildViewController: segmentCustomerProfile)
     }
    
    
  
    @IBAction func actionSegment(_ sender: Any) {
        updateView()
    }
    
   
}


extension Notification.Name {
// provider
    static let providerProfile = Notification.Name("provider_profile")
    static let providerProduct = Notification.Name("provider_product")
    static let company = Notification.Name("company")
    static let productList = Notification.Name("productList")
    static let notificationDetail = Notification.Name("notificationDetail")
    static let membership = Notification.Name("membership")
    static let planDetail = Notification.Name("planDetail")
    
    
    
}
