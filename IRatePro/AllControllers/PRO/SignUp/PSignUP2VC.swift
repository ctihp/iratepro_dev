//
//  PSignUP2VC.swift
//  IRatePro
//
//  Created by mac on 18/01/21.
//

import UIKit

class PSignUP2VC: UIViewController {

    @IBOutlet weak var viewUpload: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // MARK: - Navigation

    
    @IBAction func btnBack_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmit_Action(_ sender: Any) {
        let vc = AddDetailVC.instance(storyBoard: .Signup) as! AddDetailVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
