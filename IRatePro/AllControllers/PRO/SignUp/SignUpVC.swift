//
//  SignUpVC.swift
//  IRatePro
//
//  Created by mac on 18/01/21.
//

import UIKit
import TagListView



class SignUpVC: UIViewController ,ApprovalPopupDelegate ,TagListViewDelegate{

    @IBOutlet weak var btnSignup1HeightConstant: NSLayoutConstraint!
    @IBOutlet weak var btnSignup1: UIButton!
    @IBOutlet weak var btnArrow: UIButton!
    @IBOutlet weak var btnCheckUncheck: UIButton!
    @IBOutlet weak var stkPro: UIStackView!
    @IBOutlet weak var stkHeightContank: NSLayoutConstraint!
    @IBOutlet weak var viewStk: UIView!
    @IBOutlet weak var tfUserName: FloatingLabelInput!
    @IBOutlet weak var tfEmail: FloatingLabelInput!
   
    @IBOutlet weak var tfCompany: FloatingLabelInput!
    @IBOutlet weak var tfEINNumber: FloatingLabelInput!
    @IBOutlet weak var tfBusniess: FloatingLabelInput!
    @IBOutlet weak var tfWebsite: FloatingLabelInput!
 //   @IBOutlet weak var tfCategory: FloatingLabelInput!
    @IBOutlet weak var tfCompanyDiscription: FloatingLabelInput!
    @IBOutlet weak var tfAddress: FloatingLabelInput!
    @IBOutlet weak var tfPassword: FloatingLabelInput!
    @IBOutlet weak var tfConfirmPassword: FloatingLabelInput!
    
   
    @IBOutlet weak var tagListView: TagListView!
    @IBOutlet weak var tblviewCategory: UITableView!
    
    @IBOutlet weak var viewPopup: UIView!
    @IBOutlet weak var lblCategory: UIButton!
    var cCode = ""
    var MobileNumbwer = ""
    
    
    var strSkill = [String]()
    var arrCategory = [CategoryList]()
    
    var arrBusiness = [String]()

    var pickerView = UIPickerView()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrBusiness = ["Services" , "Transportation"]
        stkHeightContank.constant = 0
        viewStk.isHidden = true
        tfBusniess.inputView = pickerView
     //   tfCategory.inputView = pickerView
         pickerView.delegate = self
         pickerView.dataSource = self
      
        
        call_category_listAPI()
        tagListView.delegate = self
        
    }
    

    func actionRemove() {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: SignupSigninVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    
    // Mark:- All actions
    @IBAction func actionCheck(_ sender: Any) {
        if btnCheckUncheck.isSelected {
            btnCheckUncheck.isSelected = false
            stkHeightContank.constant = 0
            viewStk.isHidden = true
            btnSignup1.isHidden = false
            btnArrow.isHidden = false
            btnSignup1HeightConstant.constant = 48
        }else{
            btnCheckUncheck.isSelected = true
            stkHeightContank.constant = 650
            viewStk.isHidden = false
            btnSignup1.isHidden = true
            btnArrow.isHidden = true
            btnSignup1HeightConstant.constant = 0
            }
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSignup(_ sender: Any) {
        let valid = validation()
        if valid.status {
            var  arrTemp : [[String:Any]] = []
            for dicts in arrCategory {
               var dict = [String:Any]()
                if dicts.status2  {
                    dict["item_id"] = dicts.categoryID
                    dict["item_text"] = dicts.categoryName
                    arrTemp.append(dict)
                }
             
           }
            if arrTemp.count > 0 {
                let str =  json(from: arrTemp)
                call_SignupAPI(str!)
                
            }else{
                call_SignupAPI( "")
            }
           
            
 
    } else {
        presentAlert("", msgStr: valid.message, controller: self)
        
        }
    }
    
    public func json(from object:Any) -> String? {
           guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
               return nil
           }
           return String(data: data, encoding: String.Encoding.utf8)
       }
    
    

    
   
    @IBAction func actionCategory(_ sender: Any) {
        self.viewPopup.frame = self.view.frame
        self.viewPopup.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        self.view.addSubview(viewPopup)
        
    }
    
    @IBAction func actionPopupClose(_ sender: Any) {
        self.viewPopup.removeFromSuperview()
    }
    
    
    
    
    
    
    func validation() -> (status:Bool, message:String) {
        var msg:String = ""
        var status:Bool = true
        
        if let fullname = tfUserName.text , fullname.isEmpty {
            status = false
            msg = Validation.kEnterName.rawValue
        }
        else if let fullname = tfEmail.text , fullname.isEmpty {
            status = false
            msg = Validation.kEnterEmail.rawValue
        }
        else if !isValidEmail(candidate: tfEmail.text!){
            status = false
            msg = Validation.kEnterValidEmail.rawValue
        }else if tfPassword.text == ""{
            status = false
            msg = Validation.kEnterPassword.rawValue
        }
        else if tfConfirmPassword.text == ""{
            status = false
            msg = Validation.kEnterConfirmPassword.rawValue
        }
        else if tfConfirmPassword.text != tfPassword.text{
            status = false
            msg = Validation.kEnterValidPassword.rawValue
        }
        if btnCheckUncheck.isSelected {
            if tfCompany.text == ""{
                status = false
                msg = Validation.kEnterCompanyName.rawValue
            }else if tfEINNumber.text == ""{
                status = false
                msg = Validation.kEnterEINNumber.rawValue
            }else if tfBusniess.text == ""{
                status = false
                msg = Validation.kEnterBusinessType.rawValue
            }
            else if tfWebsite.text == ""{
                status = false
                msg = Validation.kEnterWebUrl.rawValue
            }
            else if strSkill.count == 0{
                status = false
                msg = Validation.kSelectCategory.rawValue
            }
            
            else if tfCompanyDiscription.text == ""{
                status = false
                msg = Validation.kEnterCompanyDis.rawValue
            }
            else if tfAddress.text == ""{
                status = false
                msg = Validation.kEnterAddress.rawValue
            }
            
        }
        
        
        
        return (status:status, message:msg)
    }
    func isValidEmail(candidate: String) -> Bool {
        
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        var valid = NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
        if valid {
            valid = !candidate.contains("..")
        }
        return valid
    }
    
}

extension SignUpVC: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrCategory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell", for: indexPath) as! categoryCell
     
     let obj =    arrCategory[indexPath.row]
        cell.lblText.text = obj.categoryName
        if obj.status2{
            cell.imgCheck.image = UIImage(named: "check")
        }else{
            cell.imgCheck.image = UIImage(named: "uncheck")
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // var obj = arrSearch[indexPath.row]
    if arrCategory[indexPath.row].status2{
        arrCategory[indexPath.row].status2 = false
        
     }else{
        arrCategory[indexPath.row].status2 = true
     }
     tblviewCategory.reloadData()
     setTag()
 }
    
}


extension SignUpVC: UITextFieldDelegate {
 

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        if textField == tfUserName{
            tfEmail.becomeFirstResponder()
        }
        else  if textField == tfEmail{
            tfPassword.becomeFirstResponder()
        }
        
        else  if textField == tfPassword{
            tfConfirmPassword.becomeFirstResponder()
        }
        else  if textField == tfConfirmPassword{
            if btnCheckUncheck.isSelected{
                tfCompany.becomeFirstResponder()
            }else{
                textField.resignFirstResponder()
            }
        }
       
        if btnCheckUncheck.isSelected{
            if textField == tfCompany{
                tfEINNumber.becomeFirstResponder()
            }
            else  if textField == tfEINNumber{
                tfWebsite.becomeFirstResponder()
            }
            else  if textField == tfWebsite{
                tfCompanyDiscription.becomeFirstResponder()
            }
            else  if textField == tfCompanyDiscription{
                tfAddress.becomeFirstResponder()
            }
            else  if textField == tfAddress{
                tfPassword.becomeFirstResponder()
            }
           
        }
        
        return textField.resignFirstResponder()
      //  return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
        
        return true
    }
}



extension SignUpVC{

func call_category_listAPI( ){
    let parameter = [String : Any]()
   
    ServerManager.shared.POST(url: ApiAction.category_list  , param: parameter, true,header: nil) { (data, error) in
        guard let data = data else {
            print("data not available")
            return
        }
       guard  let obj = try? JSONDecoder().decode(CategoryModel.self, from: data) else {
            return
        }
        if obj.success == ResponseApis.KSuccess {
            print("success")
            self.arrCategory = obj.categoryList
            self.tblviewCategory.reloadData()
        } else {
            print("failure")
        }
    }
    
}
    
    func call_SignupAPI( _ strCategory : String? = "" ){
        var param = [String : Any]()
        param[params.kusername] = tfUserName.text
        param[params.kemail] = tfEmail.text
        param[params.kpassword] = tfPassword.text
        param[params.kein] = tfEINNumber.text
        param[params.kbusiness_type] = tfBusniess.text
        param[params.kaddress] = tfAddress.text
        param[params.kwebsite] = tfWebsite.text
        param[params.kcompany_description] = ""
        param[params.kcategorys] = strCategory
        param[params.kcompany_name] = tfCompany.text
        param[params.kphone_number] = MobileNumbwer
        param[params.kcountry_code] = cCode
        
        
        ServerManager.shared.POST(url: ApiAction.signup , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
           guard  let obj = try? JSONDecoder().decode(SignupModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                print("success")
                let vc = ApprovalPopupVC.instance(storyBoard: .Signup) as!  ApprovalPopupVC
                     vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                     vc.isModalInPresentation = true
                     vc.delegate = self
                     self.present(vc, animated: false, completion: nil)
             //   presentAlert("", msgStr: obj.message, controller: self)
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
        
    }
        // MARK: TagListViewDelegate
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag pressed: \(title), \(sender)")
        tagView.isSelected = !tagView.isSelected
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        var mycount = 0
        for i in 0..<arrCategory.count {
            let dict = arrCategory[i]
            if dict.categoryName == title {
              arrCategory[i].status2 = false
                self.tblviewCategory.reloadData()
            }
            if arrCategory[i].status2 == true{
                mycount += 1
            }
        }
        if mycount > 0 {
            lblCategory.isHidden = true
        }else{
            lblCategory.isHidden = false
            }
        tblviewCategory.reloadData()
      
        sender.removeTagView(tagView)
    }

    
    func setTag(){
        strSkill.removeAll()
        tagListView.removeAllTags()
        for dict in arrCategory{
            if dict.status2{
              strSkill.append(dict.categoryName)
            }
        }
        if strSkill.count > 0 {
            lblCategory.isHidden = true
        }else{
            lblCategory.isHidden = false
            }
        tagListView.addTags(strSkill)
    }
    
 

}

extension SignUpVC:  UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrBusiness.count
       
    }

    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrBusiness[row]
        
    }

    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        tfBusniess.text = arrBusiness[row]
    }
}





// MARK: - CategoryModel
struct CategoryModel: Codable {
    let success, message: String
    let categoryList: [CategoryList]

    enum CodingKeys: String, CodingKey {
        case success, message
        case categoryList = "category_list"
    }
}

// MARK: - CategoryList
struct CategoryList: Codable {
    let categoryID, categoryName: String
    let categoryImage, categoryImageThumb: String
    let status: String
    var status2 : Bool = false
    
    
    enum CodingKeys: String, CodingKey {
        case categoryID = "category_id"
        case categoryName = "category_name"
        case categoryImage = "category_image"
        case categoryImageThumb = "category_image_thumb"
        case status
    }
}
   



class categoryCell: UITableViewCell {
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var imgCheck: UIImageView!
}

struct SignupModel : Codable {
    let success : String?
    let message : String?
    let userinfo : Userinfo?

    enum CodingKeys: String, CodingKey {

        case success = "success"
        case message = "message"
        case userinfo = "userinfo"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(String.self, forKey: .success)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        userinfo = try values.decodeIfPresent(Userinfo.self, forKey: .userinfo)
    }

}
/*
// MARK: - SignupModel
struct SignupModel: Codable {
    let success, message: String
    let userinfo: Userinfo
}*/

// MARK: - Userinfo
struct Userinfo : Codable {
    let user_id : String?
    let username : String?
    let email : String?
    let token : String?
    let country_code : String?
    let phone_number : String?
    let business_type : String?
    let ein : String?
    let act_token : String?
    let address : String?
    let fcm_token : String?
    let gender : String?
    let profile_image : String?
    let company_profile_image : String?
    let avg_rating : String?
    let rating_count : String?
    let user_avg_rating : String?
    let user_rating_count : String?
    let company_name : String?
    let company_description : String?
    let website : String?
    let status : String?
    let category : [Category2]?
    let business_status : String?
    let device_type : String?

    enum CodingKeys: String, CodingKey {

        case user_id = "user_id"
        case username = "username"
        case email = "email"
        case token = "token"
        case country_code = "country_code"
        case phone_number = "phone_number"
        case business_type = "business_type"
        case ein = "ein"
        case act_token = "act_token"
        case address = "address"
        case fcm_token = "fcm_token"
        case gender = "gender"
        case profile_image = "profile_image"
        case company_profile_image = "company_profile_image"
        case avg_rating = "avg_rating"
        case rating_count = "rating_count"
        case user_avg_rating = "user_avg_rating"
        case user_rating_count = "user_rating_count"
        case company_name = "company_name"
        case company_description = "company_description"
        case website = "website"
        case status = "status"
        case category = "category"
        case business_status = "business_status"
        case device_type = "device_type"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        username = try values.decodeIfPresent(String.self, forKey: .username)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        token = try values.decodeIfPresent(String.self, forKey: .token)
        country_code = try values.decodeIfPresent(String.self, forKey: .country_code)
        phone_number = try values.decodeIfPresent(String.self, forKey: .phone_number)
        business_type = try values.decodeIfPresent(String.self, forKey: .business_type)
        ein = try values.decodeIfPresent(String.self, forKey: .ein)
        act_token = try values.decodeIfPresent(String.self, forKey: .act_token)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        fcm_token = try values.decodeIfPresent(String.self, forKey: .fcm_token)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        profile_image = try values.decodeIfPresent(String.self, forKey: .profile_image)
        company_profile_image = try values.decodeIfPresent(String.self, forKey: .company_profile_image)
        avg_rating = try values.decodeIfPresent(String.self, forKey: .avg_rating)
        rating_count = try values.decodeIfPresent(String.self, forKey: .rating_count)
        user_avg_rating = try values.decodeIfPresent(String.self, forKey: .user_avg_rating)
        user_rating_count = try values.decodeIfPresent(String.self, forKey: .user_rating_count)
        company_name = try values.decodeIfPresent(String.self, forKey: .company_name)
        company_description = try values.decodeIfPresent(String.self, forKey: .company_description)
        website = try values.decodeIfPresent(String.self, forKey: .website)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        category = try values.decodeIfPresent([Category2].self, forKey: .category)
        business_status = try values.decodeIfPresent(String.self, forKey: .business_status)
        device_type = try values.decodeIfPresent(String.self, forKey: .device_type)
    }

}

struct Category2 : Codable {
    let item_id : String?
    let item_text : String?

    enum CodingKeys: String, CodingKey {

        case item_id = "item_id"
        case item_text = "item_text"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        item_id = try values.decodeIfPresent(String.self, forKey: .item_id)
        item_text = try values.decodeIfPresent(String.self, forKey: .item_text)
    }

}




extension UINavigationController {
  func popToViewController(ofClass: AnyClass, animated: Bool = true) {
    if let vc = viewControllers.last(where: { $0.isKind(of: ofClass) }) {
      popToViewController(vc, animated: animated)
    }
  }
}






