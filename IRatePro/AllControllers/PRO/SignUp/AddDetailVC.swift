//
//  AddDetailVC.swift
//  IRatePro
//
//  Created by mac on 18/01/21.
//

import UIKit

class AddDetailVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    //***********************************************
    //MARK:- Button Action
    //***********************************************

    @IBAction func btnDone_Action(_ sender: Any) {
        let vc = ApprovalPopupVC.instance(storyBoard: .Signup) as!  ApprovalPopupVC
       
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.popoverPresentationController?.delegate = self
        vc.isModalInPresentation = true
        
        self.present(vc, animated: false, completion: nil)
    }
}
extension AddDetailVC:UIPopoverPresentationControllerDelegate {
    
}
