//
//  PSignUp1VC.swift
//  IRatePro
//
//  Created by mac on 18/01/21.
//

import UIKit

class PSignUp1VC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    //***********************************************
    //MARK:- Button Action
    //***********************************************
    @IBAction func btnBack_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSignUP_Action(_ sender: Any) {
        let vc = PSignUP2VC.instance(storyBoard: .Signup) as! PSignUP2VC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
