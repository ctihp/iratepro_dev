//
//  UserReview.swift
//  IRatePro
//
//  Created by mac on 16/02/21.
//

import UIKit

class UserReview: UIViewController , UITextFieldDelegate, ApprovalPopupDelegate{
  
    
    @IBOutlet weak var lblToUser: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var tfUserType: UITextField!
    @IBOutlet weak var tfTitle: UITextField!
    @IBOutlet weak var tvComment: KMPlaceholderTextView!
    @IBOutlet weak var viewBg: UIImageView!
    
    var arrUserType = [[String : String]]()
    var strUserID = ""
    var pickerView = UIPickerView()
    var dictUserList : UserListReview?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfUserType.inputView = pickerView
        arrUserType = [["type":"User", "id": "1"], ["type":"Company", "id": "2"]]
        setUSerData()
        ratingView.type = .halfRatings
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        print(AppSharedData.shared.isCustomer)
        
        if AppSharedData.shared.isCustomer{
            tfUserType.text = "User"
            strUserID = "1"
            tfUserType.isUserInteractionEnabled = false
        }
        tfTitle.setLeftPaddingPoints(5)
        tfUserType.setLeftPaddingPoints(5)
        viewBg.layer.cornerRadius = 42
        viewBg.layer.maskedCorners = [ .layerMinXMaxYCorner ]
        
    }
    
    
    
    func setUSerData(){
        lblToUser.text = "Write a review of " + (dictUserList?.username!)!
        imgProfile.sd_setImage(with: URL(string: (dictUserList?.profile_image)! ), placeholderImage: UIImage(named: "user"))
        
        
    }
    
    
    func validation() -> (status:Bool, message:String) {
        var msg:String = ""
        var status:Bool = true
        
        if let fullname = tfTitle.text , fullname.isEmpty {
            status = false
            msg = Validation.kEnterTitle.rawValue
        }
        else if  tvComment.text! == ""{
            status = false
            msg = Validation.kDiscription.rawValue
        }
        else if  strUserID == ""{
            status = false
            msg = Validation.kUserType.rawValue
        }
        return (status:status, message:msg)
        
        }
    
    
    
    @IBAction func actionAddReview(_ sender: Any) {
        let valid = validation()
        if valid.status {
            call_addcustomerRating_API()
           
        }else{
            presentAlert("", msgStr: valid.message, controller: self)
        }
    }
    
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }


}


extension UserReview:  UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrUserType.count
       
    }

    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrUserType[row]["type"]
        
    }

    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        tfUserType.text = arrUserType[row]["type"]
        strUserID = arrUserType[row]["id"]!
    }
    
    
    
    func call_addcustomerRating_API(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
        param[params.kcustomer_id] = (dictUserList?.user_id)!
        param[params.kcompany_id] = (dictUserList?.user_id)!
        param[params.kto_user] = (dictUserList?.user_id)!
        param[params.krate] = ratingView.rating
        param[params.kreview_title] = tfTitle.text
        param[params.kreview_description] = tvComment.text
        param[params.kuser_type] = strUserID
  
        
        ServerManager.shared.POST(url: ApiAction.addcustomerRating , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
           guard  let obj = try? JSONDecoder().decode(OtpModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                
                let vc = ApprovalPopupVC.instance(storyBoard: .Signup) as!  ApprovalPopupVC
                     vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                     vc.isModalInPresentation = true
                     vc.delegate = self
                vc.strHeading = "Thanks you for sharing!"
                vc.strDiscription = "Your feedback helps others make better decisions about which apps to use"
                     self.present(vc, animated: false, completion: nil)
                
              /*  DispatchQueue.main.async(execute: {
                    let _ = presentAlertWithOptions("", message: obj.message, controller: self, buttons: ["Ok"]) { (alert, actionTag) in
                        if actionTag == 0 {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                })*/
               
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
        
    }
    
    func actionRemove() {
        self.navigationController?.popViewController(animated: true)
    }
}
