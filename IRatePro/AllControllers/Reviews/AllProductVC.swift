//
//  AllProductVC.swift
//  IRatePro
//
//  Created by mac on 18/02/21.
//

import UIKit
import SDWebImage
class AllProductVC: UIViewController {

    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var cvResult: UICollectionView!
    @IBOutlet weak var tfSearch: UITextField!
    
    var dictCompany : UserList?
    var isSelectRadius = 0
    var arrCategory = [ProductList]()
    var categoryId = "0"
    var isSearch = false
    var searchKey = ""
    var page:Int = 0
    var isDownloading = false
    var pointContentOffset = CGPoint.zero
    var isComeFrom: Page = .none
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        viewBG.layer.cornerRadius = 42
        viewBG.layer.maskedCorners = [ .layerMinXMaxYCorner ]
        tfSearch.addTarget(self, action: #selector(SearchDidChange(_:)), for: .editingChanged)
        arrCategory.removeAll()
        page = 0
        isDownloading = false
        tfSearch.text = searchKey
        
    }
    override func viewWillAppear(_ animated: Bool) {
        if isSearch {
            call_product_list_API(searchKey: searchKey)
        }else {
            callAPI()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        isSearch = false
    }
    //***********************************************
    //MARK:- Button Action
    //***********************************************
    @IBAction func btnback_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSearch_Action(_ sender: Any) {
        
    }
    @objc func SearchDidChange(_ textField: UITextField) {
        let textToSearch = textField.text ?? ""
        searchKey = textToSearch
        isSearch = true
        page = 0
        isDownloading = false
        if textToSearch.count > 2{
            call_product_list_API(searchKey: textToSearch)
        }else  if textToSearch.count == 0 {
            call_product_list_API(searchKey: "")
        }
    }
}
extension AllProductVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  arrCategory.count
        
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ResultCategoryCell", for: indexPath) as! ResultCategoryCell
        let dict = arrCategory[indexPath.row]
        cell.imgCategory.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imgCategory.sd_setImage(with: URL(string: dict.categoryImage ), placeholderImage: UIImage(named: "logo"))
        cell.lblCategoryName.text = dict.productName
        cell.constWeightCell.constant  = (self.view.frame.width / 2) - 20
        cell.rating.rating = Double(dict.ratingCount) ?? 0.0
       
        return cell
    }
    func scrollViewWillEndDragging( _ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if velocity.y > 0 {
            print("velocity:---------\(velocity.y)")
            if !isDownloading {
                isDownloading = true
                isSearch = false
                if arrCategory.count % 10 == 0 {
                    page += 10
                    self.callAPI()
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict = arrCategory[indexPath.row]
        page = 0
        isDownloading = false
        tfSearch.text = ""
        call_product_list_API(searchKey: "")
        let vc = AddReviewOnProductVC.instance(storyBoard: .Home) as! AddReviewOnProductVC
        vc.dictCompany = dictCompany
        vc.dictProduct = dict
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func call_product_list_API(searchKey str: String){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
        
        /// AppDataHelper.shard.logins.userinfo.userID
        //param[params.kcompany_id] = dictCompany!.user_id!
        //param[params.kstart] = "0"
        param[params.kcategory_id] = ""
        param[params.kstart] = page
        param[params.ksearch_keyword] = str
        
        ServerManager.shared.POST(url: ApiAction.product_all_list  , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(ProductModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                self.isDownloading = false
                //self.arrCategory.removeAll()
                if self.isSearch {
                    self.arrCategory.removeAll()
                    self.arrCategory = obj.productList
                    self.isSearch = false
                }/*else{
                    if obj.productList.count != 0{
                        self.isDownloading = false
                        self.arrCategory.append(contentsOf: obj.productList)
                    }
                }
                if self.arrCategory.count == 0{
                    self.isDownloading = true
                }else{
                    self.isDownloading = false
                }*/
                self.cvResult.reloadData()
            } else {
                self.isDownloading = true
                print("failure")
            }
        }
    }
}
extension AllProductVC {
   func callAPI() {
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
        
        /// AppDataHelper.shard.logins.userinfo.userID
        //param[params.kcompany_id] = dictCompany!.user_id!
        //param[params.kstart] = "0"
        param[params.kcategory_id] = ""
        param[params.kstart] = page
        param[params.ksearch_keyword] = ""
        
        ServerManager.shared.POST(url: ApiAction.product_all_list  , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(ProductModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                print("success----------\(obj.productList.count)-----response array")
                //self.arrCategory.removeAll()
                
                if self.arrCategory.count == 0{
                    self.isDownloading = true
                }else{
                    self.isDownloading = false
                }
             
                self.arrCategory.append(contentsOf: obj.productList)
                print("success----------\(self.arrCategory.count)-----local array")
                self.cvResult.reloadData()
            } else {
                self.isDownloading = true
                print("failure")
            }
        }
    }
}
