//
//  UserListVC.swift
//  IRatePro
//
//  Created by mac on 18/02/21.
//

import UIKit

class UserListVC: UIViewController {

    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var tblResult: UITableView!
    @IBOutlet weak var tfSearch: UITextField!
    var arrUserList = [UserListReview]()
    
    var isSearch = false
    var page:Int = 0
    var isDownloading = false
    var pointContentOffset = CGPoint.zero
    
    var searchKey = ""
    var isComeFrom: Page = .none
    
    //***********************************************
    //MARK:- Life Cycle
    //***********************************************
    override func viewDidLoad() {
        super.viewDidLoad()
        page = 0
        isDownloading = false
        tfSearch.text = searchKey
        call_customer_listAPI(searchKey: searchKey)
        viewBG.layer.cornerRadius = 40
        viewBG.layer.maskedCorners = [ .layerMinXMaxYCorner]
        tfSearch.addTarget(self, action: #selector(SearchDidChange(_:)), for: .editingChanged)
    }
    
    //***********************************************
    //MARK:- Button Action
    //***********************************************
    @IBAction func btnBack_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
  
    @IBAction func btnSearch_Action(_ sender: Any) {
        
    }
    @objc func SearchDidChange(_ textField: UITextField) {
          let textToSearch = textField.text ?? ""

        isSearch = true
        searchKey = textToSearch
        page = 0
        isDownloading = false
        if textToSearch.count > 2{
            call_customer_listAPI(searchKey: textToSearch)
        }else  if textToSearch.count == 0 {
            call_customer_listAPI(searchKey: "")
        }
    }

    //***********************************************
    //MARK:- APi Calling
    //***********************************************
    func call_customer_listAPI(searchKey key:String){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
        param[params.kstart] = "0"
        param[params.ksearch_keyword] = key
        
        ServerManager.shared.POST(url: ApiAction.customer_list  , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(UserReviewList.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                print("success")
                self.arrUserList.removeAll()
                self.arrUserList = obj.userList!
                
                if self.arrUserList.count == 0{
                                     self.tblResult.displayBackgroundImageWithText(text: "Data not Found" , fontStyle: "", fontSize: 15, imgName: "cloud")
                                 }else{
                                   self.tblResult.displayBackgroundImageWithText(text: "" , fontStyle: "", fontSize: 15, imgName: "")
                                 }
                
                self.tblResult.reloadData()
            } else {
                print("failure")
            }
        }
    }
}
extension UserListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrUserList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserReviewCell", for: indexPath) as! UserReviewCell
        let obj = arrUserList[indexPath.row]
        cell.delegate = self
        cell.lblName.text = obj.username
        cell.ratingView.rating = Double(obj.user_avg_rating!) ?? 0.0
        cell.lblBaseOn.text =  "Based on \(obj.rating_count!) Reviews"
        cell.impProfile.sd_setImage(with: URL(string: obj.profile_image! ), placeholderImage: UIImage(named: "user"))
        if indexPath.row == (arrCategory.count - 1) && (tblResult.contentOffset.y > pointContentOffset.y) {
        
            if !isDownloading {
                isDownloading = true
                isSearch = false
                if arrCategory.count % 10 == 0 {
                    page += 10
                    self.call_customer_listAPI(searchKey: searchKey)
                }
            }
        }
        return cell
    }
}
extension UserListVC: UserReviewDelegate {
    func actionGiveReview(cell: UserReviewCell) {
        guard let indexPath = tblResult.indexPath(for: cell) else {
              return
          }
        print(indexPath.row)
        let obj = arrUserList[indexPath.row]
        let vc = UserReview.instance(storyBoard: .Review) as! UserReview
        vc.dictUserList = obj
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
