//
//  ReviewVC.swift
//  IRatePro
//
//  Created by mac on 11/01/21.
//

import UIKit

class ReviewVC: UIViewController , UITableViewDelegate , UITableViewDataSource , Segment2TotalReviewDelegate, ReviewDelegate, UserReviewDelegate{
   
    
    @IBOutlet weak var viewPopup: UIView!
    @IBOutlet weak var tblreview: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var tfDetail: UITextField!
    @IBOutlet weak var tvDescription: KMPlaceholderTextView!
    @IBOutlet weak var tfTitle: UITextField!
    
    var arrRatingList = [RatingList]()
    var arrUserList = [UserListReview]()
    
    var rateId = ""
    var isSearch = false
    var page:Int = 0
    var isDownloading = false
    var pointContentOffset = CGPoint.zero
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfSearch.addTarget(self, action: #selector(SearchDidChange(_:)), for: .editingChanged)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.navigationController?.setNavigationBarHidden(true, animated: true)
        let custombar = self.tabBarController as! CustomTabBarController
        custombar.isHiddenTabBar(hidden: false)
        segmentedControl.selectedSegmentIndex = 0
        arrRatingList.removeAll()
        page = 0
        isDownloading = false
        call_productAlllist_listAPI("3")
       
    }
    

     override func viewDidAppear(_ animated: Bool) {
         super.viewDidAppear(animated)
          self.navigationController?.setNavigationBarHidden(true, animated: true)
         let custombar = self.tabBarController as! CustomTabBarController
         custombar.isHiddenTabBar(hidden: false)
     }
     
     
     
     override func viewWillDisappear(_ animated: Bool) {
         let custombar = self.tabBarController as? CustomTabBarController
                        custombar?.isHiddenTabBar(hidden: true)
     }

    
    
    func actionRemove(cell: Segment2TotalReviewCell) {
        
    }
    
    func actionMessage(cell: Segment2TotalReviewCell) {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentedControl.selectedSegmentIndex == 2 {
            return arrUserList.count
        }else{
            return arrRatingList.count
        }
        
    }
    
   
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if segmentedControl.selectedSegmentIndex == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! ReviewCell
            cell.delegate = self
            let obj =    arrRatingList[indexPath.row]
            if obj.type == "1"{
                cell.lblName.text = obj.companyName
                cell.impCompany.sd_setImage(with: URL(string: obj.companyProfileImage ), placeholderImage: UIImage(named: "user"))
            }else if obj.type == "2"{
                cell.lblName.text = obj.productName
                cell.impCompany.sd_setImage(with: URL(string: obj.productImageThumb ), placeholderImage: UIImage(named: "user"))
            }else{
                cell.lblName.text = obj.toUsername
                cell.impCompany.sd_setImage(with: URL(string: obj.toProfileImage ), placeholderImage: UIImage(named: "user"))
            }
            if obj.userType == "1"{
                cell.lblPName.text = obj.fromUsername
                cell.impReviewer.sd_setImage(with: URL(string: obj.fromProfileImage ), placeholderImage: UIImage(named: "user"))
            }else  if obj.userType == "2"{
                cell.lblPName.text = obj.fromCompanyName
                cell.impReviewer.sd_setImage(with: URL(string: obj.fromCompanyProfileImage ), placeholderImage: UIImage(named: "user"))
            }
            
               cell.lblRating.text = obj.rate  + "/5"
               cell.ratingView.rating = Double(obj.rate) ?? 0.0
            cell.lbTitle.text = obj.reviewTitle
            cell.lblDiscription.text = obj.reviewDescription
            cell.lblDate.text = obj.createdAt
            if indexPath.row == (arrRatingList.count - 1) && (tblreview.contentOffset.y > pointContentOffset.y) {
                                       if !isDownloading {
                                           isDownloading = true
                                           if arrRatingList.count % 10 == 0 {
                                               page += 10
                                            self.call_productAlllist_listAPI("3", tfSearch.text)
                                           }
                                       }
                                   }
            return cell
            
        }else  if segmentedControl.selectedSegmentIndex == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Segment2TotalReview2Cell", for: indexPath) as! Segment2TotalReviewCell
            cell.delegate = self
            let obj =    arrRatingList[indexPath.row]
            if obj.type == "1"{
                cell.lblName.text = obj.companyName
                cell.impProfile.sd_setImage(with: URL(string: obj.companyProfileImage ), placeholderImage: UIImage(named: "user"))
            }else if obj.type == "2"{
                cell.lblName.text = obj.productName
                cell.impProfile.sd_setImage(with: URL(string: obj.productImageThumb ), placeholderImage: UIImage(named: "user"))
            }else{
                cell.lblName.text = obj.toUsername
                cell.impProfile.sd_setImage(with: URL(string: obj.toProfileImage ), placeholderImage: UIImage(named: "user"))
            }
            cell.lblTitle.text = obj.reviewTitle
            cell.lbldiscription.text = obj.reviewDescription
            cell.lblRating.text = obj.rate  + "/5"
            cell.ratingView.rating = Double(obj.rate) ?? 0.0
            cell.lblDate.text = obj.createdAt
            if indexPath.row == (arrRatingList.count - 1) && (tblreview.contentOffset.y > pointContentOffset.y) {
                                       if !isDownloading {
                                           isDownloading = true
                                           if arrRatingList.count % 10 == 0 {
                                               page += 10
                                            self.call_productAlllist_listAPI("4", tfSearch.text)
                                           }
                                       }
                                   }
            return cell
            
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserReviewCell", for: indexPath) as! UserReviewCell
            let obj =    arrUserList[indexPath.row]
            cell.delegate = self
            cell.lblName.text = obj.username
            cell.ratingView.rating = Double(obj.user_avg_rating!) ?? 0.0
            cell.lblBaseOn.text =  "Based on \(obj.rating_count!) Reviews"
            cell.impProfile.sd_setImage(with: URL(string: obj.profile_image! ), placeholderImage: UIImage(named: "user"))
            
            
            if indexPath.row == (arrUserList.count - 1) && (tblreview.contentOffset.y > pointContentOffset.y) {
                                       if !isDownloading {
                                           isDownloading = true
                                           if arrUserList.count % 10 == 0 {
                                               page += 10
                                         //   self.call_productAlllist_listAPI("4", tfSearch.text)
                                            self.call_customer_listAPI(tfSearch.text)
                                           }
                                       }
                                   }
            
            return cell
            
        }
        
    }
    
 
 
    @IBAction func actionTicketListing(_ sender: Any) {
        let vc2 = TicketListingVC.instance(storyBoard: .Review) as! TicketListingVC
        self.navigationController!.pushViewController(vc2, animated: true)
    }
    @IBAction func actionSegment(_ sender: Any) {
        page = 0
        isDownloading = false
        if segmentedControl.selectedSegmentIndex == 0 {
            arrRatingList.removeAll()
          //  call_productAlllist_listAPI("3")
            self.call_productAlllist_listAPI("3", tfSearch.text)
        }else if segmentedControl.selectedSegmentIndex == 1{
            arrRatingList.removeAll()
            self.call_productAlllist_listAPI("4", tfSearch.text)
           // call_productAlllist_listAPI("4")
        }else{
            self.arrUserList.removeAll()
            call_customer_listAPI(tfSearch.text)
        }
    }
    
    func actionRemove(cell: ReviewCell) {
        
    }
    
    func actionEdit(cell: ReviewCell) {
        guard let indexPath = tblreview.indexPath(for: cell) else {
              return
          }
        print(indexPath.row)
        let obj =    arrRatingList[indexPath.row]
        rateId = obj.id
        self.viewPopup.frame = self.view.frame
        self.viewPopup.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        self.view.addSubview(viewPopup)
    }
    
    @IBAction func actionPopupClose(_ sender: Any) {
        self.viewPopup.removeFromSuperview()
    }
    @IBAction func actionAddReview(_ sender: Any) {
        let valid = validation()
        if valid.status {
            call_addTicket_API()
           
        }else{
            presentAlert("", msgStr: valid.message, controller: self)
        }
    }
    
    
    func validation() -> (status:Bool, message:String) {
        var msg:String = ""
        var status:Bool = true
        if let fullname = tfTitle.text , fullname.isEmpty {
            status = false
            msg = Validation.kEnterTitle.rawValue
        }
        else if  tfDetail.text! == ""{
            status = false
            msg = Validation.kEnterDetail.rawValue
        }
        else if  tvDescription.text! == ""{
            status = false
            msg = Validation.kDiscription.rawValue
        }
        return (status:status, message:msg)
    }
    
    
    
    
    func actionShare(cell: ReviewCell) {
        
    }
    
    //3
    func actionGiveReview(cell: UserReviewCell) {
        guard let indexPath = tblreview.indexPath(for: cell) else {
              return
          }
        print(indexPath.row)
        let obj =    arrUserList[indexPath.row]
        let vc = UserReview.instance(storyBoard: .Review) as! UserReview
        vc.dictUserList = obj
        self.navigationController?.pushViewController(vc, animated: true)
    }


}

extension ReviewVC{
    func call_productAlllist_listAPI( _ type : String , _ Search : String? = ""){
        var param = [String : Any]()
        param[params.kid] = AppDataHelper.shard.logins.userinfo.userID
        param[params.kstart] = page
        param[params.ktype] = type
        param[params.ksearch_keyword] = Search
        
        
        ServerManager.shared.POST(url: ApiAction.rateing_list  , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
          guard  let obj = try? JSONDecoder().decode(ReviewtListingModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                print("success--")
                
                if obj.ratingList.count != 0{
                    self.arrRatingList.append(contentsOf: obj.ratingList)
                    self.isDownloading  = false
                }else{
                    self.isDownloading  = true
                }
                
                
               /* if self.isSearch {
                    self.arrRatingList.removeAll()
                    self.arrRatingList = obj.ratingList
                    self.isSearch = false
                    
                }else{
                    if obj.ratingList.count != 0{
                        self.arrRatingList.append(contentsOf: obj.ratingList)
                    }
                }*/
                self.tblreview.reloadData()
                print("self.arrUserList --\(self.arrRatingList.count)-")
                
            } else {
                self.isDownloading  = true
                print("failure")
            }
        }
        
    }
    
    
    func call_productAlllist_listAPISearch( _ type : String , _ Search : String? = ""){
        var param = [String : Any]()
        param[params.kid] = AppDataHelper.shard.logins.userinfo.userID
        param[params.kstart] = page
        param[params.ktype] = type
        param[params.ksearch_keyword] = Search
        
        
        ServerManager.shared.POST(url: ApiAction.rateing_list  , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
          guard  let obj = try? JSONDecoder().decode(ReviewtListingModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                print("success")
                self.arrRatingList.removeAll()
                if obj.ratingList.count != 0{
                    self.arrRatingList = obj.ratingList
                    self.isDownloading = false
                }else{
                    self.isDownloading = true
                    
                }
                self.tblreview.reloadData()
                print("self.arrUserList --\(self.arrRatingList.count)-")
                
            } else {
                self.isDownloading = true
                print("failure")
            }
        }
        
    }
    
    
    func call_customer_listAPI(_ Search : String? = ""){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
        param[params.kstart] = page
        param[params.ksearch_keyword] = Search
        
        ServerManager.shared.POST(url: ApiAction.customer_list  , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
          guard  let obj = try? JSONDecoder().decode(UserReviewList.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                print("success")
                if obj.userList!.count != 0{
                    self.arrUserList.append(contentsOf: obj.userList!)
                    self.isDownloading  = false
                }else{
                    self.isDownloading  = true
                }
                
                self.tblreview.reloadData()
                
                print("self.arrUserList --\(self.arrUserList.count)-")
            } else {
                print("failure")
            }
        }
        
    }
    func call_customer_listAPISearch(_ Search : String? = ""){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
        param[params.kstart] = page
        param[params.ksearch_keyword] = Search
        tfSearch.isEnabled  =  false
        
        ServerManager.shared.POST(url: ApiAction.customer_list  , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
          guard  let obj = try? JSONDecoder().decode(UserReviewList.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                print("success")
                self.arrUserList.removeAll()
                self.arrUserList = obj.userList!
                
                self.tblreview.reloadData()
                print("self.arrUserList --\(self.arrUserList.count)-")
                self.tfSearch.isEnabled  =  true
                self.tfSearch.becomeFirstResponder()
            } else {
                print("failure")
                self.tfSearch.isEnabled  =  true
                self.tfSearch.becomeFirstResponder()
            }
        }
        
    }
    
    
    
    func call_addTicket_API(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
        param[params.krate_id] = rateId
        param[params.ktitle] = tfTitle.text
        param[params.kdetail] = tfDetail.text
        param[params.kdescription] = tvDescription.text
        ServerManager.shared.POST(url: ApiAction.addTicket  , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
          guard  let obj = try? JSONDecoder().decode(OtpModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                
                DispatchQueue.main.async(execute: {
                        let _ = presentAlertWithOptions("", message: obj.message, controller: self, buttons: ["Ok"]) { (alert, actionTag) in
                            if actionTag == 0 {
                                self.viewPopup.removeFromSuperview()
                            }
                        }
                    })
               
            } else {
                print("failure")
            }
        }
        
    }
    
    
    
    
}


extension ReviewVC:  UITextFieldDelegate  {
    
    @objc func SearchDidChange(_ textField: UITextField) {
          let textToSearch = textField.text ?? ""
        print("textToSearch-=-=-=-=-=-=-=-=-=-=--\(textToSearch)")
        isSearch = true
        page = 0
        isDownloading = false
        
        if textToSearch.count > 2{
            if segmentedControl.selectedSegmentIndex == 0 {
               
                self.call_productAlllist_listAPISearch("3", textToSearch)
            }else  if segmentedControl.selectedSegmentIndex == 1 {
              
                self.call_productAlllist_listAPISearch("4", textToSearch)
            }else{
                self.call_customer_listAPISearch(textToSearch)
            }
            
        }else  if textToSearch.count == 0 {
            if segmentedControl.selectedSegmentIndex == 0 {
                self.call_productAlllist_listAPISearch("3", "")
            }else  if segmentedControl.selectedSegmentIndex == 1 {
                self.call_productAlllist_listAPISearch("4", "")
            }else{
                self.call_customer_listAPISearch("")
            }
        }
       
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
   
    
}
//-=================================+Cell===================================








protocol ReviewDelegate:NSObject {
    func actionRemove(cell:ReviewCell)
    func actionEdit(cell:ReviewCell)
    func actionShare(cell:ReviewCell)
}


class ReviewCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var impCompany: UIImageView!
    //
    @IBOutlet weak var lblPName: UILabel!
    @IBOutlet weak var impReviewer: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lblDiscription: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    weak var delegate: ReviewDelegate?
    
    @IBAction func actionEdit(_ sender: Any) {
        delegate?.actionEdit(cell: self)
    }
    
    @IBAction func actionRemove(_ sender: Any) {
        delegate?.actionRemove(cell: self)
    }
    
    @IBAction func actionShare(_ sender: Any) {
        delegate?.actionShare(cell: self)
    }
    


}

//2


protocol Segment2TotalReviewDelegate:NSObject {
    func actionRemove(cell:Segment2TotalReviewCell)
    func actionMessage
    (cell:Segment2TotalReviewCell)
}

class Segment2TotalReviewCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var lbldiscription: UILabel!
    @IBOutlet weak var impProfile: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    weak var delegate: Segment2TotalReviewDelegate?
    
    @IBAction func actionMessage(_ sender: Any) {
        delegate?.actionMessage(cell: self)
    }
    
    @IBAction func actionRemove(_ sender: Any) {
        delegate?.actionRemove(cell: self)
    }


}

//3


protocol UserReviewDelegate:NSObject {
    func actionGiveReview(cell:UserReviewCell)
}

class UserReviewCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var impProfile: UIImageView!
    @IBOutlet weak var lblBaseOn: UILabel!
    
    weak var delegate: UserReviewDelegate?
    
    @IBAction func actionReview(_ sender: Any) {
        delegate?.actionGiveReview(cell: self)
    }
    
 


}







