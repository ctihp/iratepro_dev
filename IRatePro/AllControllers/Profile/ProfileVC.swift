//
//  ProfileVC.swift
//  IRatePro
//
//  Created by mac on 11/01/21.
//

import UIKit
import SDWebImage

class ProfileVC: UIViewController {
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserNameTop: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblMobileNumber: UILabel!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var btnSwitch: UISwitch!
    
    var userinfo : UserinfoProfile?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBg.layer.cornerRadius = 42
        viewBg.layer.maskedCorners = [ .layerMinXMaxYCorner ]
       
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      call_getProfile_Api()
        
        
    }


    @IBAction func actionSwitch(_ sender: Any) {
        
        
        
        let vc = EditProfilePro.instance(storyBoard: .Profile) as! EditProfilePro
    //    vc.comeFromCustomerProfile = true
        //vc.callAPI()
        vc.userinfo = userinfo
        self.navigationController?.pushViewController(vc, animated: false)
    }
    

    


   @objc func methodOfReceivedNotification(_ notification: NSNotification) {
          print("call observer")
    call_getProfile_Api()
         
      }
    
    
    
    
    @IBAction func actioneditProfile(_ sender: Any) {
        let vc = EditProfile.instance(storyBoard: .Profile) as! EditProfile
        vc.userinfo = userinfo
                    self.navigationController?.pushViewController(vc, animated: true)
        
       }
    
    @IBAction func actionSetting(_ sender: Any) {
        let vc = SettingVC.instance(storyBoard: .Profile) as! SettingVC
                    self.navigationController?.pushViewController(vc, animated: true)
       }
    
    
    @IBAction func actionLogout(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let _ = presentAlertWithOptions("", message: "Sure to logout", controller: self, buttons: ["Ok" , "Cancel"]) { (alert, actionTag) in
                if actionTag == 0 {
                    self.call_Logout_Api()
                } else {
                    //Cancel Button
                }
            }
        })
        
       }
    
    
    
    
    
    


}

extension ProfileVC{
    func call_getProfile_Api(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
        ServerManager.shared.POST(url: ApiAction.getProfile , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
           guard  let obj = try? JSONDecoder().decode(GetProfileModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                self.setProfileData(obj.userinfo)
                self.userinfo = obj.userinfo
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    func call_Logout_Api(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
        ServerManager.shared.POST(url: ApiAction.logout , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
         
            let vc = LoginVC.instance(storyBoard: .Signup) as! LoginVC
               UIApplication.shared.windows.first?.rootViewController = vc
               UIApplication.shared.windows.first?.makeKeyAndVisible()
           // self.navigationController!.setViewControllers([LoginVC.instance(storyBoard: .Signup)], animated: true)
        }
    }
    
    func setProfileData(_ obj : UserinfoProfile ){
        lblUserName.text = obj.username
        lblUserNameTop.text = obj.username
        lblEmail.text = obj.email
        lblMobileNumber.text = obj.phoneNumber
        ratingView.rating = Double(obj.avgRating) ?? 0.0
        lblRating.text = obj.avgRating + "/5"
        imgProfile.sd_setImage(with: URL(string: obj.profileImage ), placeholderImage: UIImage(named: "logo"))
       
        
    }
}


