//
//  AddProductVC.swift
//  IRatePro
//
//  Created by mac on 05/02/21.
//

import UIKit

class AddProductVC: UIViewController, TDImagePickerDelegate{

    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfDetails: UITextField!
    @IBOutlet weak var tfDescription: UITextField!
    @IBOutlet weak var tfCategory: UITextField!
    @IBOutlet weak var viewBg: UIView!
    var arrCategory = [CategoryList]()
    var categoryId = ""
    var imagepickerDocument:TDImagePicker!
    var pickerView = UIPickerView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBg.layer.cornerRadius = 40
        viewBg.layer.maskedCorners = [ .layerMinXMaxYCorner]
        tfCategory.inputView = pickerView
        pickerView.delegate = self
        pickerView.dataSource = self
        imagepickerDocument = TDImagePicker(presentationController: self, delegate: self)
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionAddImg(_ sender: UIButton) {
        print("upload verfication id button clicked")
        imagepickerDocument.present(from: sender)
        
    }
    @IBAction func actionAddProduct(_ sender: Any) {
        let valid = validation()
        if valid.status {
            call_addProduct_API()
        }else{
            presentAlert("", msgStr: valid.message, controller: self)
        }
        
    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
  
    
    func didSelect(image: UIImage?) {
        imgProduct.image = image
        
    }
    
}


extension AddProductVC:  UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrCategory.count
       
    }

    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrCategory[row].categoryName
        
    }

    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        tfCategory.text = arrCategory[row].categoryName
        categoryId = arrCategory[row].categoryID
    }
    
    
    func validation() -> (status:Bool, message:String) {
        var msg:String = ""
        var status:Bool = true
        
        if let fullname = tfName.text , fullname.isEmpty {
            status = false
            msg = Validation.kProductName.rawValue
        }
        else if tfDetails.text == ""{
            status = false
            msg = Validation.kProductDetail.rawValue
        }
        else if tfDescription.text == ""{
            status = false
            msg = Validation.kProductDiscription.rawValue
        }else if categoryId == ""{
            status = false
            msg = Validation.kSelectCategory.rawValue
        }
        
        return (status:status, message:msg)
        
        }
    
    
    func call_addProduct_API(){
         var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
        param[params.kproduct_name] = tfName.text
        param[params.kcategory] = categoryId
        param[params.kproduct_detail] = tfDetails.text
        param[params.kproduct_description] = tfDescription.text
        
         ServerManager.shared.POSTWithImage(url: ApiAction.addProduct, param: param, imageView: imgProduct) { (data, error) in
           
                 guard let data = data else {
                     print("data not available")
                     return
                 }
                 
                 guard  let obj = try? JSONDecoder().decode(OtpModel.self, from: data) else {
                      return
                  }
                  if obj.success == ResponseApis.KSuccess {
                    NotificationCenter.default.post(Notification(name: .providerProduct, object: nil, userInfo: nil))
                      let _ = presentAlertWithOptions(kAppName, message: obj.message, controller: self, buttons: ["Ok"]) { (alert, actionTag) in
                          if actionTag == 0 {
                              //Ok Button
                            
                              self.navigationController?.popViewController(animated: true)
                          } else {
                              //Cancel Button
                          }
                      }
                  } else {
                      print("failure")
                      presentAlert("", msgStr: obj.message, controller: self)
                  }
             }
             
         }
}

