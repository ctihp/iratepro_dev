//
//  EditProfilePro.swift
//  IRatePro
//
//  Created by mac on 20/01/21.
//

import UIKit
import TagListView

class EditProfilePro: UIViewController {

    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var tfUserName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfMobileNumber: UITextField!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var tfCompanyName: UITextField!
    @IBOutlet weak var tfEinNumber: UITextField!
    @IBOutlet weak var tfBusniessType: UITextField!
    @IBOutlet weak var tfWebsiteUrl: UITextField!
  //  @IBOutlet weak var tfCategory: UITextField!
    @IBOutlet weak var tfCompanyDiscriprition: UITextField!
    @IBOutlet weak var tfAddress: UITextField!
    @IBOutlet weak var lblName: UILabel!
    var pickerView = UIPickerView()
    @IBOutlet weak var lblCategory: UIButton!
    @IBOutlet weak var tblviewCategory: UITableView!
    @IBOutlet weak var tagListView: TagListView!
    @IBOutlet weak var viewPopup: UIView!
    
    var imagepickerDocument:TDImagePicker!
    var arrCategory = [CategoryList]()
    var userinfo : UserinfoProfile?
    var arrBusiness = [String]()
    var strSkill = [String]()
    var isCompanyImage = false
    
    var isPopToRoot = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBg.layer.cornerRadius = 42
        viewBg.layer.maskedCorners = [ .layerMinXMaxYCorner ]
       
        arrBusiness = ["Services" , "Transportation"]
        tfBusniessType.inputView = pickerView
     //   tfCategory.inputView = pickerView
         pickerView.delegate = self
         pickerView.dataSource = self
        call_category_listAPI()
        tagListView.delegate = self
        imagepickerDocument = TDImagePicker(presentationController: self, delegate: self)
        isCompanyImage = false
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
//            if  !isPopToRoot{
//                self.navigationController?.popToRootViewController(animated: false)
//            }
        }
    
    
    
    @IBAction func actionBack(_ sender: Any) {
        //UserDefaults.standard.set(false, forKey: "isCustomer")
        NotificationCenter.default.post(Notification(name: .providerProfile))
        isPopToRoot  = true
           self.navigationController?.popViewController(animated: true)
       }
    
    
    @IBAction func actionUpdate(_ sender: Any) {
        let valid = validation()
        if valid.status {
            var  arrTemp : [[String:Any]] = []
            for dicts in arrCategory {
               var dict = [String:Any]()
                if dicts.status2  {
                    dict["item_id"] = dicts.categoryID
                    dict["item_text"] = dicts.categoryName
                    arrTemp.append(dict)
                }
             
           }
            if arrTemp.count > 0 {
                let str =  json(from: arrTemp)
                call_SignupAPI(str!)
                
            }else{
                call_SignupAPI( "")
            }
           
            
 
    } else {
        presentAlert("", msgStr: valid.message, controller: self)
        
        }
    }
    
    public func json(from object:Any) -> String? {
           guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
               return nil
           }
           return String(data: data, encoding: String.Encoding.utf8)
       }
    
    func setProfileData(_ obj : UserinfoProfile ){
        tfUserName.text = obj.username
        tfEmail.text = obj.email
        tfMobileNumber.text = obj.phoneNumber
        ratingView.rating = Double(obj.ratingCount) ?? 0.0
        lblRating.text = obj.ratingCount  + "/5"
        print(obj.company_profile_image)
        if obj.company_profile_image  == ""{
            isCompanyImage = false
        }else{
            isCompanyImage = true
        }
        imgProfile.sd_setImage(with: URL(string: obj.company_profile_image ), placeholderImage: UIImage(named: "user"))
        
        tfCompanyName.text = obj.username
        lblName.text = obj.companyName
        tfEinNumber.text = obj.ein
        tfBusniessType.text = obj.businessType
       tfWebsiteUrl.text = obj.website
   //    tfCategory.text = obj.category
        tfCompanyDiscriprition.text = obj.companyDescription
        tfAddress.text = obj.address
        print(obj.category)
        
        for i in 0..<arrCategory.count {
            let dict = arrCategory[i]
            print(dict.categoryID)
            
            for dict2 in obj.category{
                if   dict.categoryID == dict2.itemID{
                                arrCategory[i].status2 = true
                           setTag()
                       }
            }

        }
    }
}


extension EditProfilePro{
   /* func call_SignupAPI2(){
        var param = [String : Any]()
        param[params.kusername] = tfUserName.text
        param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
        
        ServerManager.shared.POST(url: ApiAction.updateProfile , param: param, true,header: nil) { (data, error) in
            
            
            
            guard let data = data else {
                print("data not available")
                return
            }
           guard  let obj = try? JSONDecoder().decode(SignupModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                let _ = presentAlertWithOptions(kAppName, message: obj.message!, controller: self, buttons: ["Ok"]) { (alert, actionTag) in
                    if actionTag == 0 {
                        //Ok Button
                        
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        //Cancel Button
                    }
                }
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
        
    }*/
    
    
    func call_SignupAPI( _ strCategory : String? = "" ){
         var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
        param[params.kusername] = tfUserName.text
        param[params.kemail] = tfEmail.text
        param[params.kein] = tfEinNumber.text
        param[params.kbusiness_type] = tfBusniessType.text
        param[params.kaddress] = tfAddress.text
        param[params.kwebsite] = tfWebsiteUrl.text
        param[params.kcompany_description] = tfCompanyDiscriprition.text
        param[params.kcategorys] = strCategory
        param[params.kcompany_name] = tfCompanyName.text
        
        var imagesParams = [String:Data]()
        imagesParams[params.kcompany_image] = imgProfile.image?.jpegData(compressionQuality: 0.5)
         ServerManager.shared.POSTWithImage(url: ApiAction.updateProfile, param: param, imgParam: params.kcompany_image, imageView: imgProfile) { (data, error) in
           
                 guard let data = data else {
                     print("data not available")
                     return
                 }
                 
                 guard  let obj = try? JSONDecoder().decode(GetProfileModel.self, from: data) else {
                      return
                  }
                  if obj.success == ResponseApis.KSuccess {
                    let _ = presentAlertWithOptions(kAppName, message: obj.message, controller: self, buttons: ["Ok"]) { (alert, actionTag) in
                          if actionTag == 0 {
                              //Ok Button
                            UserDefaults.standard.setLoggedIn(value: true)
                            AppDataHelper.shard.logins = obj
                            UserDefaults.standard.setUserData(value: data)
                         //   print(AppDataHelper.shard.logins.userinfo.userID)
                            
                            if AppDataHelper.shard.logins.userinfo.businessStatus == "0"{
                                UserDefaults.standard.set(true, forKey: "isCustomer")
                            }else{
                                UserDefaults.standard.set(false, forKey: "isCustomer")
                            }
                            //-----------
                            
                            NotificationCenter.default.post(Notification(name: .providerProfile))
                              self.navigationController?.popViewController(animated: true)
                          } else {
                              //Cancel Button
                          }
                      }
                  } else {
                      print("failure")
                      presentAlert("", msgStr: obj.message, controller: self)
                  }
             }
             
         }
    
    func validation() -> (status:Bool, message:String) {
        var msg:String = ""
        var status:Bool = true
        
        if let fullname = tfUserName.text , fullname.isEmpty {
            status = false
            msg = Validation.kEnterName.rawValue
        }
        else if let fullname = tfEmail.text , fullname.isEmpty {
            status = false
            msg = Validation.kEnterEmail.rawValue
        }
        
        else   if tfCompanyName.text == ""{
            status = false
            msg = Validation.kEnterCompanyName.rawValue
        }else if tfEinNumber.text == ""{
            status = false
            msg = Validation.kEnterEINNumber.rawValue
        }else if tfBusniessType.text == ""{
            status = false
            msg = Validation.kEnterBusinessType.rawValue
        }
        else if tfWebsiteUrl.text == ""{
            status = false
            msg = Validation.kEnterWebUrl.rawValue
        }
        else if strSkill.count == 0{
            status = false
            msg = Validation.kSelectCategory.rawValue
        }
        
        else if tfCompanyDiscriprition.text == ""{
            status = false
            msg = Validation.kEnterCompanyDis.rawValue
        }
        else if tfAddress.text == ""{
            status = false
            msg = Validation.kEnterAddress.rawValue
        }else if isCompanyImage == false{
            status = false
            msg = Validation.kCompanyImg.rawValue
        }
        
        
        return (status:status, message:msg)
    }
}

extension EditProfilePro:  UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrBusiness.count
       
    }

    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrBusiness[row]
        
    }

    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        tfBusniessType.text = arrBusiness[row]
    }
    
    func call_category_listAPI( ){
        let parameter = [String : Any]()
       
        ServerManager.shared.POST(url: ApiAction.category_list  , param: parameter, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
           guard  let obj = try? JSONDecoder().decode(CategoryModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                print("success")
                self.arrCategory = obj.categoryList
                self.tblviewCategory.reloadData()
                
                self.setProfileData(self.userinfo!)
                
            } else {
                print("failure")
            }
        }
        
    }
}




extension EditProfilePro: UITableViewDelegate , UITableViewDataSource, TagListViewDelegate , TDImagePickerDelegate{
   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrCategory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell", for: indexPath) as! categoryCell
     
     let obj =    arrCategory[indexPath.row]
        cell.lblText.text = obj.categoryName
        if obj.status2{
            cell.imgCheck.image = UIImage(named: "check")
        }else{
            cell.imgCheck.image = UIImage(named: "uncheck")
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // var obj = arrSearch[indexPath.row]
    if arrCategory[indexPath.row].status2{
        arrCategory[indexPath.row].status2 = false
        
     }else{
        arrCategory[indexPath.row].status2 = true
     }
     tblviewCategory.reloadData()
     setTag()
 }
    
    
    // MARK: TagListViewDelegate
func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
    print("Tag pressed: \(title), \(sender)")
    tagView.isSelected = !tagView.isSelected
}

func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
    var mycount = 0
    for i in 0..<arrCategory.count {
        let dict = arrCategory[i]
        if dict.categoryName == title {
          arrCategory[i].status2 = false
            self.tblviewCategory.reloadData()
        }
        if arrCategory[i].status2 == true{
            mycount += 1
        }
    }
    if mycount > 0 {
        lblCategory.isHidden = true
    }else{
        lblCategory.isHidden = false
        }
    tblviewCategory.reloadData()
  
    sender.removeTagView(tagView)
}


func setTag(){
    strSkill.removeAll()
    tagListView.removeAllTags()
    for dict in arrCategory{
        if dict.status2{
          strSkill.append(dict.categoryName)
        }
    }
    if strSkill.count > 0 {
        lblCategory.isHidden = true
    }else{
        lblCategory.isHidden = false
        }
    tagListView.addTags(strSkill)
}
    
    @IBAction func actionCategory(_ sender: Any) {
        self.viewPopup.frame = self.view.frame
        self.viewPopup.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        self.view.addSubview(viewPopup)
        
    }
    
    @IBAction func actionPopupClose(_ sender: Any) {
        self.viewPopup.removeFromSuperview()
    }
    
    @IBAction func uploadVerificationIDBtnAction(_ sender: UIButton) {
        print("upload verfication id button clicked")
        imagepickerDocument.present(from: sender)
        
    }
    
    func didSelect(image: UIImage?) {
        imgProfile.image = image
        isCompanyImage = true
    }
    
    
    
    
    
}

