//
//  SignupSigninVC.swift
//  IRatePro
//
//  Created by mac on 24/12/20.
//

import UIKit

class SignupSigninVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func actionCreateAccount(_ sender: Any) {
        let vc = OtpVerificationVC.instance(storyBoard: .Signup) as! OtpVerificationVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func actionLogin(_ sender: Any) {
     let vc = LoginVC.instance(storyBoard: .Signup) as! LoginVC
   self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
}


