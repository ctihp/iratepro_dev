//
//  Guide1VC.swift
//  IRatePro
//
//  Created by mac on 24/12/20.
//

import UIKit


class Guide1VC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
     
        // Do any additional setup after loading the view.
        
     
    }
    
    
    @IBAction func actionNext(_ sender: Any) {
        let vc = Guide2VC.instance(storyBoard: .Guide) as! Guide2VC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
}


extension UIViewController {
    func topMostViewController() -> UIViewController {

        if let presented = self.presentedViewController {
            return presented.topMostViewController()
        }

        if let navigation = self as? UINavigationController {
            return navigation.visibleViewController?.topMostViewController() ?? navigation
        }

        if let tab = self as? UITabBarController {
            return tab.selectedViewController?.topMostViewController() ?? tab
    }

        return self
    }
}

extension UIApplication {
    func topMostViewController() -> UIViewController? {
        return self.keyWindow?.rootViewController?.topMostViewController()
    }
}

