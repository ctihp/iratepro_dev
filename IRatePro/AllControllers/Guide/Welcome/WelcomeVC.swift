//
//  ViewController.swift
//  IRatePro
//
//  Created by mac on 23/12/20.
//

import UIKit

class WelcomeVC: KBaseViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if UserDefaults.standard.isLoggedIn() {
            let loginData = UserDefaults.standard.getUserData()
            let sendModal = try? JSONDecoder().decode(GetProfileModel.self, from: loginData)
            AppDataHelper.shard.logins = sendModal
            let vc = CustomTabBarController.instance(storyBoard: .tabBarViewController) as! CustomTabBarController
            vc.tabBar.isHidden = true
            UIApplication.shared.windows[0].rootViewController = vc
            
        }else{
            let seconds = 3.0
            DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
                let vc = Guide1VC.instance(storyBoard: .Guide) as! Guide1VC
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}


extension UIDevice {
    var hasNotch: Bool {
        if #available(iOS 11.0, *) {
           return UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0 > 0
        }
        return false
   }
}





