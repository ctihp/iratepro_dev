//
//  TDPopMenuViewController.swift
//  Traydi
//
//  Created by mac on 16/01/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import UIKit
protocol TDPopProjectVCDelegate {
    func deleteIndex(  buttontag:Int )
}

class TDPopProjectVC: UIViewController {
    
    @IBOutlet weak var btnLicence: UIButton!
    @IBOutlet weak var btnExperience: UIButton!
    @IBOutlet weak var btnLabour: UIButton!
    var typeButton = ""
    var  myIndex :IndexPath?
    var delegate: TDPopProjectVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func actionLicence(_ sender: UIButton ) {
        self.delegate?.deleteIndex( buttontag: sender.tag)
    }
    
    @IBAction func actionExperience(_ sender: UIButton ) {
        self.delegate?.deleteIndex( buttontag: sender.tag)
    }
    
    @IBAction func actionLabour(_ sender: UIButton ) {
        self.delegate?.deleteIndex( buttontag: sender.tag)
    }
}
