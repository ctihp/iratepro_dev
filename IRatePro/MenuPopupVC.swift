//
//  MenuPopupVC.swift
//  IRatePro
//
//  Created by mac on 17/02/21.
//

import UIKit
protocol MenuDelegate {
    func onButtonClick(button str:MenuList)
}
class MenuPopupVC: UIViewController {

    @IBOutlet weak var btnCustomer: UIButton!
    @IBOutlet weak var btnProduct: UIButton!
    @IBOutlet weak var btnCompany: UIButton!
    var delegate: MenuDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    // MARK: - Button Action

    @IBAction func btnCompany_Action(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.onButtonClick(button: .company)
        }
    }
    
    @IBAction func btnProduct_Action(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.onButtonClick(button: .product)
        }
    }
    
    @IBAction func btnCustomer_Action(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.onButtonClick(button: .customer)
        }
    }
}
