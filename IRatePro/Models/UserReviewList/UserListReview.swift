/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct UserListReview : Codable {
	let user_id : String?
	let username : String?
	let email : String?
	let token : String?
	let country_code : String?
	let phone_number : String?
	let business_type : String?
	let ein : String?
	let act_token : String?
	let address : String?
	let fcm_token : String?
	let gender : String?
	let profile_image : String?
	let company_profile_image : String?
	let avg_rating : String?
	let rating_count : String?
	let user_avg_rating : String?
	let user_rating_count : String?
	let company_name : String?
	let company_description : String?
	let website : String?
	let status : String?
	let category : String?
	let business_status : String?
	let device_type : String?

	enum CodingKeys: String, CodingKey {

		case user_id = "user_id"
		case username = "username"
		case email = "email"
		case token = "token"
		case country_code = "country_code"
		case phone_number = "phone_number"
		case business_type = "business_type"
		case ein = "ein"
		case act_token = "act_token"
		case address = "address"
		case fcm_token = "fcm_token"
		case gender = "gender"
		case profile_image = "profile_image"
		case company_profile_image = "company_profile_image"
		case avg_rating = "avg_rating"
		case rating_count = "rating_count"
		case user_avg_rating = "user_avg_rating"
		case user_rating_count = "user_rating_count"
		case company_name = "company_name"
		case company_description = "company_description"
		case website = "website"
		case status = "status"
		case category = "category"
		case business_status = "business_status"
		case device_type = "device_type"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
		username = try values.decodeIfPresent(String.self, forKey: .username)
		email = try values.decodeIfPresent(String.self, forKey: .email)
		token = try values.decodeIfPresent(String.self, forKey: .token)
		country_code = try values.decodeIfPresent(String.self, forKey: .country_code)
		phone_number = try values.decodeIfPresent(String.self, forKey: .phone_number)
		business_type = try values.decodeIfPresent(String.self, forKey: .business_type)
		ein = try values.decodeIfPresent(String.self, forKey: .ein)
		act_token = try values.decodeIfPresent(String.self, forKey: .act_token)
		address = try values.decodeIfPresent(String.self, forKey: .address)
		fcm_token = try values.decodeIfPresent(String.self, forKey: .fcm_token)
		gender = try values.decodeIfPresent(String.self, forKey: .gender)
		profile_image = try values.decodeIfPresent(String.self, forKey: .profile_image)
		company_profile_image = try values.decodeIfPresent(String.self, forKey: .company_profile_image)
		avg_rating = try values.decodeIfPresent(String.self, forKey: .avg_rating)
		rating_count = try values.decodeIfPresent(String.self, forKey: .rating_count)
		user_avg_rating = try values.decodeIfPresent(String.self, forKey: .user_avg_rating)
		user_rating_count = try values.decodeIfPresent(String.self, forKey: .user_rating_count)
		company_name = try values.decodeIfPresent(String.self, forKey: .company_name)
		company_description = try values.decodeIfPresent(String.self, forKey: .company_description)
		website = try values.decodeIfPresent(String.self, forKey: .website)
		status = try values.decodeIfPresent(String.self, forKey: .status)
		category = try values.decodeIfPresent(String.self, forKey: .category)
		business_status = try values.decodeIfPresent(String.self, forKey: .business_status)
		device_type = try values.decodeIfPresent(String.self, forKey: .device_type)
	}

}
