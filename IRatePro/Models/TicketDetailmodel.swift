//
//  TicketDetailmodel.swift
//  IRatePro
//
//  Created by mac on 12/03/21.
//

import Foundation

struct TicketDetailModel: Codable {
    let success, message: String
    let ticketDetail: TicketDetail
    let ratingDetail: RatingDetaill
}

// MARK: - RatingDetail
struct RatingDetaill: Codable {
    let id, userID, toUser, companyID: String
    let customerID, productID, rate, reviewTitle: String
    let reviewDescription, rateImage, rateImageThumb, status: String
    let type, userType, createdAt, fromUsername: String
    let fromCompanyName, fromEmail, fromPhoneNumber, fromFcmToken: String
    let fromProfileImage: String
    let fromCompanyProfileImage, toUsername, toEmail, toPhoneNumber: String
    let toFcmToken: String
    let toProfileImage: String
    let companyName, companyDescription, companyCategory, companyEin: String
    let companyWebsite, companyAddress: String
    let companyProfileImage: String
    let productName, productCategory, productImage, productImageThumb: String
    let productDetail, productDescription, userAvgRating, productAvgRating: String
    let allAvgRating, userRatingCount, productRatingCount, allRatingCount: String
    let companyNameSearch, productNameSearch, usernameSearch: String

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case toUser = "to_user"
        case companyID = "company_id"
        case customerID = "customer_id"
        case productID = "product_id"
        case rate
        case reviewTitle = "review_title"
        case reviewDescription = "review_description"
        case rateImage = "rate_image"
        case rateImageThumb = "rate_image_thumb"
        case status, type
        case userType = "user_type"
        case createdAt = "created_at"
        case fromUsername = "from_username"
        case fromCompanyName = "from_company_name"
        case fromEmail = "from_email"
        case fromPhoneNumber = "from_phone_number"
        case fromFcmToken = "from_fcm_token"
        case fromProfileImage = "from_profile_image"
        case fromCompanyProfileImage = "from_company_profile_image"
        case toUsername = "to_username"
        case toEmail = "to_email"
        case toPhoneNumber = "to_phone_number"
        case toFcmToken = "to_fcm_token"
        case toProfileImage = "to_profile_image"
        case companyName = "company_name"
        case companyDescription = "company_description"
        case companyCategory = "company_category"
        case companyEin = "company_ein"
        case companyWebsite = "company_website"
        case companyAddress = "company_address"
        case companyProfileImage = "company_profile_image"
        case productName = "product_name"
        case productCategory = "product_category"
        case productImage = "product_image"
        case productImageThumb = "product_image_thumb"
        case productDetail = "product_detail"
        case productDescription = "product_description"
        case userAvgRating = "user_avg_rating"
        case productAvgRating = "product_avg_rating"
        case allAvgRating = "all_avg_rating"
        case userRatingCount = "user_rating_count"
        case productRatingCount = "product_rating_count"
        case allRatingCount = "all_rating_count"
        case companyNameSearch = "company_name_search"
        case productNameSearch = "product_name_search"
        case usernameSearch = "username_search"
    }
}

// MARK: - TicketDetail
struct TicketDetail: Codable {
    let id, userID, rateID, title: String
    let detail, ticketDetailDescription, status, createdAt: String
    let reviewTitle, reviewDescription, rate, userType: String
    let fromUserID, fromUserName, fromCompanyName, fromProfileImage: String
    let fromCompanyProfileImage: String

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case rateID = "rate_id"
        case title, detail
        case ticketDetailDescription = "description"
        case status
        case createdAt = "created_at"
        case reviewTitle = "review_title"
        case reviewDescription = "review_description"
        case rate
        case userType = "user_type"
        case fromUserID = "from_user_id"
        case fromUserName = "from_user_name"
        case fromCompanyName = "from_company_name"
        case fromProfileImage = "from_profile_image"
        case fromCompanyProfileImage = "from_company_profile_image"
    }
}
