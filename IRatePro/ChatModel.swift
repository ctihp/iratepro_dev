//
//  ChatModel.swift
//  Traydi
//
//  Created by mac on 18/08/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import Foundation
struct chat : Codable {
    let code : String?
    let message : String?
    var users : [chatModal]?

    enum CodingKeys: String, CodingKey {

        case code = "code"
        case message = "message"
        case users = "message_list"
    }

}

struct chatModal : Codable {

    var id : String?
    var created_at : String?
    var message : String?
    var receiver : String?
    var receiver_image : String?
    var receiver_name : String?
    var sender : String?
    var sender_image : String?
    var sender_name : String?
    var type : String?
    var attachment : String?
    var image : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case created_at = "created_at"
        case message = "message"
        case receiver = "receiver"
        case receiver_image = "receiver_image"
        case receiver_name = "receiver_name"
        case sender = "sender"
        case sender_image = "sender_image"
        case sender_name = "sender_name"
        case type = "type"
         case attachment = "attachment"
        case  image = "image"
    }

}
