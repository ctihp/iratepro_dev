//
//  TicketListingVC.swift
//  IRatePro
//
//  Created by mac on 10/03/21.
//

import UIKit

class TicketListingVC: UIViewController {
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tblTicket: UITableView!
    var arrpendingList = [List]()
    var arrcompletedList = [List]()
    
    @IBOutlet weak  var viewBg : UIView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBg.layer.cornerRadius = 40
        viewBg.layer.maskedCorners = [ .layerMinXMaxYCorner]
        // Do any additional setup after loading the view.
        segmentedControl.selectedSegmentIndex = 0
        call_ticketList_API()
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func actionSegment(_ sender: Any) {
        tblTicket.reloadData()
    }
    
   
    func call_ticketList_API( ){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.userinfo.userID
        ServerManager.shared.POST(url: ApiAction.ticketList  , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
           guard  let obj = try? JSONDecoder().decode(ticketListModel.self, from: data) else {
                return
            }
            if obj.success == ResponseApis.KSuccess {
                print("success")
                self.arrpendingList = obj.pendingList
                self.arrcompletedList = obj.completeList
                self.tblTicket.reloadData()
            
            } else {
                print("failure")
            }
        }
        
    }

}


extension TicketListingVC: UITableViewDelegate , UITableViewDataSource, TicketListingDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if   segmentedControl.selectedSegmentIndex == 0{
            return arrpendingList.count
        }else{
            return arrcompletedList.count
        }
       
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TicketListingCell", for: indexPath) as! TicketListingCell
        if   segmentedControl.selectedSegmentIndex == 0{
            let obj =    arrpendingList[indexPath.row]
               cell.delegate = self
               cell.lblDate.text = obj.createdAt
               cell.lbltitle.text = obj.title
               cell.lblDescription.text = obj.listDescription
            cell.viewRating.rating = Double(obj.rate) ?? 0
            cell.lblRatingCount.text  = obj.rate
            if obj.userType == "1"{
                cell.lblName.text = obj.fromUserName
                cell.imgToUser.sd_setImage(with: URL(string: obj.fromProfileImage ), placeholderImage: UIImage(named: "user"))
            }else{
                cell.lblName.text = obj.fromCompanyName
                cell.imgToUser.sd_setImage(with: URL(string: obj.fromCompanyProfileImage ), placeholderImage: UIImage(named: "user"))
            }
            
        }else{
            let obj = arrcompletedList[indexPath.row]
               cell.delegate = self
               cell.lblName.text = obj.reviewTitle
               cell.lblDate.text = obj.createdAt
               cell.lbltitle.text = obj.title
               cell.lblDescription.text = obj.listDescription
            cell.viewRating.rating = Double(obj.rate) ?? 0
            cell.lblRatingCount.text  = obj.rate + "/5"
            
            if obj.userType == "1"{
                cell.lblName.text = obj.fromUserName
                cell.imgToUser.sd_setImage(with: URL(string: obj.fromProfileImage ), placeholderImage: UIImage(named: "user"))
            }else{
                cell.lblName.text = obj.fromCompanyName
                cell.imgToUser.sd_setImage(with: URL(string: obj.fromCompanyProfileImage ), placeholderImage: UIImage(named: "user"))
            }
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func showDtail(cell: TicketListingCell) {
        guard let indexPath = tblTicket.indexPath(for: cell) else {
              return
          }
        let obj =    arrpendingList[indexPath.row]
        print(obj.id)
        let vc = TDChattingViewController.instance(storyBoard: .Review) as! TDChattingViewController
        vc.myId = obj.id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

@objc protocol TicketListingDelegate {
    @objc optional func showDtail(cell:TicketListingCell)
}

class TicketListingCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblRatingCount: UILabel!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imgToUser: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var viewRating: FloatRatingView!
    var delegate: TicketListingDelegate?
    
    @IBAction func actionChoosePlan(_ sender: Any) {
        delegate?.showDtail!(cell: self)
    }
    
}


struct ticketListModel : Codable{
let success, message: String
let pendingList, completeList: [List]
}

// MARK: - List
struct List: Codable {
    let id, userID, rateID, title: String
    let detail, listDescription, status, createdAt: String
    let reviewTitle, reviewDescription, rate, userType: String
    let fromUserID, fromUserName, fromCompanyName: String
    let fromProfileImage, fromCompanyProfileImage: String

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case rateID = "rate_id"
        case title, detail
        case listDescription = "description"
        case status
        case createdAt = "created_at"
        case reviewTitle = "review_title"
        case reviewDescription = "review_description"
        case rate
        case userType = "user_type"
        case fromUserID = "from_user_id"
        case fromUserName = "from_user_name"
        case fromCompanyName = "from_company_name"
        case fromProfileImage = "from_profile_image"
        case fromCompanyProfileImage = "from_company_profile_image"
    }
}


