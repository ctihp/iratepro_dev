//
//  CustomTabBarController.swift
//  Traydi
//
//  Created by mac on 23/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit


class CustomTabBarController: UITabBarController, UITabBarControllerDelegate {
    @IBOutlet var viewBottomTabbar: UIView!
    @IBOutlet var viewCurve: UIView!
    @IBOutlet weak var tabbarBtn1: UIButton!
    @IBOutlet weak var tabbarBtn2: UIButton!
    @IBOutlet weak var tabbarBtn3: UIButton!
    @IBOutlet weak var tabbarBtn33: UIButton!
    @IBOutlet weak var tabbarBtn333: UIButton!
    @IBOutlet weak var tabbarBtn4: UIButton!
    @IBOutlet weak var tabbarBtn5: UIButton!
    @IBOutlet weak var lblNotificationCount: UILabel!
    
    @IBOutlet weak var contViewCurveHeight: NSLayoutConstraint!
    var imagePicker = UIImagePickerController()
 
    let selectedImageV = UIImageView()
    let pickButton = UIButton()
    let resultsButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        var Y = self.view.frame.height - self.viewBottomTabbar.frame.height
        
        
        if  UIDevice.current.hasNotch{
            Y = self.view.frame.height - 100
            let myview = UIView()
            myview.backgroundColor = .white
            myview.frame =  CGRect(x: 0, y: self.view.frame.height - 60, width: self.view.frame.size.width, height: self.viewBottomTabbar.frame.height)
             self.view.addSubview(myview)
           // self.view.bringSubviewToFront(myview)
            
        }else{
            Y  = self.view.frame.height - self.viewBottomTabbar.frame.height
        }
        
        
        let frm = CGRect(x: 0, y: Y, width: self.view.frame.size.width, height: self.viewBottomTabbar.frame.height)
        self.viewBottomTabbar.frame = frm
        self.view.addSubview(self.viewBottomTabbar)
        self.view.bringSubviewToFront(self.viewBottomTabbar)
        self.tabBar.isHidden = true
        self.tabbarBtn1.isSelected = true
       let tapGesture = UITapGestureRecognizer(target: self, action: #selector(normalTap))
        tapGesture.numberOfTapsRequired = 1
       // tabbarBtn3.addGestureRecognizer(tapGesture)
        tabbarBtn33.addGestureRecognizer(tapGesture)
        
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(normalTap))
        tapGesture2.numberOfTapsRequired = 1
       // tabbarBtn3.addGestureRecognizer(tapGesture)
        tabbarBtn333.addGestureRecognizer(tapGesture2)
        
        tabbarBtn33.addTarget(self, action: #selector(normalTap), for: .touchUpInside)

        
        
        
        
       viewCurve.isHidden = true
       contViewCurveHeight.constant = 0
      viewBottomTabbar.frame.size.height = 60
         
        if  UIDevice.current.hasNotch{
           viewBottomTabbar.frame.origin.y = self.view.frame.height - self.viewBottomTabbar.frame.height - 30
        }else{
            viewBottomTabbar.frame.origin.y = self.view.frame.height - self.viewBottomTabbar.frame.height
            
        }
       

        
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    

 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(longTap))
               longGesture.minimumPressDuration = TimeInterval(exactly: 0.5)!
               tabbarBtn3.addGestureRecognizer(longGesture)
    }
    
    @objc func normalTap(_ sender: UIGestureRecognizer){
           print("Normal tap")
      //  self.selectedIndex = 2
        
       
     if viewCurve.isHidden {
            viewCurve.isHidden = false
               contViewCurveHeight.constant = 115
                 viewBottomTabbar.frame.size.height = 175
        if  UIDevice.current.hasNotch{
                viewBottomTabbar.frame.origin.y = self.view.frame.height - self.viewBottomTabbar.frame.height - 30
            }else{
                viewBottomTabbar.frame.origin.y = self.view.frame.height - self.viewBottomTabbar.frame.height
            }
         
           up(viewTest: viewCurve)
        }else{
            removeSubViewWithAnimation(viewMain: viewCurve)
            //viewCurve.isHidden = true
                contViewCurveHeight.constant = 0
               viewBottomTabbar.frame.size.height = 60
            if  UIDevice.current.hasNotch{
                viewBottomTabbar.frame.origin.y = self.view.frame.height - self.viewBottomTabbar.frame.height - 30
            }else{
                viewBottomTabbar.frame.origin.y = self.view.frame.height - self.viewBottomTabbar.frame.height
            }
        }
       }

    
    func up(viewTest: UIView) {
        let orignalT: CGAffineTransform = viewTest.transform
        viewTest.transform = CGAffineTransform.identity.scaledBy(x: 0.0, y: 0.0)
        UIView.animate(withDuration: 0.1, animations: {
            
            viewTest.transform = orignalT
        }, completion:nil)
    }

    
    func removeSubViewWithAnimation(viewMain: UIView) {
        let orignalT: CGAffineTransform = viewMain.transform
        UIView.animate(withDuration: 0.1, animations: {
            viewMain.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
        }, completion: {(sucess) in
            
            viewMain.transform = orignalT
           viewMain.isHidden = true
        })
        
    }
    
    
    
    
       @objc func longTap(_ sender: UIGestureRecognizer){
           print("Long tap")
           if sender.state == .ended {
               print("UIGestureRecognizerStateEnded")
               //Do Whatever You want on End of Gesture
            
           }
           else if sender.state == .began {
               print("UIGestureRecognizerStateBegan.")
           }
       }
       
    func selectTabIndex(index: Int){
           switch index {
           case 0:
               self.tabBtnAction(self.tabbarBtn1)
               break
           case 1:
                self.tabBtnAction(self.tabbarBtn2)
               break
           case 2:
                self.tabBtnAction(self.tabbarBtn3)
               break
           case 4:
                self.tabBtnAction(self.tabbarBtn4)
               break
           default:
               self.tabBtnAction(self.tabbarBtn5)
           }
       }
    
    @objc func actionlong () {
        
    }
    @IBAction func tabBtn(_ sender: UIButton) {
        print("Normal tap")
   //  self.selectedIndex = 2
     
    
  if viewCurve.isHidden {
         viewCurve.isHidden = false
            contViewCurveHeight.constant = 115
              viewBottomTabbar.frame.size.height = 175
    if  UIDevice.current.hasNotch{
             viewBottomTabbar.frame.origin.y = self.view.frame.height - self.viewBottomTabbar.frame.height - 30
         }else{
             viewBottomTabbar.frame.origin.y = self.view.frame.height - self.viewBottomTabbar.frame.height
         }
      
        up(viewTest: viewCurve)
     }else{
         removeSubViewWithAnimation(viewMain: viewCurve)
         //viewCurve.isHidden = true
             contViewCurveHeight.constant = 0
            viewBottomTabbar.frame.size.height = 60
        if  UIDevice.current.hasNotch{
             viewBottomTabbar.frame.origin.y = self.view.frame.height - self.viewBottomTabbar.frame.height - 30
         }else{
             viewBottomTabbar.frame.origin.y = self.view.frame.height - self.viewBottomTabbar.frame.height
         }
     }
    }
    
    
    @IBAction func ActionCategory(_ sender: UIButton) {
        removeSubViewWithAnimation(viewMain: viewCurve)
               //viewCurve.isHidden = true
                   contViewCurveHeight.constant = 0
                  viewBottomTabbar.frame.size.height = 60
        self.tabbarBtn1.isSelected = false
        self.tabbarBtn2.isSelected = false
        self.tabbarBtn3.isSelected = false
        self.tabbarBtn4.isSelected = false
        self.tabbarBtn5.isSelected = false
        self.tabbarBtn1.tintColor = UIColor.gray
        self.tabbarBtn2.tintColor = UIColor.gray
        self.tabbarBtn3.tintColor = UIColor.gray
        self.tabbarBtn4.tintColor = UIColor.gray
        self.tabbarBtn5.tintColor = UIColor.gray
        self.tabbarBtn4.isSelected  = true
       self.selectedIndex = 1
        self.navigationController?.popToRootViewController(animated: true)
        NotificationCenter.default.post(Notification(name: .planDetail, object: nil, userInfo: nil))
        
        // let vc2 = HomeVC.instance(storyBoard: .Favourite) as! HomeVC
        //self.navigationController!.pushViewController(vc2, animated: true)
        
        if  UIDevice.current.hasNotch{
                   viewBottomTabbar.frame.origin.y = self.view.frame.height - self.viewBottomTabbar.frame.height - 30
               }else{
                   viewBottomTabbar.frame.origin.y = self.view.frame.height - self.viewBottomTabbar.frame.height
               }
    }
    
    @IBAction func tabBtnAction(_ sender: UIButton) {
        self.tabbarBtn1.isSelected = false
        self.tabbarBtn2.isSelected = false
        self.tabbarBtn3.isSelected = false
        self.tabbarBtn4.isSelected = false
        self.tabbarBtn5.isSelected = false
        self.tabbarBtn1.tintColor = UIColor.gray
        self.tabbarBtn2.tintColor = UIColor.gray
        self.tabbarBtn3.tintColor = UIColor.gray
        self.tabbarBtn4.tintColor = UIColor.gray
        self.tabbarBtn5.tintColor = UIColor.gray
        if sender == tabbarBtn1 {
            self.tabbarBtn1.isSelected  = true
            self.tabbarBtn1.tintColor = UIColor(named: "colorBaseApp")
            self.selectedIndex = 0
            self.navigationController?.popToRootViewController(animated: true)
           
        }else if sender == tabbarBtn2{
            self.tabbarBtn2.tintColor = UIColor(named: "colorBaseApp")
            self.tabbarBtn2.isSelected  = true
          //  self.selectTabIndex(index: 1)
            NotificationCenter.default.post(Notification(name: .membership, object: nil, userInfo: nil))
            self.selectedIndex = 1
            self.navigationController?.popToRootViewController(animated: true)
        }else if sender == tabbarBtn3{
            self.tabbarBtn3.tintColor = UIColor(named: "colorBaseApp")
            self.tabbarBtn3.isSelected  = true
            self.navigationController?.popToRootViewController(animated: true)
         //   self.selectedIndex = 2
        }else if sender == tabbarBtn4{
            self.tabbarBtn4.tintColor = UIColor(named: "colorBaseApp")
            self.tabbarBtn4.isSelected  = true
            self.selectedIndex = 3
            self.navigationController?.popToRootViewController(animated: true)
        }else if sender == tabbarBtn5{
            self.tabbarBtn5.tintColor = UIColor(named: "colorBaseApp")
            self.tabbarBtn5.isSelected  = true
            self.selectedIndex = 4
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func isHiddenTabBar(hidden:Bool) {
         self.viewBottomTabbar.isHidden = hidden
        self.tabBarController?.tabBar.isHidden = hidden
        
        
    }

}






