//
//  AppDelegate.swift
//  IRatePro
//
//  Created by mac on 23/12/20.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import FirebaseMessaging
import Stripe




@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var strFcmToken:String = ""
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        if UserDefaults.standard.isLoggedIn(){
            window = UIWindow.init(frame: UIScreen.main.bounds)
            let vc = CustomTabBarController.instance(storyBoard: .tabBarViewController) as! CustomTabBarController
            let data = UserDefaults.standard.getUserData()
            guard let loginModal = try? JSONDecoder().decode(GetProfileModel.self, from:data) else { return true }
            AppDataHelper.shard.logins = loginModal
            vc.tabBar.isHidden = true
            window?.rootViewController = vc
            window?.makeKeyAndVisible()
            
        }
        
        IQKeyboardManager.shared.enable = true
              IQKeyboardManager.shared.enableAutoToolbar = true
              if #available(iOS 13.0, *) {
                          // Always adopt a light interface style.
            window?.overrideUserInterfaceStyle = .light
            application.statusBarStyle = .lightContent
            }
        (StripeAPI.defaultPublishableKey = "pk_test_NrUnXPy2i1rGylQt37Cv2uNX")
    //       pk_test_0gJww8E7CQVRTEFRTFPF1U3A Stripe.setDefaultPublishableKey("pk_test_51HgpOcL86vNnnoiZrSgDdpLfzK04VMqp01yxWySOQqK8Eq74J618TPtF6nw1t8Rq5JrC7Emd00lPbmdDiM5pIpuI00ldwFp1Kc")
        application.applicationIconBadgeNumber = 0
        UIApplication.shared.applicationIconBadgeNumber = 0
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        application.registerForRemoteNotifications()
        application.applicationIconBadgeNumber = 0
        requestNotificationAuthorization(application: application)
        
        if let userInfo = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] {
            NSLog("[RemoteNotification] applicationState: didFinishLaunchingWithOptions for iOS9: \(userInfo)")
            //TODO: Handle background notification
        }

        if (launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [String: AnyObject]) != nil {
            let when = DispatchTime.now() + 2.0
            DispatchQueue.main.asyncAfter(deadline: when) {
               // self.handleNotification(userInfo: remoteNotificationData)
            }
        }
        
        return true
    }
    
    func requestNotificationAuthorization(application: UIApplication) {
        UNUserNotificationCenter.current().delegate = self
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: {_ , _ in })
        } else {
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
    }

}

//-----hp--

extension AppDelegate: MessagingDelegate {


    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("fcm token :- \(String(describing: fcmToken))")
        self.strFcmToken = fcmToken
        UserDefaults.standard.set(fcmToken, forKey: "FCMToken")
    }

    // iOS9, called when presenting notification in foreground
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print("[RemoteNotification] applicationState:  didReceiveRemoteNotification for iOS9: \(userInfo)")
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        self.strFcmToken = Messaging.messaging().fcmToken ?? ""
        print("fcm token22 :- \(String(describing: self.strFcmToken))")
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {

        let userInfo = notification.request.content.userInfo
        print(" UserNotificationCenter applicationState: willPresentNotification: \(userInfo)")

        // send push notifcation steps - [click on ment then choose fanbase -> select mutiple name and click on send button]

         // open ContestJustMeVC view controller  when we get push notifcation from fanbase
        // on ContestJustMeVC call api " -> https://ctinfotech.com/CT06/wooo/api/getcontest" with parameter "contest_id:1"
        
        completionHandler([.alert, .sound, .badge])
    }

     func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        print("[UserNotificationCenter] applicationState: didReceiveResponse: \(userInfo)")
        if UserDefaults.standard.isLoggedIn(){
        let loginData = UserDefaults.standard.getUserData()
        let sendModal = try? JSONDecoder().decode(GetProfileModel.self, from: loginData)
        AppDataHelper.shard.logins = sendModal
        let vc = CustomTabBarController.instance(storyBoard: .tabBarViewController) as! CustomTabBarController
        vc.tabBar.isHidden = true
        UIApplication.shared.windows[0].rootViewController = vc

       NotificationCenter.default.post(Notification(name: .notificationDetail, object: userInfo, userInfo: userInfo))
        
        }

        completionHandler()
    }
}



